/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Client.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package commande;

import java.util.UUID;

import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import donneGenerique.Adresse;
import object.Client;

/***************************************************************
 * Commandes pour modifier les donnees d'un client
 ***************************************************************/
public class Cmd_Client {

	/***************************************************************
	 * Ajout d'un client dans la base de donnee
	 * 
	 * @param client 		Le client a rajouter
	 * @throws Exception
	 ***************************************************************/
	public void ajouter(Client client) throws Exception {
		new SqlController(new SqlManager()).AjoutCompteClient(client);
	}

	/***************************************************************
	 * Modification d'un client dans la base de donnee
	 * 
	 * @param client
	 * @throws Exception
	 ***************************************************************/
	public void modifier(Client client) throws Exception {
		new SqlController(new SqlManager()).SuprimmerClient(client);
		new SqlController(new SqlManager()).AjoutCompteClient(client);
	}

	/***************************************************************
	 * Selection d'un client dans la base de donnee
	 * 
	 * @param idUtilisateur 	Identite unique du client
	 * @return 					Retourne le client avec ID correspondant
	 * @throws Exception
	 ***************************************************************/
	public Client selectionner(UUID idUtilisateur) throws Exception {
		return new SqlController(new SqlManager()).Selectionner_Client(idUtilisateur);
	}
	
	/***************************************************************
	 * Supprimer un client dans la base de donnee
	 * 
	 * @param client
	 * @throws Exception
	 ***************************************************************/
	public void Suprimmer(Client client) throws Exception{
		new SqlController(new SqlManager()).SuprimmerClient(client);
	}
	
	/***************************************************************
	 * Ajout d'une nouvelle adresse par defaut dans la base de 
	 * donnee
	 * 
	 * @param client 		Le client actif
	 * @param adr 			La nouvelle adresse par defaut
	 * @throws Exception
	 ***************************************************************/
	public void AjouterNouvelleAdresseDefaut(Client client, Adresse adr) throws Exception{
		//sc�nario 8.a
		new SqlController(new SqlManager()).AjouterAdresseDefautClient(client.getId(), adr);
	}
	
	/***************************************************************
	 * Modification d'une adresse par defaut dans la base de donnee
	 * 
	 * @param client
	 * @param clientAdresseId
	 * @throws Exception
	 ***************************************************************/
	public void ModifierAdresseDefaut(Client client, UUID clientAdresseId) throws Exception{
		//sc�nario 8.b
		//adr = addrese a mettre en defautl, doit exister dans le serveur et avoir le meme id
		new SqlController(new SqlManager()).ChangerAdresseDefautClient(client.getId(), clientAdresseId);
	}

}
