/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Commande.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package commande;

import java.util.List;
import java.util.UUID;

import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import object.Utilisateur;
import object.Commande;

/***************************************************************
 * Classe de gestion des commandes
 ***************************************************************/
public class Cmd_Commande {

	/***************************************************************
	 * Enregistrement d'une commande
	 * 
	 * @param commandeAEnregistrer 		Commande a enregistrer
	 * @throws Exception
	 ***************************************************************/
	public void enregistrerCommande(Commande commandeAEnregistrer)
			throws Exception {
		// use case principal
		new SqlController(new SqlManager()).AjoutCommande(commandeAEnregistrer);
	}

	/***************************************************************
	 * Assignement d'une commande a un livreur
	 * 
	 * @param commandeAAssigner 	Commande a assigner
	 * @param livreur 				Livreur responsable de la commande
	 * @return 						Confirmation d'etat
	 ***************************************************************/
	public Boolean AssignerCommande(Commande commandeAAssigner,
			Utilisateur livreur) {
		Boolean commandeAssigne = false;
		return commandeAssigne;
	}

	/***************************************************************
	 * Selection des commandes 
	 * @return La liste de commande
	 * @throws Exception 
	 ***************************************************************/
	public List<Commande> selectionnerCommande() throws Exception {
		return new SqlController(new SqlManager()).selectionnerCommande();
	}

	
	public void CommandePreparationStart(UUID idCommande) throws Exception{
		new SqlController(new SqlManager()).CommandePreparationStart(idCommande);
	}
	
	public void CommandePreparationEnd(UUID idCommande) throws Exception{
		new SqlController(new SqlManager()).CommandePreparationEnd(idCommande);
	}

	public void suprimmerCommande(Commande cmd) throws Exception {
		new SqlController(new SqlManager()).SuprimmerCommande(cmd);
		
	}
	
	
}