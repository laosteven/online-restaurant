/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Connexion.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package commande;

import java.util.List;

import javax.swing.JOptionPane;

import modele.ModeleCommander;
import modele.ModeleLivreur;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import donneGenerique.DonneeGlobal;
import object.Client;
import object.Commande;
import object.Livreur;
import object.Restaurant;
import object.Restaurateur;
import object.Utilisateur;
import utility.Utility;
import vue.GUIClientCommande;
import vue.GUILivreur;
import vue.GUIPrincipal;

/*************************************************************
 * Commande pour la connexion dans la base de donnee
 *************************************************************/
public class Cmd_Connexion {
	
	/*************************************************************
	 * Connexion dans le logiciel
	 *
	 * @param nom 			Nom d'utilisateur
	 * @param pass 			Mot de passe de l'utilisateur
	 * @return 				Le compte de l'utilisateur
	 * @throws Exception
	 *************************************************************/
	public Boolean connecter(String nom, String pass) throws Exception{
		
		Utilisateur temp = new SqlController(new SqlManager()).Authentication(nom, pass);
		
		if (temp != null) {
			
			switch (temp.getHierarchyName()) {
			
				case "Client":
					Client  client = new SqlController(new SqlManager()).Selectionner_Client(temp.getIdUtilisateur());
					
					ModeleCommander model = new ModeleCommander(Utility.convertToObjectList(new Cmd_Restaurant().selectionnerETRetour()), client);
					
					new GUIClientCommande(model);
					
					break;
					
				case "Livreur":
					
					
					Livreur livreur = new SqlController(new SqlManager()).Selectionner_Livreur(temp.getIdUtilisateur());
					
					List<Commande> listCommande = new SqlController(new SqlManager()).selectionnerCommandeLivreur();
					
					ModeleLivreur modelLivreur = new ModeleLivreur(livreur, Utility.convertToObjectList(listCommande));
					
					new GUILivreur(modelLivreur);
					
					break;
					
				case "Entrepreneur":
					
					//Entrepreneur entrepreneur = new SqlController(new SqlManager()).Selectionner_Entrepreneur(temp.getIdUtilisateur());
					
					DonneeGlobal.getInstance().setRestaurateurList(Utility.convertToAffichageObjectList(new Cmd_Restaurateur().selectionEtRetour()));
					DonneeGlobal.getInstance().setRestaurantList(Utility.convertToAffichageObjectList(new Cmd_Restaurant().selectionnerETRetour()));	
					DonneeGlobal.getInstance().setUtilisateurConnecte(temp);
					
					new GUIPrincipal();
										
					break;
					
				case "Restaurateur":
					
					Restaurateur resto = new SqlController(new SqlManager()).Selectionner_Restaurateur(temp.getIdUtilisateur());
					
					List<Restaurant> list  = new SqlController(new SqlManager()).SelectionnnerRestaurantsFromUser(resto.getIdUtilisateur());
					
					DonneeGlobal.getInstance().setUtilisateurConnecte(resto);
					DonneeGlobal.getInstance().setRestaurantList(Utility.convertToAffichageObjectList(list));
					
					new GUIPrincipal();
					
					break;		
			}
			
			return true;


		}
		
		else{
				JOptionPane.showMessageDialog(null, "Mauvais mot de passe.");
				return false;
			}
		
		}
	}

