/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		EnvoyerCourriel.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package commande;

import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import object.AdresseClient;
import object.Commande;
import object.ItemCommande;

import donneGenerique.Adresse;

/***************************************************************
 * Envoie un message de confirmation au client
 * 
 * Reference: https://developers.google.com/appengine/docs/java/mail
 * /usingjavamail#Sending_Email_Messages
 ***************************************************************/
public class Cmd_EnvoiCourriel {
	
	private Session session;
	
	/***************************************************************
	 * Initialise une session de message
	 ***************************************************************/
	public void initialise(){
		
		// Authentification du compte GMail
		final String comptePrincipal = "LOG210eq03@gmail.com";
		final String motDePasse = "Iteration2";

		// Recuperation et ajout de proprietes
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		// Etablissement d'une session pour l'envoie de message
		session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(comptePrincipal,
								motDePasse);
					}
				});
		
	}

	/***************************************************************
	 * Constructeur
	 * Envoie une liste de commande au client
	 * 
	 * @param courriel
	 *            Adresse courriel du client
	 * @param adresse
	 *            Adresse geographique du client
	 * @param temps
	 *            Temps de livraison
	 * @param confirmeID
	 *            Numero de confirmation
	 ***************************************************************/
	public Cmd_EnvoiCourriel(String courriel, AdresseClient adresseRef,
			String temps, UUID confirmeID, String listeCmd, float total) {

		// Connection au compte
		initialise();

		try {

			// Creation du courriel
			Message message = new MimeMessage(session);

			// Destinataire
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(courriel));

			// Sujet du courriel avec # de confirmation
			message.setSubject("Confirmation de commande: #"
					+ confirmeID.toString());

			// Corps / Message du courriel

			message.setText("Cher client, \n\n"
					+ "Voici les plats de votre commandes: \n\n"
					+ listeCmd
					+ "\nLe montant total de la commande est : "
					+ total
					+ " $\n"
					+ "\nLes plats seront livres a "
					+ adresseRef.getAdresse().toStringSpaced()
					+ " vers "
					+ temps
					+ "\n\nMerci d'avoir utilisee notre systeme pour effectuer "
					+ "vos commandes. \n\n" + "L'equipe 03 \nLOG210");

			// Envoie du courriel
			Transport.send(message);

			// Confirmation dans la console
			System.out.println("Courriel envoye");

		} catch (MessagingException exception) {
			throw new RuntimeException(exception);
		}

	}
	
	/***************************************************************
	 * Constructeur
	 * Envoie un courriel a un nouveau utilisateur du systeme
	 * 
	 * @param courriel Courriel du nouveau client
	 ***************************************************************/
	public Cmd_EnvoiCourriel(String nom, String courriel){

		// Connection au compte
		initialise();

		try {

			// Creation du courriel
			Message message = new MimeMessage(session);

			// Destinataire
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(courriel));

			// Sujet du courriel avec # de confirmation
			message.setSubject("LOG210: " + nom + " Nouveau compte");

			// Corps / Message du courriel

			message.setText("Cher " + nom + ", "
					+ "\nUn nouveau compte a ete cree pour vous."
					+ "\nVous pouvez, des maintenant, effectuer "
					+ "vos commandes desires a partir des restaurants disponibles "
					+ "dans notre systeme."
					+ "\n\nNous apprecions votre support et nous vous "
					+ "remercions d'avoir utilisee notre systeme pour effectuer "
					+ "vos commandes. \n\n" + "L'equipe 03 \nLOG210");

			// Envoie du courriel
			Transport.send(message);

			// Confirmation dans la console
			System.out.println("Courriel envoye");

		} catch (MessagingException exception) {
			throw new RuntimeException(exception);
		}
		
	}

}
