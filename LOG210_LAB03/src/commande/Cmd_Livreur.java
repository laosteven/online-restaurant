/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Livreur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package commande;

import java.util.UUID;

import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import object.Livreur;
import object.Utilisateur;
import object.Commande;

/***************************************************************
 * Methodes de commandes lors d'une selection d'objets effectuee
 * par un livreur
 ***************************************************************/
public class Cmd_Livreur {

	/***************************************************************
	 * Enregistrement d'une commande selectionnee
	 * @param commandeAEnregistrer
	 ***************************************************************/
	public void enregistrerCommande(Commande commandeAEnregistrer) {

	}

	/***************************************************************
	 * Assigner une commande a un livreur
	 * 
	 * @param commandeAAssigner 	La commande a assigner
	 * @param livreur 				Le livreur qui s'occupe de la commande
	 * @return 						Etat de confirmation de la commande
	 * @throws Exception 
	 ***************************************************************/
	public Boolean assignerCommande(Commande commandeAAssigner,
			Utilisateur livreur) throws Exception {
		
		return new SqlController(new SqlManager()).AssignerCommande(commandeAAssigner.getIdCommande(), commandeAAssigner.getStatus());

	}

	/***************************************************************
	 * Selection de commande
	 * @return La liste de commande
	 * @throws Exception 
	 ***************************************************************/
	public void EffectuerLivraison(UUID idCommande, Livreur livreur) throws Exception {

		new SqlController(new SqlManager()).EffectuerLivraison(idCommande, livreur);

	}

}
