/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Menu.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package commande;

import java.util.UUID;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import object.Menu;

/*************************************************************
 * Commandes pour gestion d'un menu
 *************************************************************/
public class Cmd_Menu {

	/*************************************************************
	 * Ajout d'un menu dans la base de donnee
	 * 
	 * @param menuAjouter 	Le nouveau menu a rajouter
	 * @param id 			Le code d'identification du menu
	 * @throws Exception
	 ************************************************************/
	public void ajouter(Menu menuAjouter, UUID id) throws Exception {

		new SqlController(new SqlManager()).SupprimerMenu(menuAjouter);
		new SqlController(new SqlManager()).AjoutMenuComplet(menuAjouter, id);

	}

	/*************************************************************
	 * Modification d'un menu
	 * 
	 * @param menuModifier 	Le menu a modifier
	 * @param id 			Le code d'identification du menu
	 * @throws Exception
	 ************************************************************/
	public void modifier(Menu menuModifier, UUID id) throws Exception {

		// Dans la base de donnee

		SqlController menu = new SqlController(new SqlManager());
		// Dans la base de donnee
		menu.ModifierMenu(menuModifier, id);
	}

	/*************************************************************
	 * Suppression d'un menu de la base de donnee
	 * 
	 * @param menuSupprimer Le menu a supprimer
	 * @throws Exception
	 *************************************************************/
	public void supprimer(Menu menuSupprimer) throws Exception {

		SqlController menu = new SqlController(new SqlManager());
		menu.SupprimerMenu(menuSupprimer);

	}

	/*************************************************************
	 * Retour d'un menu selectionne
	 *************************************************************/
	public Menu selectionner(UUID id) throws Exception {

		return new SqlController(new SqlManager()).SelectionnnerMenu(id);
		
	}

}
