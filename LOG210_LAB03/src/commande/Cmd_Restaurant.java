/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Restaurant.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package commande;

import java.util.List;
import java.util.UUID;

import object.AfficheObject;
import object.Restaurant;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import donneGenerique.DonneeGlobal;

/*************************************************************
 * Commandes pour gestion d'un restaurant
 *************************************************************/
public class Cmd_Restaurant {

	/*************************************************************
	 * Supprimer un restaurant de la base de donnee
	 *
	 * @param restaurantSupprimer 	Le restaurant a supprimer
	 * @throws Exception 
	 *************************************************************/
	public void supprimer(Restaurant restaurantSupprimer) throws Exception {

		new SqlController(new SqlManager())
				.SupprimeRestaurant(restaurantSupprimer);
		new Cmd_Restaurant().selectionner();
		
	}
	
	/*************************************************************
	 * Ajout d'un restaurant.
	 *
	 * @param restaurantAjouter 	
	 * 				Restaurant a rajouter dans la base de donnee
	 * @throws Exception 
	 *************************************************************/
	public void ajouter(Restaurant restaurantAjouter) throws Exception {

		new SqlController(new SqlManager()).AjoutRestaurant(
				restaurantAjouter.getNom(), 
				restaurantAjouter.getAdresse(),
				restaurantAjouter.getTelephone(),
				restaurantAjouter.getTypeCuisine(),
				restaurantAjouter.getId(),
				restaurantAjouter.getIdAssociation());
		
		new Cmd_Restaurant().selectionner();

	}
	
	/*************************************************************
	 * Constructeur: Modification d'un restaurant.
	 *
	 * @param restaurantModifier Le restaurant a modifier
	 * @throws Exception 
	 *************************************************************/
	public void modifier(Restaurant restaurantModifier) throws Exception {
		
		//String addresse=restaurantModifier.getAdresse().toString();
	
		new SqlController(new SqlManager()).ModifieRestaurant(
				restaurantModifier.getNom(), 
				restaurantModifier.getAdresse(),
				restaurantModifier.getTelephone(),
				restaurantModifier.getTypeCuisine(),
				restaurantModifier.getIdAssociation(),
				restaurantModifier.getId());
		
		new Cmd_Restaurant().selectionner();
		
	}
	
	/*************************************************************
	 * Selection
	 *
	 * @throws Exception 
	 *************************************************************/
	public void selectionner() throws Exception {

		List<Restaurant> restoList =  new SqlController(new SqlManager()).SelectionnnerRestaurants();
		
		DonneeGlobal test=DonneeGlobal.getInstance();
		
		test.getRestaurantList().clearList();
		
		for (Restaurant restaurant : restoList) {
			test.getRestaurantList().addEnd(new AfficheObject(restaurant, true));
			test.getRestaurantList().changeEnded();
		}
		
	}
	
	public void selectionnerFromUser(UUID userId) throws Exception {

		List<Restaurant> restoList =  new SqlController(new SqlManager()).SelectionnnerRestaurantsFromUser(userId);
		
		DonneeGlobal test=DonneeGlobal.getInstance();
		
		test.getRestaurantList().clearList();
		
		for (Restaurant restaurant : restoList) {
			test.getRestaurantList().addEnd(new AfficheObject(restaurant, true));
			test.getRestaurantList().changeEnded();
		}
		
	}
	
	
	/*************************************************************
	 * Selection
	 *
	 * @throws Exception
	 * @return 
	 *************************************************************/
	public List<Restaurant> selectionnerETRetour() throws Exception {

		return new SqlController(new SqlManager()).SelectionnnerRestaurants();

	}
	
}
