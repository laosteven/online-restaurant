/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Cmd_Restaurateur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package commande;

import java.util.List;



import object.AfficheObject;
import object.Restaurateur;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import donneGenerique.DonneeGlobal;

/***************************************************************
 * Commandes pour la gestion d'un restaurateur
 ***************************************************************/
public class Cmd_Restaurateur {

	
	/*************************************************************
	 * Ajout d'un restaurateur
	 *
	 * @param restaurateurAjouter 	
	 * 				Restaurateur a rajouter dans la base de donnee
	 * @throws Exception 
	 *************************************************************/
	public void ajouter(Restaurateur restaurateurAjouter) throws Exception {

		// Dans la base de donnee
		new SqlController(new SqlManager()).AjoutRestaurateur(
				restaurateurAjouter.getNom(), 
				restaurateurAjouter.getAdresse(),
				restaurateurAjouter.getPrenom(),
				restaurateurAjouter.getId(),
				restaurateurAjouter.getIdUtilisateur());

		// Dans la liste de restaurateur
		new Cmd_Restaurateur().selectionRestaurateur();
		
	}

	/*************************************************************
	 * Modification d'un restaurateur
	 *
	 * @param restaurateurModifier Le restaurateur a modifier
	 * @throws Exception 
	 *************************************************************/
	public void modifier(Restaurateur restaurateurModifier) throws Exception {

		// Dans la base de donnee
		new SqlController(new SqlManager()).ModifieRestaurateur(restaurateurModifier);
		
		// Dans la liste de restaurateur
		new Cmd_Restaurateur().selectionRestaurateur();
		
	}
	
	/*************************************************************
	 * Selection des restaureateurs
	 *
	 * @throws Exception 
	 *************************************************************/
	public void selectionRestaurateur() throws Exception {

		List<Restaurateur> restoList = new SqlController(new SqlManager()).SelectionnnerRestaurateurs();
		
		DonneeGlobal test=DonneeGlobal.getInstance();
		
		test.getRestaurateurList().clearList();
		
		for (Restaurateur restaurateur : restoList) {
			test.getRestaurateurList().addEnd(new AfficheObject(restaurateur,true));	
			test.getRestaurantList().changeEnded();
		}
		
	}
	
	/*************************************************************
	 * Supprimer un restaurateur
	 *
	 * @param restaurateurSupprimer Le restaurateur a supprimer
	 * @throws Exception 
	 *************************************************************/
	public void supprimer(Restaurateur restaurateurSupprimer) throws Exception {

		// Dans la base de donnee
		new SqlController(new SqlManager())
				.SupprimeRestaurateur(restaurateurSupprimer);
		
		// Dans la liste de restaurateur
		new Cmd_Restaurateur().selectionRestaurateur();
		
	}

	public List<Restaurateur> selectionEtRetour() throws Exception {
		
		return new SqlController(new SqlManager()).SelectionnnerRestaurateurs();
	}	
	
}
