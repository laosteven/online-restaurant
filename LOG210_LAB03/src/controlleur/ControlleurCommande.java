/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ControlleurCommande.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package controlleur;

import java.util.Observer;
import javax.swing.JOptionPane;
import commande.Cmd_Commande;
import commande.Cmd_EnvoiCourriel;
import donneGenerique.Adresse;
import object.Client;
import object.Commande;
import object.ItemCommande;
import object.Commande.CommandeStatus;
import object.ItemMenu;
import object.Restaurant;
import modele.ModeleCommander;

/****************************************************************
 * Controlleur des commandes de livraison
 ****************************************************************/
public class ControlleurCommande {
	
	/****************************************************************
	 * Attribut
	 ****************************************************************/
	private modele.ModeleCommander modele;

	/****************************************************************
	 * Constructeur
	 * Definition du modele de commande
	 * 
	 * @param modele Le modele de la commande
	 ****************************************************************/
	public ControlleurCommande(ModeleCommander modele) {
		super();
		this.modele = modele;
	}

	/****************************************************************
	 * Changement d'index selon le restaurant selectionne
	 * @param index
	 ****************************************************************/
	public void selectedRestaurantIndexChange(int index) {
		modele.setSelectedRestaurant(index);
	}

	/****************************************************************
	 * Changement d'index selon le menu selectionne
	 * @param index
	 ****************************************************************/
	public void selectedMenuIndexChange(int index) {
		modele.setSelectedItemMenu(index);
	}

	/****************************************************************
	 * Annuler l'observeur
	 * @param obsEnlever Observeur a enlever
	 ****************************************************************/
	public void removeObserver(Observer obsEnlever) {
		modele.deleteObserver(obsEnlever);
	}

	/****************************************************************
	 * Ajout d'un plat dans le carnet de commande du client
	 ****************************************************************/
	public void ajouterItemCarnet() {

		int quantity = 0;
		
		try {
			quantity = Integer.parseInt(JOptionPane.showInputDialog("Entrez la quantit� d�sir�e", ""));
		} catch (NumberFormatException exception) {
			
		}

		// Si la quantite n'est pas vide
		if(quantity != 0) {
			
			ItemCommande item = new ItemCommande(modele.getSelectedItemMenu(),quantity);
			modele.ajouter(item);
			
		}
		
	}
	
	/****************************************************************
	 * Effacement d'un plat de la liste de carnet
	 ****************************************************************/
	public void supprimerItemCarnet(int index) {
		
		modele.removeCommandeAtIndex(index);
		
	}
	
	public void ajouterItemCarnetTest(int nb,ItemMenu itemC) {

		//int quantity = Integer.parseInt(JOptionPane.showInputDialog("Entrez la quantit� d�sir�e", ""));
		
		ItemCommande item = new ItemCommande(itemC,nb);	
		modele.ajouter(item);
		
	}
	

	/****************************************************************
	 * Affichage d'une description de plat
	 ****************************************************************/
	public void afficherDescription() {
		
		try {
			
			JOptionPane.showMessageDialog(null, 
					modele.getSelectedItemMenu().getDescription(), 
					modele.getSelectedItemMenu().getNom(),
					JOptionPane.PLAIN_MESSAGE);
			
		} catch (NullPointerException exception) {
			
			JOptionPane.showMessageDialog(null, "Aucun plat " +
					"a ete selectionne.");
			
		}
		
	}

	/****************************************************************
	 * Confirmation d'une commande
	 * 
	 * @param dateCommande 			Temps de livraison de la commande
	 * @param adresseSpecifie		Adresse de livraison
	 * @return						Numero de confirmation de la commande
	 ****************************************************************/
	public String passerCommande(String dateCommande) {
		
		Commande nouvelleCommande = new Commande(
				modele.getSelectedRestaurant(), 
				dateCommande, 
				modele.getClientEnCours().getAdresseDefaut().getAdresse(),
				CommandeStatus.Ordered);
		nouvelleCommande.setListItem(modele.getCommande());
		
		// Enregistrement de la commande du client
		try {
			new Cmd_Commande().enregistrerCommande(nouvelleCommande);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Si le client possede un courriel
		if(modele.getClientEnCours().getCourriel() != null){
			
			// Envoi du courriel
			new Cmd_EnvoiCourriel(
					modele.getClientEnCours().getCourriel(), 
					modele.getClientEnCours().getAdresseDefaut(), 
					dateCommande, 
					nouvelleCommande.getNoConfirmation(), 
					nouvelleCommande.displayItem(), 
					modele.getTotal());
			
		}
		
		// Retourne un message de confirmation au client
		return "Voici le numero de confirmation de votre commande: \n"
				+ nouvelleCommande.getNoConfirmation();

	}
	
	public String passerCommandeTEST(String dateCommande,
			Restaurant restaurant, Adresse adresse) {

		cmdTest = new Commande(restaurant, dateCommande, adresse,
				CommandeStatus.Ordered);
//		cmdTest.setListItem(modele.getCommande());

		// Envoi du courriel

		// Retourne un message de confirmation au client
		return "Voici le numero de confirmation de votre commande: \n"
				+ cmdTest.getNoConfirmation();

	}

	private Commande cmdTest;

	public Commande getTestCommande() {
		return cmdTest;

	}
	

	/****************************************************************
	 * Supprimer un plat du carnet de commande
	 ****************************************************************/
	public void supprimerCommande() {
		modele.getCommande().clearList();
	}
	
}
