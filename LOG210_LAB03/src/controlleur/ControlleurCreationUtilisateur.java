/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ControlleurCreationUtilisateur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package controlleur;

import java.util.UUID;
import object.Client;
import commande.Cmd_Client;
import commande.Cmd_EnvoiCourriel;
import donneGenerique.Adresse;

/******************************************************************
 * Controlleur pour la gestion des informations d'un nouveau
 * utilisateur
 ******************************************************************/
public class ControlleurCreationUtilisateur {

	/**************************************************************
	 * Attributs
	 **************************************************************/

	// Indicateurs de messages d'erreurs
	private boolean erreurSurvenu;
	private String messageErreur;

	// Attributs du nouveau client
	private UUID idUtilisateur;
	private UUID idClient;
	private String nom;
	private String prenom;
	private String nomUtilisateur;
	private String motDePasse;
	private String confirmationPwd;
	
	private String courriel;
	private boolean courrielExistant;
	private String numTelephone;
	private String dateNaissance;
	
	private Adresse adresse;
	private UUID ClientAdressId;
	private UUID adresseId;
	private String numero;
	private String rue;
	private String ville;
	private String province;
	private String codePostal;

	/**************************************************************
	 * Accesseur
	 * 
	 * Renvoie un message d'erreur
	 **************************************************************/
	public String getMessageErreur() {
		return messageErreur;
	}
	
	/**************************************************************
	 * Constructeur
	 * Creation d'une liste d'adresse
	 **************************************************************/
	public ControlleurCreationUtilisateur() {

	}

	/**************************************************************
	 * Creation d'utilisateur
	 * 
	 * @return Verifie s'il manque des informations
	 * @throws Exception 
	 **************************************************************/
	public Boolean creeUtilisateur(UUID idUtilisateur, UUID idClient, String nomRef, String prenomRef,
			String nomUtilisateurRef, String numTelephoneRef, String motDePasseRef,
			String confirmPwdRef, String dateNaissanceRef, String courrielRef, Boolean flag) throws Exception {

		// Definition de parametre
		erreurSurvenu = true;

		// Prise en reference
		this.idUtilisateur = idUtilisateur;
		this.idClient = idClient;
		this.nom = nomRef;
		this.prenom = prenomRef;
		this.nomUtilisateur = nomUtilisateurRef;
		this.confirmationPwd = confirmPwdRef;
		this.motDePasse = motDePasseRef;	
		
		this.courriel = courrielRef;
		this.numTelephone = numTelephoneRef;
		this.dateNaissance = dateNaissanceRef;
			

		// Verification des cases vides et invalides
		if (nom.isEmpty())
			messageErreur = "Le nom est obligatoire";

		else if (prenom.isEmpty())
			messageErreur = "Le prenom est obligatoire";

		else if (prenom.isEmpty())
			messageErreur = "Le nom d'utilisateur est obligatoire";
		
		else if (numTelephone.isEmpty())
			messageErreur = "Le num�ro de t�l�phone est obligatoire";
			
		else if (motDePasse.isEmpty())
			messageErreur = "Le mot de passe est obligatoire";
		
		else if(confirmationPwd.isEmpty())
			messageErreur = "Le mot de passe de confirmation est obligatoire";

		// Comparaison des mots de passe
		else if (!confirmationPwd.equals(motDePasse))
			messageErreur = "Les mots de passes ne sont pas similaires";
		
		else {

			// Aucune erreur detectee
			erreurSurvenu = false;
			
			// Verification secondaire
			
			// Date de naissance
			if (!dateNaissance.isEmpty()) {

				try{
					erreurSurvenu = verifieDateNaissance();
				} catch (StringIndexOutOfBoundsException |
						ArrayIndexOutOfBoundsException exception){	
					messageErreur = "La date de naissance ne correspond " +
							"pas au format demande";		
					erreurSurvenu = true;
				}	
				
			}
			
			// Courriel
			if (!courriel.isEmpty()) {
				
				if (!courriel.contains("@")) {
					messageErreur = "Le courriel est invalide";
					erreurSurvenu = true;
					courrielExistant = true;
				}
				
			}
		
		}
		
		if(!erreurSurvenu) {
			
			// Creation du client avec une ID unique
			Client client = new Client(this.idClient,this.idUtilisateur, dateNaissance,
					numTelephone, courriel, nom, prenom, nomUtilisateur, adresse);
			
			client.getAdresseDefaut().setIdAdresse(this.ClientAdressId);			
			client.setMotDePasse(motDePasse);

			if(flag){
				new Cmd_Client().modifier(client);
			}
			else{
				
				// Permettre au nouveau client de s'authentifier
				new Cmd_Client().ajouter(client);
				
				// Envoi un courriel
				if(courrielExistant)
					new Cmd_EnvoiCourriel(prenom, courriel);
				
			}
			
		}

		return erreurSurvenu;

	}
	
	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public String getConfirmationPwd() {
		return confirmationPwd;
	}

	public String getCourriel() {
		return courriel;
	}

	public String getNumTelephone() {
		return numTelephone;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public Boolean TestcreeUtilisateur( String nomRef, String prenomRef,
			String nomUtilisateurRef, String numTelephoneRef, String motDePasseRef,
			String confirmPwdRef, String dateNaissanceRef, String courrielRef)
					 throws Exception {

		// Definition de parametre
		erreurSurvenu = true;

		// Prise en reference
		this.idUtilisateur = idUtilisateur;
		this.idClient = idClient;
		this.nom = nomRef;
		this.prenom = prenomRef;
		this.nomUtilisateur = nomUtilisateurRef;
		this.confirmationPwd = confirmPwdRef;
		this.motDePasse = motDePasseRef;	
		
		this.courriel = courrielRef;
		this.numTelephone = numTelephoneRef;
		this.dateNaissance = dateNaissanceRef;
			

		// Verification des cases vides et invalides
		if (nom.isEmpty())
			messageErreur = "Le nom est obligatoire";

		else if (prenom.isEmpty())
			messageErreur = "Le prenom est obligatoire";

		else if (prenom.isEmpty())
			messageErreur = "Le nom d'utilisateur est obligatoire";
		
		else if (numTelephone.isEmpty())
			messageErreur = "Le num�ro de t�l�phone est obligatoire";
			
		else if (motDePasse.isEmpty())
			messageErreur = "Le mot de passe est obligatoire";
		
		else if(confirmationPwd.isEmpty())
			messageErreur = "Le mot de passe de confirmation est obligatoire";

		// Comparaison des mots de passe
		else if (!confirmationPwd.equals(motDePasse))
			messageErreur = "Les mots de passes ne sont pas similaires";
		
		else {

			// Aucune erreur detectee
			erreurSurvenu = false;
			
			// Verification secondaire
			
			// Date de naissance
			if (!dateNaissance.isEmpty()) {

				try{
					erreurSurvenu = verifieDateNaissance();
				} catch (StringIndexOutOfBoundsException |
						ArrayIndexOutOfBoundsException exception){	
					messageErreur = "La date de naissance ne correspond " +
							"pas au format demande";		
					erreurSurvenu = true;
				}	
				
			}
			
			// Courriel
			if (!courriel.isEmpty()) {
				
				if (!courriel.contains("@")) {
					messageErreur = "Le courriel est invalide";
					erreurSurvenu = true;
					courrielExistant = true;
				}
				
			}
		
		}
		
	
			
		

		return erreurSurvenu;

	}
	
	
	
	
	/**************************************************************
	 * Creation d'adresse
	 * 
	 * @return Verifie s'il manque des informations
	 **************************************************************/
	public boolean creeAdresse(UUID idAdresse, UUID idClientAdresse ,String numeroRef, String rueRef, String villeRef,
			String provinceRef, String codePostalRef) {

		// Definition de parametre
		erreurSurvenu = true;

		// Prise en reference
		this.adresseId = idAdresse;
		this.ClientAdressId = idClientAdresse;
		this.numero = numeroRef;
		this.rue = rueRef;
		this.ville = villeRef;
		this.province = provinceRef;
		this.codePostal = codePostalRef;

		// Verification des cases vides
		if (numero.isEmpty())
			messageErreur = "Le numero de l'adresse est obligatoire";

		else if (rue.isEmpty())
			messageErreur = "La rue de l'adresse est obligatoire";

		else if (ville.isEmpty())
			messageErreur = "La ville est obligatoire";

		else if (province.isEmpty())
			messageErreur = "La province est obligatoire";
		
		else if (codePostal.isEmpty())
			messageErreur = "Le code postal est obligatoire";
		
		else if (!codePostal.isEmpty()){
			
			try {
				
				if(!verifieCodePostal()) 
					messageErreur = "Le code postal est invalide";
				
				else {

					// Aucune erreur detectee
					erreurSurvenu = false;

					// Creation d'adresse
					adresse = new Adresse(adresseId, numero, rue,
							ville, province, codePostal);

				}
				
			} catch(StringIndexOutOfBoundsException 
					| ArrayIndexOutOfBoundsException exception) {
				messageErreur = "Le code postal est invalide: "
						+ exception.getMessage();
			}
			
		}	

		return erreurSurvenu;

	}
	
	
	
	/**************************************************************
	 * Verification de la date de naissance
	 * 
	 * @return Verifie s'il manque des informations
	 **************************************************************/
	public boolean verifieDateNaissance(){
		
		boolean verifie = true;

		String[] dateSepare = dateNaissance.split("-");

		if(dateSepare[0].length() != 4)
			messageErreur = "L'annee ne correspond pas au " +
					"format demande";
		
		else if(dateSepare[1].length() != 2)
			messageErreur = "Le mois ne correspond pas au " +
					"format demande";
		
		else if(dateSepare[2].length() != 2)
			messageErreur = "Le jour ne correspond pas au " +
					"format demande";
		
		else if(dateSepare.length < 2)
			messageErreur = "Le format de la date de naissance " +
					"est invalide";
		
		else 
			verifie = false;
		
		return verifie;

	}
	
	/**************************************************************
	 * Methode pour verifier le code postal
	 **************************************************************/
	public boolean verifieCodePostal(){
		
		// Variable
		boolean[] verifie = new boolean[6];
		
		// Enleve les espaces
		String code = codePostal.replaceAll("\\s+", "");
		
		// Verification des lettres
		verifie[0] = Character.isLetter(code.charAt(0));
		verifie[1] = Character.isLetter(code.charAt(2));
		verifie[2] = Character.isLetter(code.charAt(4));
		
		// Verification des chiffres
		verifie[3] = Character.isDigit(code.charAt(1));
		verifie[4] = Character.isDigit(code.charAt(3));
		verifie[5] = Character.isDigit(code.charAt(5));
		
		// Verifie chaque cas du tableau booleenne
		for(boolean confirme : verifie){
			
			// Si une des confirmation contient une donnee invalide 
			if(!confirme)
				
				// Le code postal est invalide
				return false;
			
		}
		
		return true;
		
	}
	
	/**************************************************************
	 * Verification du code postal pour le client / livreur
	 **************************************************************/
	public boolean verifieCodePostal(String code){
		
		this.codePostal = code;
		boolean verifie = false;
		
		try {
			verifie = verifieCodePostal();
		} catch (StringIndexOutOfBoundsException 
					| ArrayIndexOutOfBoundsException exception) {
			System.out.println(exception);
		}

		return verifie;
		
	}

}
