/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ControlleurLivreur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package controlleur;

import commande.Cmd_Livreur;
import donneGenerique.Adresse;
import modele.ModeleLivreur;

/***************************************************************
 * Controlleur du livreur
 ***************************************************************/
public class ControlleurLivreur {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private ModeleLivreur modeleDonne;

	/***************************************************************
	 * Constructeur par defaut
	 * 
	 * @param modeleDonne 	Modele du livreur
	 * @param livreur 		L'utilisateur / Le livreur
	 ***************************************************************/
	public ControlleurLivreur(ModeleLivreur modeleDonne) {
		
		super();
		this.modeleDonne = modeleDonne;		
	}

	public ControlleurLivreur() {
		// sert au test
	}

	/***************************************************************
	 * Modifie l'index de la liste a la commande courante
	 * 
	 * @param index Index de la liste par rapport a la selection
	 ***************************************************************/
	public void commandeCourante(int index) {
		
		modeleDonne.getListCommande().setObject(index);
	}

	
	/****************************************************************
	 * Changement d'index selon le restaurant selectionne
	 * @param index
	 ****************************************************************/
	public void selectedCommandeIndexChange(int index) {
		modeleDonne.setCommandeSelectionner(index);
	}
	
	/***************************************************************
	 * Acceptation d'une tache de livraison
	 * 
	 * @return Message de confirmation
	 * @throws Exception 
	 ***************************************************************/
	public Boolean accepter() throws Exception {
		
		return new Cmd_Livreur().assignerCommande(
				modeleDonne.getCommandeSelectionner(), 
				modeleDonne.getLivreur());

	}
	
	/***************************************************************
	 * Creation du lien URL qui permet le visionnement du trajet 
	 * entre les deux adresses calcule dans Google Maps.
	 * 
	 * @return Le lien URL
	 ***************************************************************/
	public String creerURL(){
		
		//	return "http://maps.google.com/";
		
		return "http://maps.google.com/maps?saddr=" +
		modeleDonne.getLivreur().getAdresseActuelle().toStringSpaced()+
		"&daddr=" + 
		modeleDonne.getCommandeSelectionner().getRestaurant().getAdresse().toStringSpaced() +
		"&mrad=" +
		modeleDonne.getCommandeSelectionner().getAdresseLivraison().toStringSpaced();
		
	}
	
	
	public String TestcreerURL(Adresse livreur,Adresse restaurant,Adresse client){
		
		//	return "http://maps.google.com/";
		
		return "http://maps.google.com/maps?saddr=" +
		livreur.toStringSpaced()+
		"&daddr=" + 
		restaurant.toStringSpaced() +
		"&mrad=" +
		client.toStringSpaced();
		
	}
	
	
}
