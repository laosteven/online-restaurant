/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ControlleurMenu.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package controlleur;

import commande.Cmd_Menu;
import object.ItemMenu;
import modele.ModeleMenu;

/***************************************************************
 * Controlleur du menu
 ***************************************************************/
public class ControlleurMenu {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private ModeleMenu modele;
	private String messageErreur;

	/***************************************************************
	 * Constructeur Instancier modeleMenu
	 * 
	 * @param modele
	 *            Le modele retenu
	 ***************************************************************/
	public ControlleurMenu(ModeleMenu modele) {
		super();
		this.modele = modele;
	}

	/***************************************************************
	 * Assigner un nom au menu
	 * 
	 * @param nomMenu Le nom du menu
	 ***************************************************************/
	public Boolean assignerNom(String nomMenu) {
		if (nomMenu == null || nomMenu.isEmpty()) {
			messageErreur = "Veuillez inscrir un nom pour votre menu";
			return false;
		}
		modele.setNom(nomMenu);
		return true;
		

	}

	/***************************************************************
	 * Creation d'un nouveau plat dans le menu
	 * 
	 * @param itemAjouter 		Le plat a rajouter
	 * @return La premiere case defini s'il y a eu une erreur
	 *         (true si oui false si non).
	 *         La deuxieme case defini si l'item a
	 *         ete ajoute (true si oui false si non).
	 ***************************************************************/
	public Boolean[] nouveauPlat(ItemMenu itemAjouter) {
		
		// Initialisation du message d'erreur
		messageErreur = "";
		
		// Definition des cases de l'attribut 'erreurSurvenu'
		Boolean[] erreurSurvenu = new Boolean[2];
		erreurSurvenu[0] = true;
		erreurSurvenu[1] = false;
		
		// Si le nom est absent
		if (itemAjouter.getNom().isEmpty()) 
			messageErreur = "Le nom du plat est obligatoire, le plat n'a pas ete ajoute";
		
		// Si le prix est absent
		else if (itemAjouter.getPrix() == null)
			messageErreur = "Le prix est obligatoire, le plat n'a pas ete ajoute";
		
		// Si la description est absente
		else if (itemAjouter.getDescription().isEmpty())
		{messageErreur = "Le plat a ete ajoute sans description";
		modele.addItem(itemAjouter);
		erreurSurvenu[0] = true;
		erreurSurvenu[1] = true;

		}
			
		
		// Sinon si tous les cases sont remplies
		else {
			
			// Ajout du plat dans le modele
			modele.addItem(itemAjouter);

			// Redefinition des cases
			erreurSurvenu[0] = false;
			erreurSurvenu[1] = true;
		}

		return erreurSurvenu;

	}

	/***************************************************************
	 * Sauvegarde du menu
	 * 
	 * @return Confirmation de sauvegarde
	 ***************************************************************/
	public Boolean enregistrerMenu() {
		
		// Definition d'attribut
		Boolean aAjoute = false;
		
		// Si le restaurant ne possede pas de nom
		if (modele.getMenuRestaurant().getNom() == null) 
			messageErreur = "Le menu doit avoir un nom, il n'a pas �t� enregistr�";
		
		// S'il possede un nom
		else {
			
			// Ecraser les donnees
			modele.getRestaurantModifier().setMenu(modele.getMenuRestaurant());
			
			Cmd_Menu ajouterMenu = new Cmd_Menu();
			aAjoute = true;
			
			try {
				
				// Ajout d'un menu du restaurant
				ajouterMenu.ajouter(modele.getMenuRestaurant(), modele
						.getRestaurantModifier().getId());
				
			} catch (Exception erreur) {
				
				// Echec d'ajout
				aAjoute = false;
				
				// Nouveau message d'erreur
				messageErreur = 
						"Erreur critique, contactez l'administrateur, " +
						"le menu n'a pas pu etre sauvegarde dans la base de donnee";
				
				erreur.printStackTrace();
				
			} // Fin du 'try/catch'

		} // Fin du 'if/else'

		return aAjoute;

	}

	/***************************************************************
	 * Supprimer le plat selon l'index de la liste
	 * @param index Plat selectionne de la liste
	 ***************************************************************/
	public void supprimerItem(int index) {
		modele.removeItemAtIndex(index);
	}

	/***************************************************************
	 * Modification d'un plat du menu
	 * @param index 			Index de la liste a modifier
	 * @param itemaRemplacer	Information du plat modifiee
	 ***************************************************************/
	public Boolean[] modifierItem(int index, ItemMenu itemaRemplacer) {
	
		// Initialisation du message d'erreur
				messageErreur = "";
				
				// Definition des cases de l'attribut 'erreurSurvenu'
				Boolean[] erreurSurvenu = new Boolean[2];
				erreurSurvenu[0] = true;
				erreurSurvenu[1] = false;
		
		if (itemaRemplacer.getNom().isEmpty()) 
			messageErreur = "Le nom du plat est obligatoire, le plat n'a pas ete ajoute";
		
		// Si le prix est absent
		else if (itemaRemplacer.getPrix().equals(null))
			messageErreur = "Le prix est obligatoire, le plat n'a pas ete ajoute";
		
		// Si la description est absente
		else if (itemaRemplacer.getDescription().isEmpty())
		{messageErreur = "Le plat a ete ajoute sans description";
		modele.modifierItemMenu(index, itemaRemplacer);
		erreurSurvenu[0] = true;
		erreurSurvenu[1] = true;

		}
		
		

		// Sinon si tous les cases sont remplies
		else {
			
			// Ajout du plat dans le modele
			modele.modifierItemMenu(index, itemaRemplacer);

			// Redefinition des cases
			erreurSurvenu[0] = false;
			erreurSurvenu[1] = true;
		}

		return erreurSurvenu;

		
		
		
	
	}

	/***************************************************************
	 * Envoi du message d'erreur
	 * @return Le message d'erreur
	 ***************************************************************/
	public String getMessageErreur() {
		return messageErreur;
	}
	
	/*       m�thode de test
	 * 
	 * 
	 */

public Boolean[] TestnouveauPlat(ItemMenu itemAjouter) {
		
		// Initialisation du message d'erreur
		messageErreur = "";
		
		// Definition des cases de l'attribut 'erreurSurvenu'
		Boolean[] erreurSurvenu = new Boolean[2];
		erreurSurvenu[0] = true;
		erreurSurvenu[1] = false;
		
		// Si le nom est absent
		if (itemAjouter.getNom().isEmpty()) 
			messageErreur = "Le nom du plat est obligatoire, le plat n'a pas ete ajoute";
		
		// Si le prix est absent
		else if (itemAjouter.getPrix() == null)
			messageErreur = "Le prix est obligatoire, le plat n'a pas ete ajoute";
		
		// Si la description est absente
		else if (itemAjouter.getDescription().isEmpty())
		{messageErreur = "Le plat a ete ajoute sans description";
		modele.addItem(itemAjouter);
		erreurSurvenu[0] = true;
		erreurSurvenu[1] = true;

		}
			
		
		// Sinon si tous les cases sont remplies
		else {
			
			// Ajout du plat dans le modele
			//modele.addItem(itemAjouter);

			// Redefinition des cases
			erreurSurvenu[0] = false;
			erreurSurvenu[1] = true;
		}

		return erreurSurvenu;

	}
	

public Boolean[] TestmodifierItem(int index, ItemMenu itemaRemplacer) {
	
	// Initialisation du message d'erreur
			messageErreur = "";
			
			// Definition des cases de l'attribut 'erreurSurvenu'
			Boolean[] erreurSurvenu = new Boolean[2];
			erreurSurvenu[0] = true;
			erreurSurvenu[1] = false;
	
	if (itemaRemplacer.getNom().isEmpty()) 
		messageErreur = "Le nom du plat est obligatoire, le plat n'a pas ete ajoute";
	
	// Si le prix est absent
	else if (itemaRemplacer.getPrix().equals(null))
		messageErreur = "Le prix est obligatoire, le plat n'a pas ete ajoute";
	
	// Si la description est absente
	else if (itemaRemplacer.getDescription().isEmpty())
	{messageErreur = "Le plat a ete ajoute sans description";
	modele.modifierItemMenu(index, itemaRemplacer);
	erreurSurvenu[0] = true;
	erreurSurvenu[1] = true;

	}
	
	

	// Sinon si tous les cases sont remplies
	else {
		
		// Ajout du plat dans le modele
		//modele.modifierItemMenu(index, itemaRemplacer);

		// Redefinition des cases
		erreurSurvenu[0] = false;
		erreurSurvenu[1] = true;
	}

	return erreurSurvenu;

	
	
	

}


public Boolean TestenregistrerMenu() {
	
	// Definition d'attribut
	Boolean aAjoute = false;
	
	// Si le restaurant ne possede pas de nom
	if (modele.getMenuRestaurant().getNom() == null) 
		messageErreur = "Le menu doit avoir un nom, il n'a pas �t� enregistr�";
	
	// S'il possede un nom
	else {
		
		// Ecraser les donnees
		//modele.getRestaurantModifier().setMenu(modele.getMenuRestaurant());
		
		//Cmd_Menu ajouterMenu = new Cmd_Menu();
		aAjoute = true;
		
		try {
			
			// Ajout d'un menu du restaurant
			//ajouterMenu.ajouter(modele.getMenuRestaurant(), modele
				//	.getRestaurantModifier().getId());
			
		} catch (Exception erreur) {
			
			// Echec d'ajout
			aAjoute = false;
			
			// Nouveau message d'erreur
			messageErreur = 
					"Erreur critique, contactez l'administrateur, " +
					"le menu n'a pas pu etre sauvegarde dans la base de donnee";
			
			erreur.printStackTrace();
			
		} // Fin du 'try/catch'

	} // Fin du 'if/else'

	return aAjoute;

}

	
}
