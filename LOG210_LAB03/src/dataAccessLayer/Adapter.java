/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Adapter.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package dataAccessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import modele.ObjetListe;
import donneGenerique.Adresse;
import object.Commande;
import object.ItemCommande;
import object.ItemMenu;
import object.Client;
import object.Entrepreneur;
import object.Hierarchy;
import object.Livreur;
import object.Menu;
import object.Restaurant;
import object.Restaurateur;
import object.Utilisateur;
import object.AdresseClient;
import object.Commande.CommandeStatus;
import object.Hierarchy.HierachyType;

/*************************************************************
 * Adapteur entre Java - MySQL
 *************************************************************/
public class Adapter {

	/*************************************************************
	 * Recuperation de donnees.
	 * 
	 * @param data
	 *            La donnee a recuperer
	 * @return Le profil de l'utilisateur
	 * @throws SQLException
	 *             Gestion de probleme SQL
	 *************************************************************/
	public static Utilisateur DataToUser(ResultSet data) throws SQLException {

		// Creation d'un utilisateur
		Utilisateur user = new Utilisateur();

		// Gestion d'erreur
		try {

			// Si un profil est disponible
			while (data.next()) {

				// Definition du profil de l'utilisateur
				user.setIdUtilisateur(UUID.fromString((data
						.getString("IdUtilisateur"))));
				user.setNomUtilisateur(data.getString("NomUtilisateur"));
				user.setNom(data.getString("Nom"));
				user.setPrenom(data.getString("Prenom"));
				user.setHierarchyName(HierachyType.valueOf(data.getString("Name")));
			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return user;

	}

	/***************************************************************
	 * Transfert de donnees dans la liste de restaurants
	 * 
	 * @param data 				L'information du restaurant
	 * @return 					La liste de restaurant
	 * @throws SQLException		Exception SQL
	 ***************************************************************/
	public static List<Restaurant> DataToListRestaurant(ResultSet data)
			throws SQLException {

		List<Restaurant> list = new ArrayList<Restaurant>();

		// Gestion d'erreur
		try {

			while (data.next()) {

				Restaurant resto = new Restaurant();
				resto.setId(UUID.fromString((data.getString("idRestaurant"))));
				resto.setNom(data.getString("Nom"));
				resto.setTelephone(data.getString("Telephone"));
				resto.setTypeCuisine(data.getString("TypeCuisine"));
				resto.setIdAssociation(UUID.fromString((data.getString("idAssociation"))));
				Adresse restoAdresse = new Adresse();
				restoAdresse.setCodePostal(data.getString("codePostal"));
				restoAdresse.setIdAdresse(UUID.fromString((data.getString("idAddress"))));
				restoAdresse.setNo(data.getString("numero"));
				restoAdresse.setProvince(data.getString("province"));
				restoAdresse.setRue(data.getString("rue"));
				restoAdresse.setVille(data.getString("ville"));
				resto.setAdresse(restoAdresse);

				list.add(resto);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return list;
	}

	/***************************************************************
	 * Transfert de donnees dans la liste de restaurateurs
	 * 
	 * @param data 				L'information du restaurateurs
	 * @return 					La liste de restaurateurs
	 * @throws SQLException		Exception SQL
	 ***************************************************************/
	public static List<Restaurateur> DataToListRestaurateur(ResultSet data)
			throws SQLException {

		List<Restaurateur> list = new ArrayList<Restaurateur>(0);

		// Gestion d'erreur
		try {

			while (data.next()) {

				Restaurateur resto = new Restaurateur();
				resto.setId(UUID.fromString((data.getString("idRestaurateur"))));
				resto.setNom(data.getString("Nom"));
				resto.setPrenom(data.getString("Prenom"));
				resto.setIdUtilisateur(UUID.fromString(data.getString("idUtilisateur")));
				
				Adresse restoAdresse = new Adresse();
				restoAdresse.setCodePostal(data.getString("codePostal"));
				restoAdresse.setIdAdresse(UUID.fromString((data.getString("idAddress"))));
				restoAdresse.setNo(data.getString("numero"));
				restoAdresse.setProvince(data.getString("province"));
				restoAdresse.setRue(data.getString("rue"));
				restoAdresse.setVille(data.getString("ville"));
				resto.setAdresse(restoAdresse);

				list.add(resto);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return list;
	}

	/***************************************************************
	 * 
	 * @param data
	 * @return
	 ***************************************************************/
	public static List<Hierarchy> DataToHierachyList(ResultSet data) {

		List<Hierarchy> list = new ArrayList<Hierarchy>();

		// Gestion d'erreur
		try {

			while (data.next()) {

				Hierarchy hierarchy = new Hierarchy();

				hierarchy.setDescription(data.getString("Name"));
				hierarchy.setIdHierarchy(UUID.fromString((data
						.getString("idHierarchy"))));

				list.add(hierarchy);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return list;
	}

	public static Entrepreneur DataToEntrepreneur(ResultSet data) {

		// Creation d'un utilisateur
		Entrepreneur user = new Entrepreneur();

		// Gestion d'erreur
		try {

			// Si un profil est disponible
			while (data.next()) {

				user.setIdUtilisateur(UUID.fromString((data
						.getString("IdUtilisateur"))));
				user.setNomUtilisateur(data.getString("NomUtilisateur"));
				user.setNom(data.getString("Nom"));
				;
				user.setPrenom(data.getString("Prenom"));
				user.setHierarchyName(HierachyType.valueOf(data.getString("name")));
				user.setCourriel(data.getString("email"));
				user.setDateNaissance(data.getString("birthday"));
				user.setNumeroTel(data.getString("telNumber"));
				//user.setId(UUID.fromString((data.getString("IdEntrepreneur"))));

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return user;
	}

	public static ObjetListe DataToAddressList(ResultSet data) {

		ObjetListe list = new ObjetListe();

		// Gestion d'erreur
		try {

			while (data.next()) {

				AdresseClient address = new AdresseClient();
				Adresse addr = new Adresse();

				addr.setIdAdresse(UUID.fromString((data
						.getString("idAddress"))));
				addr.setNo(data.getString("numero"));
				addr.setCodePostal(data.getString("codePostal"));
				addr.setProvince(data.getString("province"));
				addr.setRue(data.getString("rue"));
				addr.setVille(data.getString("ville"));

				address.setAdresse(addr);
				address.setParD�faut(data.getBoolean("isDefault"));
				address.setIdAdresse(UUID.fromString((data
						.getString("idClientAddress"))));

				list.addEnd(address);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return list;
	}

	public static ObjetListe DataToListItemMenu(ResultSet data)
			throws SQLException {

		ArrayList<Object> list = new ArrayList<Object>();

		// Gestion d'erreur
		try {

			while (data.next()) {

				ItemMenu item = new ItemMenu();

				item.setNom(data.getString("name"));
				item.setIdItemMenu(UUID.fromString((data
						.getString("idMenuitem"))));
				item.setDescription(data.getString("description"));
				item.setPrix(data.getDouble("price"));

				list.add(item);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return utility.Utility.convertToObjectList(list);
	}

	public static Menu DataToMenu(ResultSet data) throws SQLException, Exception {

		// Creation d'un utilisateur
		Menu menu = new Menu();
		
		// Gestion d'erreur
		try {

			// Si un profil est disponible
			while (data.next()) {
				menu.setNom(data.getString("name"));
				menu.setIdMenu(UUID.fromString((data.getString("idMenu"))));
			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return menu;
	}

	public static Client DataToClient(ResultSet data) {

		// Creation d'un utilisateur
		Client user = new Client();

				// Gestion d'erreur
				try {

					// Si un profil est disponible
					while (data.next()) {

						user.setIdUtilisateur(UUID.fromString((data
								.getString("IdUtilisateur"))));
						user.setNomUtilisateur(data.getString("NomUtilisateur"));
						user.setNom(data.getString("Nom"));
						;
						user.setPrenom(data.getString("Prenom"));
						user.setHierarchyName(HierachyType.valueOf(data.getString("name")));
						user.setCourriel(data.getString("email"));
						user.setDateNaissance(data.getString("birthday"));
						user.setNumeroTel(data.getString("telNumber"));
						user.setId(UUID.fromString((data.getString("IdClient"))));

					}

				} catch (Exception event) {

					// Affichage du probleme
					System.out.println(event.toString());
				}

				return user;
	}
	
	public static Livreur DataToLivreur(ResultSet data){

		// Creation d'un utilisateur
		Livreur livreur = new Livreur();

				// Gestion d'erreur
				try {

					// Si un profil est disponible
					while (data.next()) {

						livreur.setIdUtilisateur(UUID.fromString((data
								.getString("IdUtilisateur"))));
						livreur.setNomUtilisateur(data.getString("NomUtilisateur"));
						livreur.setNom(data.getString("Nom"));
						;
						livreur.setPrenom(data.getString("Prenom"));
						livreur.setHierarchyName(HierachyType.valueOf(data.getString("name")));
						livreur.setId(UUID.fromString(data.getString("idLivreur")));
					/*	livreur.setCourriel(data.getString("email"));
						livreur.setDateNaissance(data.getString("birthday"));
						livreur.setNumeroTel(data.getString("telNumber"));
						
						
						Adresse adr = new Adresse();
						adr.setIdAdresse(UUID.fromString(data.getString("idAddress")));
						adr.setCodePostal(data.getString("codePostal"));
						adr.setNo(data.getString("numero"));
						adr.setProvince(data.getString("province"));
						adr.setRue(data.getString("rue"));
						adr.setVille(data.getString("ville"));
						*/
						
						
					}

				} catch (Exception event) {

					// Affichage du probleme
					System.out.println(event.toString());
				}

				return livreur;
	}

	public static List<Commande> DataTCommandeList(ResultSet data) {

		// Creation d'un utilisateur
		List<Commande> cmdList = new ArrayList<Commande>();

						// Gestion d'erreur
						try {

							// Si un profil est disponible
							while (data.next()) {

								Commande cmd = new Commande();
								
								cmd.setIdCommande(UUID.fromString((data.getString("idCommande"))));
								cmd.setDateCommande(data.getString("dateCommande"));
								cmd.setNoConfirmation(UUID.fromString(data.getString("confirmationNumber")));
								cmd.setStatus(CommandeStatus.valueOf(data.getString("status")));		
								
								
								Restaurant resto = new Restaurant();
																
								Adresse adrResto = new Adresse();
								
								adrResto.setIdAdresse(UUID.fromString(data.getString("restoIdAddress")));
								adrResto.setCodePostal(data.getString("restoCodePostal"));
								adrResto.setNo(data.getString("restoNumero"));
								adrResto.setProvince(data.getString("restoProvince"));
								adrResto.setRue(data.getString("restoRue"));
								adrResto.setVille(data.getString("restoVille"));
																
								resto.setAdresse(adrResto);
								resto.setNom(data.getString("restoName"));

								cmd.setRestaurant(resto);
								
																					
								Adresse adrCmd = new Adresse();

								adrCmd.setIdAdresse(UUID.fromString(data.getString("commandeIdAddress")));
								adrCmd.setCodePostal(data.getString("commandeCodePostal"));
								adrCmd.setNo(data.getString("commandeNumero"));
								adrCmd.setProvince(data.getString("restoProvince"));
								adrCmd.setRue(data.getString("commandeRue"));
								adrCmd.setVille(data.getString("commandeVille"));
								
								cmd.setAdresseLivraison(adrCmd);							
								
								cmdList.add(cmd);

							}

						} catch (Exception event) {

							// Affichage du probleme
							System.out.println(event.toString());
						}

						return cmdList;
	}

	public static List<ItemCommande> DataToItemCommande(ResultSet data) {
		
			// Creation d'un utilisateur
			List<ItemCommande> itemCmdList = new ArrayList<ItemCommande>();

						// Gestion d'erreur
						try {

							// Si un profil est disponible
							while (data.next()) {

								ItemCommande itemCmd = new ItemCommande();
								
								itemCmd.setQuantit�(Integer.parseInt(data.getString("quantity")));
								itemCmd.setIdItemCommande(UUID.fromString(data.getString("idMenuItem")));
								
								ItemMenu item = new ItemMenu();
								
								item.setNom(data.getString("name"));
								item.setIdItemMenu(UUID.fromString((data
										.getString("idMenuitem"))));
								item.setDescription(data.getString("description"));
								item.setPrix(data.getDouble("price"));
								
								itemCmd.setItemCommand�(item);

								itemCmdList.add(itemCmd);
							}

						} catch (Exception event) {

							// Affichage du probleme
							System.out.println(event.toString());
						}

						return itemCmdList;
	}

	public static Boolean BooleanFromData(ResultSet data) {
		
					// Creation d'un utilisateur
					Boolean flag = false;

								// Gestion d'erreur
								try {

									// Si un profil est disponible
									while (data.next()) {

										flag = data.getBoolean("@flag");
									}

								} catch (Exception event) {

									// Affichage du probleme
									System.out.println(event.toString());
								}

								return flag;
	}

	public static Restaurateur DataToRestaurant(ResultSet data) {

		Restaurateur resto = new Restaurateur();

		// Gestion d'erreur
		try {

			while (data.next()) {

				resto.setId(UUID.fromString((data.getString("idRestaurateur"))));
				resto.setNom(data.getString("Nom"));
				resto.setPrenom(data.getString("Prenom"));
				resto.setHierarchyName(HierachyType.valueOf(data.getString("name")));
				resto.setIdUtilisateur(UUID.fromString(data.getString("IdUtilisateur")));
				
				Adresse restoAdresse = new Adresse();
				restoAdresse.setCodePostal(data.getString("codePostal"));
				restoAdresse.setIdAdresse(UUID.fromString((data.getString("idAddress"))));
				restoAdresse.setNo(data.getString("numero"));
				restoAdresse.setProvince(data.getString("province"));
				restoAdresse.setRue(data.getString("rue"));
				restoAdresse.setVille(data.getString("ville"));
				resto.setAdresse(restoAdresse);

			}

		} catch (Exception event) {

			// Affichage du probleme
			System.out.println(event.toString());
		}

		return resto;
	}
}
