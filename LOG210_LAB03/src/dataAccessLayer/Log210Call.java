/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Log210Call.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package dataAccessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import object.Client;
import object.Commande.CommandeStatus;
import object.Livreur;
import object.Restaurant;
import object.Restaurateur;
import dataAccessLayer.MySqlConnector;
import dataAccessLayer.MySqlConnector.StoredProcParameters;
import donneGenerique.Adresse;

/*************************************************************
 * Gestion d'information a la base de donnee dans MySQL
 *************************************************************/
public class Log210Call {

	/*************************************************************
	 * Test d'appel.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param index 			Iterateur
	 * @return 				Confirmation du test
	 *************************************************************/
	public ResultSet getHelloWorld(MySqlConnector mySqlConnector, int index) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				2);

		listParam
				.add(new StoredProcParameters("var1", Integer.toString(index)));
		listParam.add(new StoredProcParameters("var2", Integer.toString(index
				+ index)));

		return mySqlConnector.executeStoredProc("DoingStuff", listParam);
		
	}

	/*************************************************************
	 * Ajout d'un restaurant dans la base de donnee.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param nom 				Nom du restaurant
	 * @param adresse 			Adresse du restaurant
	 * @param telephone 		Telephone du restaurant
	 * @param typecuisine 		Mets specifique du restaurant
	 * @param idassociation 	Association du restaurant
	 *************************************************************/
	public void ajoutRestaurant(MySqlConnector mySqlConnector, String nom,
			Adresse adresse, String telephone, String typecuisine, UUID idRestaurant,
			UUID idassociation) {

		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		// Definition du profil
		listParam.add(new StoredProcParameters("pNom", nom));
		listParam.add(new StoredProcParameters("pIdAdresse", adresse.getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pTelephone", telephone));
		listParam.add(new StoredProcParameters("pTypeCuisine", typecuisine));
		listParam.add(new StoredProcParameters("pIdRestaurant", idRestaurant.toString()));
		listParam.add(new StoredProcParameters("pIdAssociation", idassociation.toString()));
		
		listParam.add(new StoredProcParameters("pNumero", adresse.getNo()));
		listParam.add(new StoredProcParameters("pRue", adresse.getRue()));
		listParam.add(new StoredProcParameters("pVille", adresse.getVille()));
		listParam.add(new StoredProcParameters("pCodePostal", adresse.getCodePostal()));
		listParam.add(new StoredProcParameters("pProvince", adresse.getProvince()));
		
		
		
		mySqlConnector.executeStoredProc("Restaurant_Insert", listParam);
		
	}

	/*************************************************************
	 * Ajout d'un restaurateur dans la base de donnee.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param nom 			Nom du restaurateur
	 * @param adresse 			Adresse du restaurateur
	 * @param prenom 			Prenom du restaurateur
	 *************************************************************/
	public void ajoutRestaurateur(MySqlConnector mySqlConnector, String nom,
			Adresse adresse, String prenom, UUID id, UUID idUser) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		// Definition des parametres
		listParam.add(new StoredProcParameters("pNom", nom));
		
		// TODO Steven: David / Jean, verifiez si ca marche
		listParam.add(new StoredProcParameters("pIdAdresse", adresse.getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pPrenom", prenom));
		listParam.add(new StoredProcParameters("pId", id.toString()));

		//Adresse adr = client.getAdresseDefaut().getAdresse();
		
		listParam.add(new StoredProcParameters("pNumero", adresse.getNo()));
		listParam.add(new StoredProcParameters("pRue", adresse.getRue()));
		listParam.add(new StoredProcParameters("pVille", adresse.getVille()));
		listParam.add(new StoredProcParameters("pCodePostal", adresse.getCodePostal()));
		listParam.add(new StoredProcParameters("pProvince", adresse.getProvince()));
		
		listParam.add(new StoredProcParameters("pIdUtilisateur",idUser.toString() ));
		listParam.add(new StoredProcParameters("pUserName", prenom + nom));
		
		// Enregistrement
		mySqlConnector.executeStoredProc("Restaurateur_Insert", listParam);

	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param nom
	 * @param idMenu
	 * @param idRestaurant
	 ***************************************************************/
	public void ajoutMenu(MySqlConnector mySqlConnector, String nom,
			 UUID idMenu, UUID idRestaurant) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				2);

		// Definition des parametres
		listParam.add(new StoredProcParameters("pName", nom));
		listParam.add(new StoredProcParameters("pIdMenu", idMenu.toString()));
		listParam.add(new StoredProcParameters("pIdRestaurant", idRestaurant.toString()));
		

		// Enregistrement
		mySqlConnector.executeStoredProc("Menu_Insert", listParam);

	}
	

	public void ajoutMenuItem(MySqlConnector mySqlConnector, String nom, String description, Double price,

			 UUID idMenuItem, UUID idMenu) {
	

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				4);

		// Definition des parametres
		listParam.add(new StoredProcParameters("pName", nom));
		listParam.add(new StoredProcParameters("pIdMenuItem", idMenuItem.toString()));
		listParam.add(new StoredProcParameters("pIdMenu", idMenu.toString()));
		listParam.add(new StoredProcParameters("pDescription", description));
		listParam.add(new StoredProcParameters("pPrice", price.toString()));
		
		

		// Enregistrement
		mySqlConnector.executeStoredProc("MenuItem_Insert", listParam);

	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param nom
	 * @param idMenu
	 * @param idRestaurant
	 ***************************************************************/
	public void modifierMenu(MySqlConnector mySqlConnector, String nom,
			 UUID idMenu, UUID idRestaurant) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				2);

		// Definition des parametres
		listParam.add(new StoredProcParameters("pName", nom));
		listParam.add(new StoredProcParameters("pIdMenu", idMenu.toString()));
		listParam.add(new StoredProcParameters("pIdRestaurant", idRestaurant.toString()));
		

		// Enregistrement
		mySqlConnector.executeStoredProc("Menu_Update", listParam);

	}

	public void modifierMenuItem(MySqlConnector mySqlConnector, String nom, String description, Double price,
			 UUID idMenuItem, UUID idMenu) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				4);

		// Definition des parametres
		listParam.add(new StoredProcParameters("pName", nom));
		listParam.add(new StoredProcParameters("pIdMenuItem", idMenuItem.toString()));
		listParam.add(new StoredProcParameters("pIdMenu", idMenu.toString()));
		listParam.add(new StoredProcParameters("pDescription", description));
		listParam.add(new StoredProcParameters("pPrice", price.toString()));
		
		

		// Enregistrement
		mySqlConnector.executeStoredProc("MenuItem_Update", listParam);

	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param idMenu
	 ***************************************************************/
	public void supprimerMenu(MySqlConnector mySqlConnector,
			 UUID idMenu) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		// Definition des parametres
		listParam.add(new StoredProcParameters("pIdMenu", idMenu.toString()));
		
		

		// Enregistrement
		mySqlConnector.executeStoredProc("Menu_Delete", listParam);

	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param idMenuItem
	 ***************************************************************/
	public void supprimerMenuItem(MySqlConnector mySqlConnector,
			 UUID idMenuItem) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				);

		// Definition des parametres
		listParam.add(new StoredProcParameters("pIdMenuItem", idMenuItem.toString()));

		// Enregistrement
		mySqlConnector.executeStoredProc("MenuItem_Delete", listParam);

	}
	
	
	/*************************************************************
	 * Ajout d'un compte client dans la base de donnee.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param nom				Nom du client
	 * @param adresse 			Adresse du client
	 * @param dateNaissance   	Date de naissance du client
	 * @param telephone  		Numero de telephone du client
	 * @param motDePasse  		Mot de passe du client
	 *************************************************************/
	public void ajoutCompteClient(MySqlConnector mySqlConnector, Client client) {

		// Creation d'une liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		// Definition des parametres
		listParam.add(new StoredProcParameters("pIdClient", client.getId().toString()));
		listParam.add(new StoredProcParameters("pDateNaissance", client.getDateNaissance()));
		listParam.add(new StoredProcParameters("pTelephone", client.getNumeroTel()));
		listParam.add(new StoredProcParameters("pEmail", client.getCourriel()));
		listParam.add(new StoredProcParameters("pIdUtilisateur", client.getIdUtilisateur().toString()));
		
		listParam.add(new StoredProcParameters("pIdClientAddress", client.getAdresseDefaut().getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pIdAddress", client.getAdresseDefaut().getAdresse().getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pIsDefault", client.getAdresseDefaut().getAdrParDéfaut() ? "1":"0"));
		
		listParam.add(new StoredProcParameters("pPrenom", client.getPrenom()));
		listParam.add(new StoredProcParameters("pNom", client.getNom()));
		listParam.add(new StoredProcParameters("pIdHierarchy", "5d4fada7-54b2-423f-9bd7-e2068719b5f4"));
		listParam.add(new StoredProcParameters("pNomUtilisateur", client.getNomUtilisateur()));
		listParam.add(new StoredProcParameters("pMotDePasse", client.getMotDePasse()));
		
		Adresse adr = client.getAdresseDefaut().getAdresse();
		
		listParam.add(new StoredProcParameters("pNumero", adr.getNo()));
		listParam.add(new StoredProcParameters("pRue", adr.getRue()));
		listParam.add(new StoredProcParameters("pVille", adr.getVille()));
		listParam.add(new StoredProcParameters("pCodePostal", adr.getCodePostal()));
		listParam.add(new StoredProcParameters("pProvince", adr.getProvince()));

		// Enregistrement
		mySqlConnector.executeStoredProc("Client_Insert", listParam);

	}

	/*************************************************************
	 * Modification d'un restaurant dans la base de donnee
	 * par creation d'une nouvelle liste.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param nom 			Nom du restaurant
	 * @param adresse 			Adresse du restaurant
	 * @param telephone 		Telephone du restaurant
	 * @param typecuisine 		Mets specifique du restaurant
	 ***************************************************************/
	public void modifieRestaurant(MySqlConnector mySqlConnector, String nom,
			Adresse adresse, String telephone, String typecuisine, UUID idAsso, UUID idRestaurant) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				7);

		listParam.add(new StoredProcParameters("nom", nom));
		listParam.add(new StoredProcParameters("idAdresse", adresse.getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("telephone", telephone));
		listParam.add(new StoredProcParameters("typeCuisine", typecuisine));
		listParam.add(new StoredProcParameters("idAsso", idAsso.toString()));
		listParam.add(new StoredProcParameters("idRestaurant", idRestaurant.toString()));
		
		listParam.add(new StoredProcParameters("pNumero", adresse.getNo()));
		listParam.add(new StoredProcParameters("pRue", adresse.getRue()));
		listParam.add(new StoredProcParameters("pVille", adresse.getVille()));
		listParam.add(new StoredProcParameters("pCodePostal", adresse.getCodePostal()));
		listParam.add(new StoredProcParameters("pProvince", adresse.getProvince()));

		mySqlConnector.executeStoredProc("Restaurant_Update", listParam);

	}

	/*************************************************************
	 * Modification d'un restaurateur dans la base de donnee.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param nom 			Nom du restaurateur
	 * @param adresse 			Adresse du restaurateur
	 * @param prenom 			Prenom du restaurateur
	 * @param id the id
	 ***************************************************************/
	public void modifieRestaurateur(MySqlConnector mySqlConnector, String nom,
			String adresse, String prenom, UUID id) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				4);

		listParam.add(new StoredProcParameters("nom", nom));
		listParam.add(new StoredProcParameters("adresse", adresse));
		listParam.add(new StoredProcParameters("prenom", prenom));
		listParam.add(new StoredProcParameters("id", id.toString()));

		mySqlConnector.executeStoredProc("Restaurateur_Update", listParam);
	}

	/*************************************************************
	 * Supprimer un restaurant de la liste.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param identif  			Numero d'identification
	 *************************************************************/
	public void supprimeRestaurant(MySqlConnector mySqlConnector, Restaurant resto) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pId", resto.getId().toString()));
		listParam.add(new StoredProcParameters("pIdAddress", resto.getAdresse().getIdAdresse().toString()));

		mySqlConnector.executeStoredProc("Restaurant_Delete", listParam);

	}

	/*************************************************************
	 * Supprimer un restaurateur de la liste.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param index  			Numero d'identification
	 *************************************************************/
	public void supprimeRestaurateur(MySqlConnector mySqlConnector, Restaurateur restaurateur) {
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pId", restaurateur.getId().toString()));
		listParam.add(new StoredProcParameters("pIdUtilisateur", restaurateur.getIdUtilisateur().toString()));
		listParam.add(new StoredProcParameters("pIdAddress", restaurateur.getAdresse().getIdAdresse().toString()));

		mySqlConnector.executeStoredProc("Restaurateur_Delete", listParam);

	}

	/*************************************************************
	 * Methode de confirmation pour l'authentification de l'utilisateur.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @param userName 			Nom d'utilisateur
	 * @param password 			Mot de passe de l'utilisateur
	 * @return 					Confirmation d'acces
	 *************************************************************/
	public ResultSet authentication(MySqlConnector mySqlConnector,
			String userName, String password) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				2);

		listParam.add(new StoredProcParameters("pUser", userName));
		listParam.add(new StoredProcParameters("pPassword", password));

		return mySqlConnector.executeStoredProc("User_Authentication",
				listParam);
		
	}
	
	/*************************************************************
	 * Methode de selection des restaurants.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @return 					liste de restaurants
	 * *************************************************************/
	public ResultSet SelectionnerRestaurateurs(MySqlConnector mySqlConnector){		
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		return mySqlConnector.executeStoredProc("Restaurateur_SelectAll",listParam);
	}
	
	
	/*********************************`****************************
	 * Methode de selection des restaurateurs.
	 *
	 * @param mySqlConnector 	Connection SQL
	 * @return 					list de retaurateurs
	 *************************************************************/
	public ResultSet SelectionnnerRestaurants(MySqlConnector mySqlConnector){
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		return mySqlConnector.executeStoredProc("Restaurant_SelectAll",	listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @return
	 ***************************************************************/
	public ResultSet selectionnerCommande(MySqlConnector mySqlConnector){
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		return mySqlConnector.executeStoredProc("Commande_SelectAll",	listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param index
	 ***************************************************************/
	public void supprimeCommande(MySqlConnector mySqlConnector, UUID index) {
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				1);

		listParam.add(new StoredProcParameters("pIdCommande", index.toString()));

		mySqlConnector.executeStoredProc("Commande_Delete", listParam);
	}

	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param commandeId
	 * @param restaurantId
	 * @param commandeDate
	 * @param adresse
	 * @param status
	 * @param confirmationNumber
	 ***************************************************************/
	public void modifieCommande(MySqlConnector mySqlConnector, UUID commandeId, UUID restaurantId, 
			Date commandeDate, String adresse, Boolean status, String confirmationNumber) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				6);

		listParam.add(new StoredProcParameters("pIdCommande", commandeId.toString()));
		listParam.add(new StoredProcParameters("pIdRestaurant", restaurantId.toString()));
		listParam.add(new StoredProcParameters("pDateCommande", commandeDate.toString()));
		listParam.add(new StoredProcParameters("pAdresseCommande", adresse));
		listParam.add(new StoredProcParameters("pStatus", status.toString()));
		listParam.add(new StoredProcParameters("pConfirmationNumber", confirmationNumber));

		mySqlConnector.executeStoredProc("Commande_Update", listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param commandeId
	 * @param restaurantId
	 * @param commandeDate
	 * @param adresse
	 * @param status
	 * @param confirmationNumber
	 ***************************************************************/
	public void ajoutCommande(MySqlConnector mySqlConnector, UUID commandeId, UUID restaurantId, 
			String commandeDate, UUID adresse, CommandeStatus status, UUID confirmationNumber) {

		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdCommande", commandeId.toString()));
		listParam.add(new StoredProcParameters("pIdRestaurant", restaurantId.toString()));
		listParam.add(new StoredProcParameters("pDateCommande", commandeDate.toString()));
		listParam.add(new StoredProcParameters("pIdAdresseCommande", adresse.toString()));
		listParam.add(new StoredProcParameters("pStatus", status.toString()));
		listParam.add(new StoredProcParameters("pConfirmationNumber", confirmationNumber.toString()));

		mySqlConnector.executeStoredProc("Commande_Insert", listParam);
		
	}

	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @return
	 ***************************************************************/
	public ResultSet SelectionnerHierarchy(MySqlConnector mySqlConnector) {
		
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				0);

		return mySqlConnector.executeStoredProc("Hierarchy_SelectAll", listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param id
	 * @return
	 ***************************************************************/
	public ResultSet SelectionnnerMenu(MySqlConnector mySqlConnector, UUID id){
		
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				1);

		listParam.add(new StoredProcParameters("pId", id.toString()));

		return mySqlConnector.executeStoredProc("Menu_Select", listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param id
	 * @return
	 ***************************************************************/
	public ResultSet SelectionnnerMenuItem(MySqlConnector mySqlConnector, UUID id){
		
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				1);

		listParam.add(new StoredProcParameters("pId", id.toString()));

		return mySqlConnector.executeStoredProc("MenuItem_Select", listParam);
	}
	
	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param id
	 * @return
	 ***************************************************************/
	public ResultSet SelectionnnerEntrepreneur(MySqlConnector mySqlConnector, UUID id){
		
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>(
				1);

		listParam.add(new StoredProcParameters("pId", id.toString()));

		return mySqlConnector.executeStoredProc("Client_Select", listParam);
	}

	/***************************************************************
	 * 
	 * @param mySqlConnector
	 * @param id
	 * @return
	 ***************************************************************/
	public ResultSet SelectionnnerAddressClient(MySqlConnector mySqlConnector, UUID id) {
		
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdClient", id.toString()));

		return mySqlConnector.executeStoredProc("ClientAddress_Select", listParam);
	}
	
	public void AjouterCommandeItem(MySqlConnector mySqlConnector, UUID menuItemId, UUID commandeId, UUID menuId, int quantity){
		
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pId", menuItemId.toString()));
		listParam.add(new StoredProcParameters("pIdCommande", commandeId.toString()));
		listParam.add(new StoredProcParameters("pIdMenuItem", menuId.toString()));
		listParam.add(new StoredProcParameters("pQuantity", String.valueOf(quantity)));

		mySqlConnector.executeStoredProc("CommandeItem_Insert", listParam);
	}

	public void supprimeClient(MySqlConnector mysqlConnector, Client client) {
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdUtilisateur", client.getIdUtilisateur().toString()));
		listParam.add(new StoredProcParameters("pIdClientAddress", client.getAdresseDefaut().getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pIdAddress", client.getAdresseDefaut().getAdresse().getIdAdresse().toString()));
		listParam.add(new StoredProcParameters("pIdClient", client.getId().toString()));

		mysqlConnector.executeStoredProc("Client_Delete", listParam);
		
	}

	public ResultSet SelectionnnerClient(MySqlConnector mysqlConnector,
			UUID id) {
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pId", id.toString()));

		return mysqlConnector.executeStoredProc("Client_Select", listParam);
	}
	
	public void ClientAddress_SelectDefault(MySqlConnector mysqlConnector, UUID clientId, UUID adresseDefaut){
		
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdClient", clientId.toString()));
		listParam.add(new StoredProcParameters("pIdAddressClient", adresseDefaut.toString()));

		mysqlConnector.executeStoredProc("ClientAddress_SelectDefault", listParam);
	}
	
	public void ClientAddress_Insert(MySqlConnector mysqlConnector, UUID clientId, Adresse adr){
		
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pId", UUID.randomUUID().toString()));
		listParam.add(new StoredProcParameters("pAddressId", UUID.randomUUID().toString()));
		listParam.add(new StoredProcParameters("pIsDefault", "1"));
		listParam.add(new StoredProcParameters("pIdClient", clientId.toString()));
		
		listParam.add(new StoredProcParameters("pNumero", adr.getNo()));
		listParam.add(new StoredProcParameters("pRue", adr.getRue()));
		listParam.add(new StoredProcParameters("pVille", adr.getVille()));
		listParam.add(new StoredProcParameters("pCodePostal", adr.getCodePostal()));
		listParam.add(new StoredProcParameters("pProvince", adr.getProvince()));

		mysqlConnector.executeStoredProc("ClientAddress_Insert", listParam);
		
	}

	public ResultSet selectionnerCommandeItem(MySqlConnector getMysqlConnector,
			UUID idItemMenu) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdCommande", idItemMenu.toString()));


		return getMysqlConnector.executeStoredProc("CommandeItem_select", listParam);
		
	}

	public ResultSet SelectionnnerLivreur(MySqlConnector getMysqlConnector,
			UUID idUtilisateur) {
		// Creation de liste
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdUtilisateur", idUtilisateur.toString()));

		return getMysqlConnector.executeStoredProc("Livreur_Select", listParam);
	}

	public ResultSet AssignerCommande(MySqlConnector getMysqlConnector,
			UUID idCommande, CommandeStatus status) throws SQLException {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();

		listParam.add(new StoredProcParameters("pIdCommande", idCommande.toString()));

		return getMysqlConnector.executeStoredProc("Commande_Assign", listParam);
		
	}

	public void EffectuerLivraison(MySqlConnector getMysqlConnector,
			UUID idCommande, UUID livraisonId, String date, Livreur livreur) {
	
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdLivraison", livraisonId.toString()));
		listParam.add(new StoredProcParameters("pIdCommande", idCommande.toString()));
		listParam.add(new StoredProcParameters("pDate", date));
		listParam.add(new StoredProcParameters("pIdLivreur", livreur.getId().toString()));

		getMysqlConnector.executeStoredProc("Livraison_Insert", listParam);	
		
	}

	public ResultSet SelectionnerRestaurateur(MySqlConnector getMysqlConnector,
			UUID idUtilisateur) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdUtilisateur", idUtilisateur.toString()));

		return getMysqlConnector.executeStoredProc("Restaurateur_Select", listParam);	
	}
	
	public ResultSet RestaurantSelect(MySqlConnector getMysqlConnector,
			UUID idUtilisateur) {

		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdUtilisateur", idUtilisateur.toString()));

		return getMysqlConnector.executeStoredProc("Restaurant_Select", listParam);	
	}

	public void Commande_PreparationStart(MySqlConnector getMysqlConnector,
			UUID idCommande) {
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdCommande", idCommande.toString()));

		getMysqlConnector.executeStoredProc("Commande_Preparation", listParam);	
		
	}

	public void Commande_PreparationEnd(MySqlConnector getMysqlConnector,
			UUID idCommande) {
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdCommande", idCommande.toString()));

		getMysqlConnector.executeStoredProc("Commande_Ready", listParam);	
		
	}

	public void SuprimmerCommande(MySqlConnector getMysqlConnector,
			UUID idCommande) {
		
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		
		listParam.add(new StoredProcParameters("pIdCommande", idCommande.toString()));

		getMysqlConnector.executeStoredProc("Commande_Delete", listParam);
		
	}

	public ResultSet selectionnerCommandeLivreur(
			MySqlConnector getMysqlConnector) {
		List<StoredProcParameters> listParam = new ArrayList<MySqlConnector.StoredProcParameters>();
		return getMysqlConnector.executeStoredProc("Commande_SelectAllLivreur",	listParam);

	}
	
	
	
}
