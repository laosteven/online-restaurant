/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		MySqlConnector.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package dataAccessLayer;

import java.util.Iterator;
import java.util.List;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


/*************************************************************
 * Connection a la base de donnee dans MySQL
 *************************************************************/
public class MySqlConnector {

	/************************************************************* 
	 * Attribut 
	 *************************************************************/
	private Connection _connection;

	/*************************************************************
	 * Constructeur par defaut: Connection a la base de donnee.
	 *
	 * @param connectionString 		Saisissement du port
	 * @throws Exception 
	 *************************************************************/
	public MySqlConnector(String connectionString) throws Exception {

		// Gestion d'erreur
		try {

			// Connection au BD
			Class.forName("com.mysql.jdbc.Driver");
			_connection = DriverManager.getConnection(connectionString);

		} catch (Exception exception) {
			//throw new Exception();	
		}
		
	}

	/*************************************************************
	 * Execution des procÚdures stockÚs.
	 *
	 * @param storedProcName 	Nom
	 * @param param 			Parametres enregistres
	 * @return 					Retourne si autorise, retourne rien
	 * si non-autorise
	 *************************************************************/
	public ResultSet executeStoredProc(String storedProcName,
			List<StoredProcParameters> param) {

		// Gestion d'erreur
		try {

			// Confirmation
			CallableStatement cmd = initCommand(storedProcName, param);
			ResultSet data = cmd.executeQuery();

			return data;

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return null;
	}

	/*************************************************************
	 * Appel disponible.
	 *
	 * @param name the name
	 * @param parameterList the parameter list
	 * @return the callable statement
	 * @throws SQLException 
	 *************************************************************/
	private CallableStatement initCommand(String name,
			List<StoredProcParameters> parameterList) throws SQLException {
		
		String paramCount = "";
		
		for (int i = 1; i <= parameterList.size(); i++) {
			
			if (i != parameterList.size()) 
				paramCount = paramCount + "?, ";
			else 
				paramCount = paramCount + "?";
		
		}
		
		CallableStatement command = _connection.prepareCall("{CALL " + name
				+ "(" + paramCount + ")}");

		for (Iterator<StoredProcParameters> i = parameterList.iterator(); i
				.hasNext();) {
			
			StoredProcParameters item = i.next();
			command.setString(item.getName(), item.GetValue());
			
		}

		return command;
	}

	/*************************************************************
	 * Fermeture de la connection.
	 *
	 * @throws SQLException 
	 *************************************************************/
	public void dispose() throws SQLException {

		// S'il existe une connection etablie en marche
		if (_connection != null && !_connection.isClosed())
			_connection.close();

	}

	/*************************************************************
	 * Sous-classe: Enregistrement des parametres
	 *************************************************************/
	public static class StoredProcParameters {
		
		/************************************************************* 
		 * Attributs 
		 *************************************************************/
		public String pValue;
		
		/** The P name. */
		public String pName;

		/*************************************************************
		 * Constructeur par defaut.
		 *
		 * @param name 		Nom de l'utilisateur
		 * @param value 	Valeur
		 *************************************************************/
		public StoredProcParameters(String name, String value) {
			pName = name;
			pValue = value;
		}

		/*************************************************************
		 * Accesseurs
		 *************************************************************/
		public String getName() {
			return pName;
		}

		/**
		 * Gets the value.
		 *
		 * @return the string
		 */
		public String GetValue() {
			return pValue;
		}

	}

}
