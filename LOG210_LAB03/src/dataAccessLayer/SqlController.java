/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		SqlController.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package dataAccessLayer;

import java.sql.ResultSet;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import donneGenerique.Adresse;
import modele.ObjetListe;
import object.Client;
import object.Commande;
import object.Commande.CommandeStatus;
import object.Entrepreneur;
import object.Hierarchy;
import object.ItemCommande;
import object.Livreur;
import object.Menu;
import object.Restaurant;
import object.Restaurateur;
import object.Utilisateur;
import object.ItemMenu;
import utility.Utility;

/*************************************************************
 * Controlleur du SQL
 *************************************************************/
public class SqlController {

	/************************************************************* 
	 * Attributs 
	 *************************************************************/
	private SqlManager _sqlManager;
	private Log210Call _log210Call;

	/*************************************************************
	 * Constructeur par defaut.
	 *
	 * @param sqlManager Gestion du SQL
	 *************************************************************/
	public SqlController(SqlManager sqlManager) {
		_log210Call = new Log210Call();
		_sqlManager = sqlManager;
	}

	/***************************************************************
	 * Etablissement de connection.
	 *
	 * @param param the param
	 * @return the int
	 * @throws Exception 
	 ***************************************************************/ 
	public void GetHelloWorld(int param) throws Exception {
		
		_log210Call.getHelloWorld(_sqlManager.GetMysqlConnector(), param);

	}

	/*************************************************************
	 * Ecriture dans la base de donnee pour un restaurant.
	 *
	 * @param nom 				Nom du restaurant
	 * @param adresse 			Adresse du restaurant
	 * @param telephone 		Telephone du restaurant
	 * @param typecuisine 		Mets specifique du restaurant
	 * @param idassociation 	Association du restaurant
	 * @return 					Confirmation d'enregistrement
	 * @throws Exception 
	 ***************************************************************/
	public void AjoutRestaurant(String nom, Adresse adresse, String telephone,
			String typecuisine, UUID idRestaurant, UUID idassociation) throws Exception {
		
		_log210Call.ajoutRestaurant(_sqlManager.GetMysqlConnector(), nom,
				adresse, telephone, typecuisine, idRestaurant, idassociation);
	
	}
	
	/***************************************************************
	 * 
	 * @param nom
	 * @param adresse
	 * @param dateNaissance
	 * @param telephone
	 * @param motDePasse
	 * @param email
	 * @param idUtilisateur
	 * @param idAddress
	 * @param id
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void AjoutCompteClient(Client client)
			throws Exception {

		_log210Call.ajoutCompteClient(_sqlManager.GetMysqlConnector(), client);
	}
	
	/***************************************************************
	 * 
	 * @param menu
	 * @param id
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void AjoutMenuComplet(Menu menu, UUID id) throws Exception {

		_log210Call.ajoutMenu(_sqlManager.GetMysqlConnector(), menu.getNom(),
				menu.getIdMenu(), id);

		menu.getListeItemMenu().setListBeginning();
		while (menu.getListeItemMenu().getObject() != null) {

			ItemMenu itemCourant = (ItemMenu) menu.getListeItemMenu()
					.getObjectAndIterate();
			_log210Call.ajoutMenuItem(_sqlManager.GetMysqlConnector(),
					itemCourant.getNom(), itemCourant.getDescription(),
					itemCourant.getPrix(), itemCourant.getIdItemMenu(),
					menu.getIdMenu());
		}
	}
	
	/***************************************************************
	 * 
	 * @param menu
	 * @param id
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void ModifierMenu(Menu menu, UUID id) throws Exception {

		_log210Call.modifierMenu(_sqlManager.GetMysqlConnector(),
				menu.getNom(), menu.getIdMenu(), id);
	}

	/***************************************************************
	 * 
	 * @param menu
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void SupprimerMenu(Menu menu) throws Exception {

		_log210Call.supprimerMenu(_sqlManager.GetMysqlConnector(),
				menu.getIdMenu());

	}

	/***************************************************************
	 * 
	 * @param menu
	 * @param id
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void AjoutMenuItem(ItemMenu menu, UUID id) throws Exception {

		_log210Call.ajoutMenuItem(_sqlManager.GetMysqlConnector(),
				menu.getNom(), menu.getDescription(), menu.getPrix(),
				menu.getIdItemMenu(), id);
	}

	/***************************************************************
	 * 
	 * @param menu
	 * @param id
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void ModifierMenuItem(ItemMenu menu, UUID id) throws Exception {

		_log210Call.modifierMenuItem(_sqlManager.GetMysqlConnector(),
				menu.getNom(), menu.getDescription(), menu.getPrix(),
				menu.getIdItemMenu(), id);
	}

	/***************************************************************
	 * 
	 * @param item
	 * @return
	 * @throws Exception
	 ***************************************************************/
	public void SupprimerMenuItem(ItemMenu item) throws Exception {

		_log210Call.supprimerMenu(_sqlManager.GetMysqlConnector(),
				item.getIdItemMenu());
	}

	/*************************************************************
	 * Authentification de l'utilisateur.
	 *
	 * @param userName 		Nom de l'utilisateur
	 * @param password 		Mot de passe
	 * @return 				Condition d'acces
	 * @throws Exception 
	 ***************************************************************/
	public Utilisateur Authentication(String userName, String password)
			throws Exception {

		// Recuperation de donnees
		ResultSet data = _log210Call.authentication(
				_sqlManager.GetMysqlConnector(), userName, password);
		
		// Confirmation de donnees par l'adapteur
		Utilisateur user =  Adapter.DataToUser(data);
		
		
		if (user.getNomUtilisateur() == "") 
		return null;
		
		return user;
		
	}

	/*************************************************************
	 * Ecriture dans la base de donnee pour un restaurateur.
	 *
	 * @param nom 			Nom du restaurateur
	 * @param adresse 		Adresse du restaurateur
	 * @param prenom 		Prenom du restaurateur
	 * @return 				Confirmation d'enregistrement
	 * @throws Exception 
	 ***************************************************************/
	public void AjoutRestaurateur(String nom, Adresse adresse, String prenom, UUID id, UUID idUtilisateur)
			throws Exception {
		
		_log210Call.ajoutRestaurateur(_sqlManager.GetMysqlConnector(), 
				nom,
				adresse, 
				prenom,
				id,
				idUtilisateur);
	}

	/*************************************************************
	 * Modification pour un restaurant.
	 *
	 * @param nom 				Nom du restaurant
	 * @param adresse 			Adresse du restaurant
	 * @param telephone 		Telephone du restaurant
	 * @param typecuisine 		Mets specifique du restaurant
	 * @return 					Confirmation d'enregistrement
	 * @throws Exception 
	 ***************************************************************/
	public void ModifieRestaurant(String nom, Adresse adresse, String telephone,
			String typecuisine, UUID idAsso, UUID idRestaurant)
			throws Exception {

		_log210Call.modifieRestaurant(_sqlManager.GetMysqlConnector(), nom,
				adresse, telephone, typecuisine, idAsso, idRestaurant);
	}

	/***************************************************************
	 * Modification pour un restaurateur.
	 *
	 * @param nom 				Nom du restaurateur
	 * @param adresse 			Adresse du restaurateur
	 * @param prenom 			Prenom du restaurateur
	 * @param id the id
	 * @return 					Confirmation d'enregistrement
	 * @throws Exception 
	 ***************************************************************/
	public void ModifieRestaurateur(Restaurateur restaurateur)
			throws Exception {
		
		_log210Call.supprimeRestaurateur(_sqlManager.GetMysqlConnector(), restaurateur);
		
		_log210Call.ajoutRestaurateur(_sqlManager.GetMysqlConnector(), 
				restaurateur.getNom(),
				restaurateur.getAdresse(), 
				restaurateur.getPrenom(),
				restaurateur.getId(),
				restaurateur.getIdUtilisateur());

	}

	/*************************************************************
	 * Supprimer un restaurant.
	 *
	 * @param identif 		Identification dans la liste
	 * @return 				Confirmation
	 * @throws Exception 
	 ***************************************************************/
	public void SupprimeRestaurant(Restaurant resto) throws Exception {
		
		_log210Call
				.supprimeRestaurant(_sqlManager.GetMysqlConnector(), resto);

	}

	/*************************************************************
	 * Supprimer un restaurateur.
	 *
	 * @param identif 		Identification dans la liste
	 * @return 				Confirmation
	 * @throws Exception 
	 ***************************************************************/
	public void SupprimeRestaurateur(Restaurateur restaurateur) throws Exception {
		
		_log210Call.supprimeRestaurateur(_sqlManager.GetMysqlConnector(), restaurateur);
		
	}
	
    /***************************************************************
     * Selectionnner restaurateurs.
     *
     * @return the list
     * @throws Exception the exception
     ***************************************************************/
	public List<Restaurateur> SelectionnnerRestaurateurs() throws Exception {
		ResultSet data = _log210Call.SelectionnerRestaurateurs(_sqlManager
				.GetMysqlConnector());

		return Adapter.DataToListRestaurateur(data);

	}

    /***************************************************************
     * Selectionnner restaurants.
     *
     * @return the list
     * @throws Exception the exception
     ***************************************************************/
	public List<Restaurant> SelectionnnerRestaurants() throws Exception {
		ResultSet data = _log210Call.SelectionnnerRestaurants(_sqlManager
				.GetMysqlConnector());
		
		List<Restaurant> restoList = Adapter.DataToListRestaurant(data);
		
		for (Restaurant restaurant : restoList) {
			restaurant.setMenu(SelectionnnerMenu(restaurant.getId()));			
		}
		
		return restoList;
	}
    
    /***************************************************************
     * 
     * @return
     * @throws Exception
     ***************************************************************/
    public List<Hierarchy> SelectionnerHierarchy() throws Exception{
    	
    	ResultSet data = _log210Call.SelectionnerHierarchy(_sqlManager.GetMysqlConnector());
    	
    	return Adapter.DataToHierachyList(data);
    }
    
    /***************************************************************
     * 
     * @param id
     * @return
     * @throws Exception
     ***************************************************************/
    public Client Selectionner_Client(UUID id) throws Exception{
    	
    	ResultSet data = _log210Call.SelectionnnerClient(_sqlManager.GetMysqlConnector(), id);
    	
    	Client client = Adapter.DataToClient(data);
    	
    	ObjetListe listAdress = new ObjetListe();
       	
       	data = _log210Call.SelectionnnerAddressClient(_sqlManager.GetMysqlConnector(), client.getId());
       	 
       	listAdress = Adapter.DataToAddressList(data);
       	
       	client.setListAdresse(listAdress);

    	return client;	
    }

    public void AjoutCommande(Commande cmd) throws Exception{
        
    	_log210Call.ajoutCommande(_sqlManager.GetMysqlConnector(), cmd.getIdCommande(), cmd.getRestaurant().getId(), 
    			cmd.getDateCommande(), cmd.getAdresseLivraison().getIdAdresse(), cmd.getStatus(), cmd.getNoConfirmation());
    	
    	cmd.getListItem().setListBeginning();
    	while(cmd.getListItem().getObject() != null){
    		ItemCommande item = (ItemCommande) cmd.getListItem().getObjectAndIterate();
    		_log210Call.AjouterCommandeItem(_sqlManager.GetMysqlConnector(), UUID.randomUUID(), cmd.getIdCommande(), item.getItemCommand�().getIdItemMenu(), item.getQuantit�());
    	}
    }

	public ObjetListe SelectionnnerItemMenu(UUID id) throws Exception {
		
		ResultSet data = _log210Call.SelectionnnerMenuItem(_sqlManager.GetMysqlConnector(), id);
    	
		ObjetListe ListItemMenu = new ObjetListe();
		
    	ListItemMenu = Adapter.DataToListItemMenu(data);
    	
 
    	return ListItemMenu;
	}
	
	public Menu SelectionnnerMenu(UUID id) throws Exception {
		
		ResultSet data = _log210Call.SelectionnnerMenu(_sqlManager.GetMysqlConnector(), id);
    	Menu menu = new Menu();
		menu = Adapter.DataToMenu(data);
		
		menu.setListeItemMenu(SelectionnnerItemMenu(menu.getIdMenu()));
    	 
    	return menu;
	}

	public void SuprimmerClient(Client client) throws Exception {

		_log210Call.supprimeClient(_sqlManager.GetMysqlConnector(), client);
		
	}

	public Entrepreneur Selectionner_Entrepreneur(UUID idUtilisateur) throws Exception {

		ResultSet data = _log210Call.SelectionnnerEntrepreneur(_sqlManager.GetMysqlConnector(), idUtilisateur);
    	
    	Entrepreneur client = Adapter.DataToEntrepreneur(data);

    	return client;
	}

	public void ChangerAdresseDefautClient(UUID clientId, UUID adresseId) throws Exception{
		_log210Call.ClientAddress_SelectDefault(_sqlManager.GetMysqlConnector(), clientId, adresseId);
	}
	
	public void AjouterAdresseDefautClient(UUID clientId, Adresse adr) throws Exception{
		_log210Call.ClientAddress_Insert(_sqlManager.GetMysqlConnector(),clientId, adr);
	}

	public Livreur Selectionner_Livreur(UUID idUtilisateur) throws Exception {

		ResultSet data = _log210Call.SelectionnnerLivreur(_sqlManager.GetMysqlConnector(), idUtilisateur);
    	
    	return Adapter.DataToLivreur(data);
	}

	public List<Commande> selectionnerCommande() throws Exception {

		ResultSet data = _log210Call.selectionnerCommande(_sqlManager.GetMysqlConnector());
		List<Commande> list = Adapter.DataTCommandeList(data);
		
		for (Commande commande : list) {
			
			ObjetListe listItem = Utility.convertToObjectList(SelectionnerCommandeItem(commande.getIdCommande()));
			commande.setListItem(listItem);
		}
		return list;
	}
	
	public List<ItemCommande> SelectionnerCommandeItem(UUID idItemMenu) throws Exception{

		ResultSet data = _log210Call.selectionnerCommandeItem(_sqlManager.GetMysqlConnector(), idItemMenu);

		return Adapter.DataToItemCommande(data);
		
	}
	
	public Boolean AssignerCommande(UUID idCommande, CommandeStatus status)throws Exception{
		
		ResultSet data = _log210Call.AssignerCommande(_sqlManager.GetMysqlConnector(),idCommande,status);
		
		return Adapter.BooleanFromData(data);
	}

	public void EffectuerLivraison(UUID idCommande, Livreur livreur) throws Exception {

		_log210Call.EffectuerLivraison(_sqlManager.GetMysqlConnector(), idCommande,UUID.randomUUID(), DateTime.now().toString(), livreur);		
	}

	public Restaurateur Selectionner_Restaurateur(UUID idUtilisateur) throws Exception {

		ResultSet data = _log210Call.SelectionnerRestaurateur(_sqlManager.GetMysqlConnector(), idUtilisateur);
    	
    	return Adapter.DataToRestaurant(data);
	}

	public List<Restaurant> SelectionnnerRestaurantsFromUser(UUID userId) throws Exception {

		ResultSet data = _log210Call.RestaurantSelect(_sqlManager.GetMysqlConnector(), userId);
    	
    	return Adapter.DataToListRestaurant(data);
	}

	public void CommandePreparationStart(UUID idCommande) throws Exception {
		_log210Call.Commande_PreparationStart(_sqlManager.GetMysqlConnector(),idCommande);
		
	}

	public void CommandePreparationEnd(UUID idCommande) throws Exception {
		_log210Call.Commande_PreparationEnd(_sqlManager.GetMysqlConnector(),idCommande);
		
	}

	public void SuprimmerCommande(Commande commande) throws Exception {
		_log210Call.SuprimmerCommande(_sqlManager.GetMysqlConnector(),commande.getIdCommande());
		
	}

	public List<Commande> selectionnerCommandeLivreur() throws Exception {
		ResultSet data = _log210Call.selectionnerCommandeLivreur(_sqlManager.GetMysqlConnector());
		List<Commande> list = Adapter.DataTCommandeList(data);
		
		for (Commande commande : list) {
			
			ObjetListe listItem = Utility.convertToObjectList(SelectionnerCommandeItem(commande.getIdCommande()));
			commande.setListItem(listItem);
		}
		return list;
	}
}
	
    
