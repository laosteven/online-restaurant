/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		SqlManager.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package dataAccessLayer;

import java.sql.SQLException;

/**
 * The Class SqlManager.
 */
public class SqlManager {

	/************************************************************* 
	 * Attributs
	 ************************************************************/
	private MySqlConnector _mysqlConnector;

	/** The _my sql connector string. */
	private String _mySqlConnectorString;

	/*************************************************************
	 * Constructeur: Nouvelle connexion.
	 * 
	 * @param mySqlConnectorString
	 *            connexion au port
	 *************************************************************/
	// / <summary>
	// / sql manager to connect to the database
	// / </summary>
	public SqlManager(String mySqlConnectorString) {

		_mySqlConnectorString = mySqlConnectorString;

	}

	/*************************************************************
	 * Constructeur par defaut Etablissement de connexion au SQL
	 *************************************************************/
	public SqlManager() {

		
		_mySqlConnectorString = "jdbc:mysql://localhost/log210_db?"
				+ "user=root&password=admin";
		/*/
		_mySqlConnectorString = "jdbc:mysql://70.82.32.155:3328/log210_db?"
				+ "user=root&password=admin";
		 */
		/*
		_mySqlConnectorString = "jdbc:mysql://70.82.32.155:3328/log210_db?"
				+ "user=root&password=admin";
		*/		
				
		//connection pour jean
		
	}

	/************************************************************* 
	 * Initiateur de connection SQL.
	 * 
	 * @throws Exception
	 *************************************************************/
	private void InitMysqlConnector() throws Exception {

		String mySqlConnectorString = _mySqlConnectorString;

		if (mySqlConnectorString.isEmpty() && mySqlConnectorString != null)
			throw new Exception();

		_mysqlConnector = new MySqlConnector(_mySqlConnectorString);

	}

	/************************************************************* 
	 * Fermeture
	 * de la connexion.
	 * 
	 * @throws SQLException
	 ************************************************************/
	public void Dispose() throws SQLException {
		if (_mysqlConnector != null) {
			_mysqlConnector.dispose();
		}
	}

	/*************************************************************
	 * Recuperation de la connexion.
	 * 
	 * @return La connexion en cours
	 * @throws Exception
	 *************************************************************/
	public MySqlConnector GetMysqlConnector() throws Exception {

		if (_mysqlConnector == null)
			InitMysqlConnector();

		return _mysqlConnector;

	}

}
