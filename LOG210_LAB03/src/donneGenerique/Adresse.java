/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Adresse.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package donneGenerique;

import java.util.UUID;

/***************************************************************
 * Classe de gestion d'adresses geographiques des utilisateurs
 * incrits dans le systeme.
 ***************************************************************/
public class Adresse {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private String no;
	private String rue;
	private String ville;
	private String province;
	private String codePostal;
	private UUID idAdresse;

	/***************************************************************
	 * Accesseurs
	 ***************************************************************/

	public String getNo() {
		return no;
	}

	public String getRue() {
		return rue;
	}

	public String getVille() {
		return ville;
	}

	public String getProvince() {
		return province;
	}

	public String getCodePostal() {
		return codePostal;
	}
	
	public UUID getIdAdresse() {
		return this.idAdresse;
	}
	
	/***************************************************************
	 * Mutateurs
	 ***************************************************************/
	public void setNo(String no) {
		this.no = no;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	
	public void setIdAdresse(UUID idAdresse) {
		this.idAdresse = idAdresse;
	}

	
	/***************************************************************
	 * Constructeur par defaut
	 ***************************************************************/
	public Adresse() {

	}

	/***************************************************************
	 * Constructeur
	 * 
	 * @param no 			Numero de l'adresse
	 * @param rue			Rue de l'adresse
	 * @param ville			Ville de l'adresse
	 * @param province		Province de l'adresse
	 * @param codePostal	Code postal de l'adresse
	 ***************************************************************/
	public Adresse(UUID id, String no, String rue, String ville, String province,
			String codePostal) {

		this.idAdresse = id;
		this.no = no;
		this.rue = rue;
		this.ville = ville;
		this.province = province;
		this.codePostal = codePostal;
		
	}

	/***************************************************************
	 * Distinction des donnees d'une adresse
	 * 
	 * @param dataToParse
	 ***************************************************************/
	public Adresse(String dataToParse) {

		String[] dataParsed = dataToParse.split("/");
		no = dataParsed[0];
		rue = dataParsed[1];
		ville = dataParsed[2];
		province = dataParsed[3];
		codePostal = dataParsed[4];

	}

	/***************************************************************
	 * @return L'adresse complet separe par des '/' pour chaque 
	 * 			information
	 ***************************************************************/
	@Override
	public String toString() {
		return no + '/' + rue + '/' + ville + '/' + province + '/' + codePostal;
	}

	/***************************************************************
	 * @return L'adresse complet
	 ***************************************************************/
	public String toStringSpaced() {
		return no + ' ' + rue + ' ' + ville + ' ' + province + ' ' + codePostal;
	}

	/***************************************************************
	 * Methode de comparaison entre adresses
	 * 
	 * @param comparer L'adresse a comparer
	 * @return Determine si les deux adresses sont similaires
	 ***************************************************************/
	public Boolean compare(Adresse comparer) {
		
		Boolean equals = false;
		
		if (this.no.equals(comparer.no)
				&& this.codePostal.equals(comparer.codePostal)
				&& this.province.equals(comparer.province)
				&& this.rue.equals(comparer.rue)
				&& this.ville.equals(comparer.ville)) {
			equals = true;
		}
		
		return equals;
	}
	
	/***************************************************************
	 * Methode de comparaison entre adresses
	 * 
	 * @param comparer L'adresse a comparer en format String
	 * @return Determine si les deux adresses sont similaires
	 ***************************************************************/
	public Boolean compareString(String comparer) {
		
		Boolean equals = false;
		
		String[] adresseRef = comparer.split(" ");
		
		if (this.no.equals(adresseRef[0])
				&& this.rue.equals(adresseRef[1])
				&& this.ville.equals(adresseRef[2])
				&& this.province.equals(adresseRef[3])
				&& this.codePostal.equals(adresseRef[4])) {
			equals = true;
		}
		
		return equals;
	}

}
