/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		DonneeGlobal.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package donneGenerique;

import object.Utilisateur;
import modele.ObjetListe;

/******************************************************************
 * Suit le patron Singleton
 * 
 * Classe qui gere les donnees globaux du logiciel
 ******************************************************************/
public class DonneeGlobal {

	/**************************************************************
	 * Attributs
	 *************************************************************/
	static private DonneeGlobal globalData = null;

	private  Utilisateur utilisateurConnecte;
	private  ObjetListe restaurantList = new ObjetListe();
	private  ObjetListe restaurateurList = new ObjetListe();

	/**************************************************************
	 * Accesseurs
	 *************************************************************/
	public ObjetListe getRestaurantList() {
		return restaurantList;
	}

	public ObjetListe getRestaurateurList() {
		return restaurateurList;
	}
	
	public Utilisateur getUtilisateurConnecte() {
		return utilisateurConnecte;
	}

	/**************************************************************
	 * Mutateur
	 **************************************************************/
	public void setUtilisateurConnecte(Utilisateur utilisateurConnecte) {
		this.utilisateurConnecte = utilisateurConnecte;
	}

	public void setRestaurantList(ObjetListe restaurantList) {
		this.restaurantList = restaurantList;
	}

	public void setRestaurateurList(ObjetListe restaurateurList) {
		this.restaurateurList = restaurateurList;
	}

	/**************************************************************
	 * Constructeur par defaut
	 **************************************************************/
	private DonneeGlobal() {

	}

	/**************************************************************
	 * Singleton
	 **************************************************************/
	public synchronized static DonneeGlobal getInstance() {
		if (globalData == null)
			globalData = new DonneeGlobal();

		return globalData;
	}

	/**************************************************************
	 * Nettoyer la liste
	 **************************************************************/
	public void clearList() {
		restaurantList.clearList();
		restaurateurList.clearList();
	}

}
