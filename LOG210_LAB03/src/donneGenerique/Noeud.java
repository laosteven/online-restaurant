/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Noeud.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package donneGenerique;

/***************************************************************
 * Permet la gestion de noeud dans une liste chainee.
 ***************************************************************/
public class Noeud {

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	/**************************************************************
	 * Attributs 
	 **************************************************************/
	// La liste peut les utiliser directement via un objet
	// donc pas besoin d'accesseurs et de mutateurs
	private Object value;
	
	private Noeud previousNoeud;
	private Noeud nextNoeud;

	/**************************************************************
	 * Constructeur par copie d'attributs.
	 *
	 * @param data the data
	 * @param previousNoeud the previous noeud
	 * @param nextNoeud 
	 **************************************************************/
	public Noeud(Object data, Noeud previousNoeud, Noeud nextNoeud) {
		value = data;
		this.previousNoeud = previousNoeud;
		this.nextNoeud = nextNoeud;
	}

	/**************************************************************
	 * @return le noeud suivant
	 **************************************************************/
	public Noeud getNextNoeud() {
		return this.nextNoeud;
	}

	/**************************************************************
	 * @return la valeur du noeud
	 **************************************************************/
	public Object getNoeudValue() {
		return this.value;
	}

	/**************************************************************
	 * @return le noeud precedent
	 **************************************************************/
	public Noeud getPreviousNoeud() {
		return this.previousNoeud;
	}

	/**************************************************************
	 * Etablit le noeud suivant.
	 *
	 * @param nextNoeud Le prochain noeud a etablir
	 **************************************************************/
	public void setNextNoeud(Noeud nextNoeud) {
		this.nextNoeud = nextNoeud;
	}

	/**************************************************************
	 * Etablit le noeud precedent.
	 *
	 * @param previousNoeud Le noeud precedent a etablir
	 **************************************************************/
	public void setPreviousNoeud(Noeud previousNoeud) {
		this.previousNoeud = previousNoeud;
	}
	
}
