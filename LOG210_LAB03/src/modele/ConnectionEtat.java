/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ConnectionEtat.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package modele;

import java.util.Observable;

import object.Utilisateur;

/******************************************************************
 * Classe qui verifie l'etat de connection de l'utilisateur
 ******************************************************************/
public class ConnectionEtat extends Observable {

	/**************************************************************
	 * Attributs 
	 **************************************************************/
	private boolean connectionState;
	private Utilisateur loggedUser;
	
	/****************************************************************
	 * Constructeur
	 * 
	 * Initialise l'etat de connection a fausse.
	 ****************************************************************/
	public ConnectionEtat() {
		super();
		connectionState = false;
	}
	
	/**************************************************************.
	 * @return Information de connection sur l'utilisateur
	 **************************************************************/
	public Utilisateur getLoggedUser() {
		return loggedUser;
	}

	/**************************************************************
	 * Redefinir l'etat de connection.
	 *
	 * @param loggedUser L'etat de connection a reprendre
	 **************************************************************/
	public void setLoggedUser(Utilisateur loggedUser) {
		this.loggedUser = loggedUser;
	}

	/**************************************************************
	 * @return L'etat de connection actuel
	 **************************************************************/
	public boolean isConnectionState() {
		return connectionState;
	}

	/**************************************************************
	 * Recupere les donnees de connection de l'utilisateur.
	 *
	 * @param connectionState the new connection state
	 * @return Etat de connection
	 **************************************************************/
	public void setConnectionState(boolean connectionState) {
		this.connectionState = connectionState;
		super.setChanged();
		super.notifyObservers();
	}

}
