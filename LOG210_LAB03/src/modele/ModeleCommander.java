/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ModeleCommander.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package modele;

import java.util.Observable;

import object.Client;
import object.Restaurant;
import object.ItemCommande;
import object.ItemMenu;

/***************************************************************
 * Modele regroupant les commandes pour chaque utilisateur
 * dans le systeme
 ***************************************************************/
public class ModeleCommander extends Observable {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private Restaurant selectedRestaurant;
	private ObjetListe listRestaurant;
	private ObjetListe commande;
	private ItemMenu selectedItemMenu;
	private float total;
	private Client clientEnCours;


	/***************************************************************
	 * Accesseurs
	 ***************************************************************/
	public Client getClientEnCours() {
		return clientEnCours;
	}

	public ObjetListe getCommande() {
		return commande;
	}
	
	public ItemMenu getSelectedItemMenu() {
		return selectedItemMenu;
	}
	

	public Float getTotal() {
		return total;
	}
	
	public ObjetListe getListRestaurant() {
		return listRestaurant;
	}
	
	public Restaurant getSelectedRestaurant() {
		return selectedRestaurant;
	}
	
	
	/***************************************************************
	 * Mutateurs
	 ***************************************************************/
	public void setSelectedItemMenu(int index) {
		selectedRestaurant.getMenu().getListeItemMenu().setObject(index);
		selectedItemMenu = (ItemMenu) selectedRestaurant.getMenu().getListeItemMenu().getObject();
		
	
	}

	public void setSelectedRestaurant(int index) {
		listRestaurant.setObject(index);
		selectedRestaurant = (Restaurant) listRestaurant.getObject();
		super.setChanged();
		super.notifyObservers();
	}
	

	/***************************************************************
	 * Constructeur: Instancie la liste d'objet et le client connecte
	 * 
	 * @param listRestaurant 	La liste de restaurant disponible
	 * @param clientConnecte 	Le client connecte au systeme
	 ***************************************************************/
	public ModeleCommander(ObjetListe listRestaurant, Client clientConnecte) {
		super();
		this.listRestaurant = listRestaurant;
		this.clientEnCours = clientConnecte;
		this.commande = new ObjetListe();
		//this.selectedRestaurant = (Restaurant)listRestaurant.getObject();
	}

	/***************************************************************
	 * Constructeur: Instancie une liste de restaurant
	 ***************************************************************/
	public ModeleCommander() {
		super();
		this.listRestaurant = new ObjetListe();
		commande = new ObjetListe();
	}


	/***************************************************************
	 * Ajout d'un nouveau plat
	 * @param itemAjouter 	Le plat a rajouter dans la liste
	 ***************************************************************/
	public void addItemCommande(ItemCommande itemAjouter) {
		commande.addEnd(itemAjouter);
		super.setChanged();
		super.notifyObservers();
	}

	

	/***************************************************************
	 * Supprimer une commande
	 * @param index 	Index selectionne de la liste
	 ***************************************************************/
	public void removeCommandeAtIndex(int index) {
		commande.setObject(index);
		ItemCommande commandeEnlever = (ItemCommande) commande.getObject();
		total = (float) (total - (commandeEnlever.getQuantit�() * commandeEnlever
				.getItemCommand�().getPrix()));
		commande.remove();
		super.setChanged();
		super.notifyObservers();
	}

	/***************************************************************
	 * Modification d'un plat selectionne de la liste
	 * @param index 				Index de la liste
	 * @param itemaRemplacer 		Le plat a modifier
	 ***************************************************************/
	public void modifierItemMenu(int index, ItemMenu itemaRemplacer) {
		commande.setObject(index);
		commande.replaceObject(itemaRemplacer);
		super.setChanged();
		super.notifyObservers();
	}

	/***************************************************************
	 * Ajouter une nouvelle commande dans le carnet
	 * @param commande 	La commande a rajouter dans le carnet
	 ***************************************************************/
	public void ajouter(ItemCommande commande) {
		this.commande.addEnd(commande);
		total += (commande.getQuantit�() * commande.getItemCommand�().getPrix());
		super.setChanged();
		super.notifyObservers();
	}

}
