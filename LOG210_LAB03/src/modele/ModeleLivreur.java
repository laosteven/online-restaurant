package modele;

import java.util.Observable;
import object.Commande;
import object.Livreur;

public class ModeleLivreur extends Observable {
	private Livreur livreur;
	private ObjetListe listCommande;
	private Commande commandeSelectionner;

	public ModeleLivreur(Livreur livreur, ObjetListe listCommande) {
		super();
		this.livreur = livreur;
		this.listCommande = listCommande;
	}

	public Livreur getLivreur() {
		return livreur;
	}

	public void setLivreur(Livreur livreur) {
		this.livreur = livreur;
		super.setChanged();
		super.notifyObservers();
	}

	public Commande getCommandeSelectionner() {
		return commandeSelectionner;
	}

	public void setCommandeSelectionner(int index) {
		this.listCommande.setObject(index);
		this.commandeSelectionner  = (Commande) listCommande.getObject();

		//super.setChanged();
		//super.notifyObservers();
	}

	public ObjetListe getListCommande() {
		return listCommande;
	}

	public void setListCommande(ObjetListe listCommande) {
		this.listCommande = listCommande;
		super.setChanged();
		super.notifyObservers();
	}

}
