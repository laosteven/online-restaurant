package modele;

import java.util.Observable;

import object.Menu;
import object.Restaurant;
import object.ItemMenu;

public class ModeleMenu extends Observable {
	private Menu menuRestaurant;
	private Restaurant restaurantModifier;

	public object.Menu getMenuRestaurant() {
		return menuRestaurant;
	}

	public void setMenu(Menu menuChanger) {
		menuRestaurant = menuChanger;

	}

	public ModeleMenu(Menu menuRestaurant, Restaurant restaurant) {
		super();
		this.menuRestaurant = menuRestaurant;
		restaurantModifier = restaurant;
	}

	public ModeleMenu(Restaurant restaurant) {
		super();
		this.menuRestaurant = restaurant.getMenu();
		restaurantModifier = restaurant;
	}

	public void setNom(String nom) {
		this.menuRestaurant.setNom(nom);
		super.setChanged();
		super.notifyObservers();

	}

	public void addItem(ItemMenu itemAjouter) {
		menuRestaurant.getListeItemMenu().addEnd(itemAjouter);
		menuRestaurant.getListeItemMenu().setListBeginning();
		super.setChanged();
		super.notifyObservers();
	}

	public void removeItemAtIndex(int index) {
		menuRestaurant.getListeItemMenu().setObject(index);
		menuRestaurant.getListeItemMenu().remove();
		super.setChanged();
		super.notifyObservers();
	}

	public void modifierItemMenu(int index, ItemMenu itemaRemplacer) {
		menuRestaurant.getListeItemMenu().setObject(index);
		menuRestaurant.getListeItemMenu().replaceObject(itemaRemplacer);
		super.setChanged();
		super.notifyObservers();
	}

	public Restaurant getRestaurantModifier() {
		return restaurantModifier;
	}
}
