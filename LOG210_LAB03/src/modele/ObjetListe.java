/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ObjetListe.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

/***************************************************************
 * Permet de faire la gestion d'une liste de formes
 * 
 * @author Guillaume Lepine
 ***************************************************************/
package modele;

import java.util.Observable;
import donneGenerique.Noeud;

/**************************************************************
 * Liste d'objet qui renferme les informations pour chaque restaurants et
 * restaurateurs.
 **************************************************************/
public class ObjetListe extends Observable {

	/*************************************************************
	 * Attributs
	 **************************************************************/
	private Noeud firstNoeud;
	private Noeud lastNoeud;
	private Noeud currentNoeud;

	private int nbElement = 0;
	private boolean atTheEnd = false;

	/**************************************************************
	 * Constructeur par defaut
	 **************************************************************/
	public ObjetListe() {

	}

	/**************************************************************
	 * Ajout au debut de la liste.
	 * 
	 * @param dataToAdd
	 **************************************************************/
	public void addBeginning(Object dataToAdd) {

		Noeud nodeToAdd = new Noeud(dataToAdd, null, firstNoeud);

		if (nbElement == 0) {

			firstNoeud = nodeToAdd;
			lastNoeud = firstNoeud;

		} else {

			firstNoeud.setPreviousNoeud(nodeToAdd);
			firstNoeud = nodeToAdd;

		}

		nbElement++;
		currentNoeud = firstNoeud;

	}

	/**************************************************************
	 * Ajoute a la fin de la liste.
	 * 
	 * @param dataToAdd
	 **************************************************************/
	public void addEnd(Object dataToAdd) {

		if (nbElement == 0) {

			addBeginning(dataToAdd);

		} else {

			Noeud nodeToadd = new Noeud(dataToAdd, lastNoeud, null);
			lastNoeud.setNextNoeud(nodeToadd);
			nbElement++;
			lastNoeud = nodeToadd;
			currentNoeud = lastNoeud;

		}

		super.setChanged();
		super.notifyObservers();
	}

	/***************************************************************
	 * Ajoute a la fin d la liste, utiliser seulement lorsque un nombre �lev� de
	 * Object doivent �tre ajouter cette m�thode permet de rafraichir seulement
	 * une fois tous les objets ajouter. ne pas oublier d'appeler la m�thode
	 * changeEnded
	 * 
	 * @param dataToAdd
	 ***************************************************************/

	public void addLargeAmount(Object dataToAdd) {

		if (nbElement == 0) {

			addBeginning(dataToAdd);

		} else {

			Noeud nodeToadd = new Noeud(dataToAdd, lastNoeud, null);
			lastNoeud.setNextNoeud(nodeToadd);
			nbElement++;
			lastNoeud = nodeToadd;
			currentNoeud = lastNoeud;

		}

	}

	/**************************************************************
	 * Rafraichit les observateurs, utiliser apres avoir utiliser
	 * addLargeAmount.
	 **************************************************************/

	public void changeEnded() {
		super.setChanged();
		super.notifyObservers();
	}

	/**************************************************************
	 * @return le nombre d'elements dans la liste
	 **************************************************************/
	public int getNbElement() {
		return this.nbElement;
	}

	/*************************************************************
	 * @return la valeur du noeud suivant
	 **************************************************************/
	public Object getNextNoeudValue() {

		Object nodeValue = null;

		if (currentNoeud.getNextNoeud() != null)
			nodeValue = currentNoeud.getNextNoeud().getNoeudValue();

		return nodeValue;
	}

	/**************************************************************
	 * @return le Object � l'index courant
	 **************************************************************/
	public Object getObject() {

		Object nodevalue = null;

		if (currentNoeud != null && atTheEnd == false)
			nodevalue = (Object) currentNoeud.getNoeudValue();

		return (Object) nodevalue;
	}

	public Object getObjectToAssociate() {

		return (Object) (Object) currentNoeud.getNoeudValue();
	}

	/**************************************************************
	 * @return le Object � l'index courant et passe au prochain index, utiliser
	 *         si on parcours la liste dans une boucle.Si on est rendu � la fin
	 *         de la liste la m�thode retournera null
	 **************************************************************/
	public Object getObjectAndIterate() {

		Object nodeValue = null;

		if (atTheEnd == false && currentNoeud != null) {

			nodeValue = (Object) currentNoeud.getNoeudValue();
			nextObject();

		}

		return (Object) nodeValue;
	}

	/**************************************************************
	 * Avance au prochain objet
	 **************************************************************/
	public void nextObject() {

		if (currentNoeud.getNextNoeud() != null)
			currentNoeud = currentNoeud.getNextNoeud();

		else
			atTheEnd = true;
	}

	/**************************************************************
	 * Recule a la forme precedente
	 **************************************************************/
	public void previousObject() {

		if (currentNoeud.getPreviousNoeud() != null) {

			currentNoeud = currentNoeud.getPreviousNoeud();
			atTheEnd = false;

		}

	}

	/**************************************************************
	 * Etablie le premier noeud
	 **************************************************************/
	public void setListBeginning() {
		currentNoeud = firstNoeud;
		atTheEnd = false;
	}

	/**************************************************************
	 * Place l'index courant � la fin de la liste
	 **************************************************************/
	public void setListEnd() {
		currentNoeud = lastNoeud;
	}

	/**************************************************************
	 * Etablie l'index du noeud.
	 * 
	 * @param index
	 **************************************************************/
	public void setObject(int index) {

		currentNoeud = firstNoeud;
		atTheEnd = false;

		for (int i = 0; i < index; i++) {

			currentNoeud = currentNoeud.getNextNoeud();

		}

	}

	/**************************************************************
	 * Nettoyer la liste
	 **************************************************************/
	public void clearList() {
		
		nbElement = 0;
		firstNoeud = null;
		currentNoeud = null;
		lastNoeud = null;
		atTheEnd = false;
		
	}

	/**************************************************************
	 * Supprimer un objet de la liste
	 **************************************************************/
	public void remove() {
		
		if (currentNoeud.getPreviousNoeud() != null) {
			currentNoeud.getPreviousNoeud().setNextNoeud(
					currentNoeud.getNextNoeud());
		}
		else
			firstNoeud = currentNoeud.getNextNoeud();
		
		if (currentNoeud.getNextNoeud() != null) {
			currentNoeud.getNextNoeud().setPreviousNoeud(
					currentNoeud.getPreviousNoeud());
		}
		else
			lastNoeud = currentNoeud.getPreviousNoeud();
		
		if (currentNoeud != null) {
			currentNoeud = currentNoeud.getPreviousNoeud();
		}
		nbElement--;
		
	}

	/**************************************************************
	 * Replacement d'un objet
	 * @param newObject Objet a replacer
	 **************************************************************/
	public void replaceObject(Object newObject) {
		currentNoeud.setValue(newObject);
	}
	
}