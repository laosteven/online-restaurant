/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		AdresseClient.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/
package object;

import java.util.UUID;

import donneGenerique.Adresse;

/***************************************************************
 * Information sur l'adresse du client
 ***************************************************************/
public class AdresseClient {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private Adresse adresse;
	private Boolean parD�faut;
	private UUID idAdresse;
	
	/***************************************************************
	 * Constructeur par defaut
	 ***************************************************************/
	public AdresseClient() {

	}

	/***************************************************************
	 * Attribution d'adresse au client
	 * 
	 * @param adresse 		Adresse du client
	 * @param parD�faut 	Confirmation d'adresse unique
	 ***************************************************************/
	public AdresseClient(Adresse adresse, Boolean parD�faut) {
		super();
		this.adresse = adresse;
		this.parD�faut = parD�faut;
		setIdAdresse(UUID.randomUUID());
	}

	/***************************************************************
	 * Attribution d'adresse au client
	 * 
	 * @param idAdresseDB 	Identite numerique de l'adresse
	 * @param adresse 		Adresse du client
	 * @param parD�faut 	Confirmation d'adresse unique
	 ***************************************************************/
	public AdresseClient(UUID idAdresseDB, Adresse adresse, Boolean parD�faut) {
		super();
		this.adresse = adresse;
		this.parD�faut = parD�faut;
		this.setIdAdresse(idAdresseDB);
	}

	/***************************************************************
	 * Accesseurs
	 ***************************************************************/
	public Adresse getAdresse() {
		return adresse;
	}
	
	public Boolean getAdrParD�faut() {
		return parD�faut;
	}

	public UUID getIdAdresse() {
		return idAdresse;
	}
	
	/***************************************************************
	 * Mutateurs
	 ***************************************************************/
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public void setParD�faut(Boolean parD�faut) {
		this.parD�faut = parD�faut;
	}

	public void setIdAdresse(UUID idAdresse) {
		this.idAdresse = idAdresse;
	}

}
