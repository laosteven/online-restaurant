/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		AfficheObjet.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package object;


/******************************************************************
 * Classe qui represente un objet qui doit etre affiche
 ******************************************************************/
public class AfficheObject {

	/************************************************************** 
	 * Attributs 
	 **************************************************************/
	private Object objectToDispay;
	private boolean displayed;

	/**************************************************************
	 * Recupere l'objet affiche.
	 *
	 * @return L'objet affiche
	 **************************************************************/
	public Object getMainObject() {
		return objectToDispay;
	}

	/**************************************************************
	 * Redefinir l'objet affiche.
	 *
	 * @param mainObject L'objet a redefinir
	 **************************************************************/
	public void setMainObject(Object mainObject) {
		this.objectToDispay = mainObject;
	}
	
	/**************************************************************
	 * Redefinir l'etat d'affichage.
	 *
	 * @param isDispalyed the new dispalyed
	 **************************************************************/
	public void setDispalyed(boolean isDispalyed) {
		this.displayed = isDispalyed;
	}
	
	/**************************************************************
	 * Verifier l'affichage
	 **************************************************************/
	public boolean isDisplayed() {
		return displayed;
	}

	/**************************************************************
	 * Constructeur
	 **************************************************************/
	public AfficheObject(Object mainObject, boolean isDisplayed) {
		super();
		this.objectToDispay = mainObject;
		this.displayed = isDisplayed;
	}

}
