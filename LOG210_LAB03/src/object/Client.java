/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Client.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package object;

import java.util.UUID;
import donneGenerique.Adresse;
import modele.ObjetListe;

public class Client extends Utilisateur {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private UUID id;
	private ObjetListe listAdresse;

	/***************************************************************
	 * 
	 * @param id
	 * @param dateNaissance
	 * @param numeroTel
	 * @param courriel
	 * @param nom
	 * @param prenom
	 * @param nomUtilisateur
	 * @param listAdressse
	 * @param Defaut
	 ***************************************************************/
	public Client(UUID idClient, UUID idUtilisateur, String dateNaissance, String numeroTel,
			String courriel, String nom, String prenom, String nomUtilisateur, Adresse Defaut) {
		//super();
		this.id = idClient;
		this.idUtilisateur = idUtilisateur;
		this.dateNaissance = dateNaissance;
		this.numeroTel = numeroTel;
		this.courriel = courriel;
		this.nom = nom;
		this.prenom = prenom;
		this.nomUtilisateur = nomUtilisateur;
		this.listAdresse = new ObjetListe();
		this.listAdresse.addBeginning(new AdresseClient(Defaut, true));
	}

	public Client(UUID id, String dateNaissance2, String numeroTel,
			String courriel, String nom, String prenom, String nomUtilisateur,
			ObjetListe listAdressse) {
		super();
		this.id = id;
		this.dateNaissance = dateNaissance2;
		this.numeroTel = numeroTel;
		this.courriel = courriel;
		this.nom = nom;
		this.prenom = prenom;
		this.nomUtilisateur = nomUtilisateur;
		this.listAdresse = listAdressse;
	}

	public modele.ObjetListe getListAdresse() {
		return listAdresse;
	}

	public void setListAdresse(modele.ObjetListe listAdresse) {
		this.listAdresse = listAdresse;
	}

	public AdresseClient getAdresseDefaut() {

		listAdresse.setListBeginning();
		while (listAdresse != null) {

			AdresseClient itemCourant = (AdresseClient) listAdresse
					.getObjectAndIterate();
			if (itemCourant.getAdrParDéfaut()) {
				listAdresse.setListBeginning();
				return itemCourant;
			}

		}
		return null;
	}

	public void AddNewAdresse(Adresse adresse) {
		listAdresse.setListBeginning();

		if (listAdresse == null) {
			listAdresse = new ObjetListe();
		} else {

			while (listAdresse != null && listAdresse.getObject() != null) {

				AdresseClient itemCourant = (AdresseClient) listAdresse
						.getObjectAndIterate();
				itemCourant.setParDéfaut(false);
			}
		}
		listAdresse.setListBeginning();
		listAdresse.addEnd(new AdresseClient(adresse, true));
	}

	public void SetAdresseDefaut(Adresse adresse) {
		listAdresse.setListBeginning();

		if (listAdresse == null) {
			listAdresse = new ObjetListe();
		} else {

			while (listAdresse != null && listAdresse.getObject() != null) {

				AdresseClient itemCourant = (AdresseClient) listAdresse
						.getObjectAndIterate();
				itemCourant.setParDéfaut(false);
				if (itemCourant.getAdresse().getIdAdresse() == adresse
						.getIdAdresse()) {
					itemCourant.setParDéfaut(true);
				}
			}
		}
	}

	public Client() {
		listAdresse = new ObjetListe();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getNumeroTel() {
		return numeroTel;
	}

	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}

	public String getCourriel() {
		return courriel;
	}

	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

}