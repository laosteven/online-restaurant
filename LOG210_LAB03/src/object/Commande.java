/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIClientConfirme.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package object;

import java.util.UUID;
import modele.ObjetListe;
import donneGenerique.Adresse;

/***************************************************************
 * Informations de chaque commandes selectionnees par un 
 * client.
 ***************************************************************/
public class Commande {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private UUID idCommande;
	private Restaurant restaurant;
	private String dateCommande;
	private Adresse adresseLivraison;
	private CommandeStatus statut;
	private UUID noConfirmation;
	private ObjetListe listItem;

	/***************************************************************
	 * Constructeur
	 * Creation d'une commande
	 * 
	 * @param idCommande			Numero de la commande
	 * @param restaurant 			Restaurant qui prepare la commande
	 * @param dateCommande			Date de la commande
	 * @param adresseLivraison		Adresse du client
	 * @param statutRef 			Statut de la commande
	 * @param noConfirmation		Numero de confirmation du client
	 * @param clientCommande		Le client
	 ***************************************************************/
	public Commande(UUID idCommande, Restaurant restaurant,
			String dateCommande, Adresse adresseRef,
			CommandeStatus statutRef, UUID noConfirmRef) {
		
		super();
		this.idCommande = idCommande;
		this.restaurant = restaurant;
		this.dateCommande = dateCommande;
		this.adresseLivraison = adresseRef;
		statut = statutRef;
		noConfirmation = noConfirmRef;
		
	}

	/***************************************************************
	 * Constructeur
	 * 
	 * @param restaurant			Numero de la commande
	 * @param dateCommande			Date de la commande
	 * @param adresseSpecifie 		Adresse de la commande
	 * @param statutRef 			Statut de la commande
	 * @param client 				Le client
	 ***************************************************************/
	public Commande(Restaurant restaurant, String dateCommande,
			Adresse adresseSpecifie, CommandeStatus statutRef) {
		
		super();
		this.restaurant = restaurant;
		this.dateCommande = dateCommande;
		this.adresseLivraison = adresseSpecifie;
		statut = statutRef;
		noConfirmation = UUID.randomUUID();
		idCommande = UUID.randomUUID();
		
	}
	
	/***************************************************************
	 * Constructeur par defaut
	 ***************************************************************/
	public Commande() {
		
	}
	
	/***************************************************************
	 * Affiche la liste de commande complete choisi par le
	 * client
	 * @return La liste de la commande
	 ***************************************************************/
	public String displayItem(){
		
		// Initialisation de la liste
		String liste = "";
		
		// Reference au debut de la liste
		listItem.setListBeginning();
		
		// Tant que la liste n'est pas vide
		while (listItem.getObject() != null) {

				liste += listItem.getObject().toString() + "\n";
				listItem.nextObject();

		}
		
		return liste;
			
	}

	/***************************************************************
	 * Mutateur
	 ***************************************************************/
	public void setStatus(CommandeStatus statusRef) {
		statut = statusRef;
	}
	
	public void setListItem(ObjetListe listItem) {
		this.listItem = listItem;
	}
	
	public void setIdCommande(UUID idCommande) {
		this.idCommande = idCommande;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public void setDateCommande(String dateCommande) {
		this.dateCommande = dateCommande;
	}

	public void setAdresseLivraison(Adresse adresseLivraison) {
		this.adresseLivraison = adresseLivraison;
	}

	public void setNoConfirmation(UUID noConfirmation) {
		this.noConfirmation = noConfirmation;
	}

	/***************************************************************
	 * Accesseurs
	 ***************************************************************/
	public UUID getIdCommande() {
		return idCommande;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}
	
	public String getDateCommande() {
		return dateCommande;
	}

	public Adresse getAdresseLivraison() {
		return adresseLivraison;
	}

	public UUID getNoConfirmation() {
		return noConfirmation;
	}

	public CommandeStatus getStatus() {
		return statut;
	}

	public ObjetListe getListItem() {
		return listItem;
	}

	public enum CommandeStatus {
		Initialized, Ordered, Preparation, Ready, Shipped;
	}

}