package object;

import java.util.UUID;

/**************************************************************
 * Classe qui defini les attributs d'un entrepreneur (admin)
 **************************************************************/
public class Entrepreneur extends Utilisateur {

	private UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

}
