package object;

import java.util.UUID;

public class Hierarchy {
	
	public Hierarchy(UUID id, String name){
		
		this.Description = name;
		this.idHierarchy = id;
	}
	
	public Hierarchy() {

		this.Description = "";
		this.idHierarchy = null;
	}

	private UUID idHierarchy;
	
	private String Description;

	public UUID getIdHierarchy() {
		return idHierarchy;
	}

	public void setIdHierarchy(UUID idHierarchy) {
		this.idHierarchy = idHierarchy;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
	public enum HierachyType{
		Entrepreneur, Client, Livreur, Restaurateur;
	}

}
