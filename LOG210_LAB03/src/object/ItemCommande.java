/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		ItemCommande.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/
package object;

import java.util.UUID;
import object.ItemMenu;

/***************************************************************
 * Plats commandes du client
 ***************************************************************/
public class ItemCommande {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private UUID IdItemCommande;
	private ItemMenu itemCommand�;
	private int quantit�;

	/***************************************************************
	 * Constructeur
	 * 
	 * @param idItemCommande
	 * @param itemCommand�
	 * @param quantit�
	 ***************************************************************/
	public ItemCommande(UUID idItemCommande, ItemMenu itemCommand�, int quantit�) {
		super();
		IdItemCommande = idItemCommande;
		this.itemCommand� = itemCommand�;
		this.quantit� = quantit�;
	}

	/***************************************************************
	 * Constructeur
	 * 
	 * @param itemCommand�
	 * @param quantit�
	 ***************************************************************/
	public ItemCommande(ItemMenu itemCommand�, int quantit�) {
		super();
		IdItemCommande = UUID.randomUUID();
		this.itemCommand� = itemCommand�;
		this.quantit� = quantit�;
	}

	/***************************************************************
	 * Constructeur
	 ***************************************************************/
	public ItemCommande() {

	}

	/***************************************************************
	 * Accesseurs
	 ***************************************************************/
	public UUID getIdItemCommande() {
		return IdItemCommande;
	}
	
	public ItemMenu getItemCommand�() {
		return itemCommand�;
	}
	
	public int getQuantit�() {
		return quantit�;
	}
	
	/***************************************************************
	 * Mutateurs
	 ***************************************************************/
	public void setIdItemCommande(UUID idItemCommande) {
		IdItemCommande = idItemCommande;
	}

	public void setItemCommand�(ItemMenu itemCommand�) {
		this.itemCommand� = itemCommand�;
	}

	public void setQuantit�(int quantit�) {
		this.quantit� = quantit�;
	}

	/***************************************************************
	 * Affichage du plat et du montant
	 ***************************************************************/
	@Override
	public String toString() {
		return quantit� + " * " + itemCommand�.getNom() + " ("
				+ itemCommand�.getPrix() * quantit� + " $)";
	}

}