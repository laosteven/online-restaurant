package object;

import java.util.UUID;

public class ItemMenu {
	private String nom;
	private UUID idItemMenu;
	public UUID getIdItemMenu() {
		return idItemMenu;
	}
	public void setIdItemMenu(UUID idItemMenu) {
		this.idItemMenu = idItemMenu;
	}
	private String description;
	private Double prix;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public ItemMenu(String nom, String description, Double prix) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		idItemMenu=UUID.randomUUID();
	}
	public ItemMenu(UUID idItem,String nom, String description, Double prix) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		idItemMenu=idItem;
	}
	public ItemMenu() {
		// TODO Auto-generated constructor stub
	}
	
	

}
