package object;

import java.util.UUID;

import donneGenerique.Adresse;

public class Livreur extends Utilisateur{

	private UUID id;
	private Adresse adresseActuelle;
	
	

	public Adresse getAdresseActuelle() {
		return adresseActuelle;
	}

	public void setAdresseActuelle(Adresse adresseActuelle) {
		this.adresseActuelle = adresseActuelle;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
}
