package object;

import java.util.UUID;

import modele.ObjetListe;




public class Menu {
	private String	nom;
	private UUID idMenu;
	public UUID getIdMenu() {
		return idMenu;
	}
	public void setIdMenu(UUID idMenu) {
		this.idMenu = idMenu;
	}
	private ObjetListe listeItemMenu;
	public Menu(String nom, ObjetListe listeItemMenu) {
		super();
		this.nom = nom;
		this.listeItemMenu = listeItemMenu;
		idMenu=UUID.randomUUID();
	}
	public Menu(String nom, UUID idMenu, ObjetListe listeItemMenu) {
		super();
		this.nom = nom;
		this.idMenu = idMenu;
		this.listeItemMenu = listeItemMenu;
	}
	public Menu() {
		idMenu=UUID.randomUUID();
		this.listeItemMenu=new ObjetListe ();
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public ObjetListe getListeItemMenu() {
		return listeItemMenu;
	}
	public void setListeItemMenu(ObjetListe listeItemMenu) {
		this.listeItemMenu = listeItemMenu;
	}

}

