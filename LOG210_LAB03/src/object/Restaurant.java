/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Restaurant.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package object;


import java.util.UUID;

import donneGenerique.Adresse;

// TODO: Auto-generated Javadoc
/**
 * ******************************************************************
 * Classe qui represente le restaurant
 * ******************************************************************.
 */
public class Restaurant {

	/** ************************************************************** Attributs **************************************************************. */
	private String nom = "";
	
	/** The adresse. */
	private Adresse adresse =null;
	
	/** The telephone. */
	private String telephone = "";
	
	private Menu menu;
	
	/** The type cuisine. */
	private String typeCuisine = "";
	
	/** The identif. */
	private UUID id = null;
	
	/** The id association. */
	private UUID idAssociation = null;

	/**
	 * **************************************************************
	 * Constructeur: recueille nom du restaurant et adresse.
	 *
	 * @param pNom Nom du restaurant
	 * @param pAdresse Adresse du restaurant
	 * **************************************************************
	 * @param pTelephone the telephone
	 * @param pType the type
	 * @param pIdentif the identif
	 * @param pIdAssociation the id association
	 */
	public Restaurant(String pNom, Adresse pAdresse, String pTelephone,
			String pType, UUID pIdentif, UUID pIdAssociation) {
		nom = pNom;
		adresse = pAdresse;
		telephone = pTelephone;
		typeCuisine = pType;
		id = pIdentif;
		idAssociation = pIdAssociation;
	}

	/**
	 * Instantiates a new restaurant.
	 *
	 * @param pNom the nom
	 * @param pAdresse the adresse
	 * @param pTelephone the telephone
	 * @param pType the type
	 * @param pIdentif the identif
	 */
	public Restaurant(String pNom, Adresse pAdresse, String pTelephone,
			String pType, UUID pIdentif) {
		nom = pNom;
		adresse = pAdresse;
		telephone = pTelephone;
		typeCuisine = pType;
		id = pIdentif;

	}
	
	public Restaurant(String pNom, String pAdresse, String pTelephone,
			String pType, UUID pIdentif) {
		nom = pNom;
		adresse = new Adresse(pAdresse);
		telephone = pTelephone;
		typeCuisine = pType;
		id = pIdentif;

	}

	/**
	 * Instantiates a new restaurant.
	 *
	 * @param pNom the nom
	 * @param pAdresse the adresse
	 * @param pTelephone the telephone
	 * @param pType the type
	 */
	public Restaurant(String pNom, String pAdresse, String pTelephone,
			String pType) {
		nom = pNom;
		adresse = new Adresse(pAdresse);
		telephone = pTelephone;
		typeCuisine = pType;

	}
	
	public Restaurant(String pNom, Adresse pAdresse, String pTelephone,
			String pType) {
		nom = pNom;
		adresse = pAdresse;
		telephone = pTelephone;
		typeCuisine = pType;

	}

	/**
	 * Instantiates a new restaurant.
	 */
	public Restaurant() {
		menu = new Menu();
		// TODO Auto-generated constructor stub
	}

	/**
	 * **************************************************************
	 * Accesseurs
	 * **************************************************************.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Gets the adresse.
	 *
	 * @return the adresse
	 */

	public Adresse getAdresse() {
		return adresse;
	}

	/**
	 * Gets the telephone.
	 *
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Gets the type cuisine.
	 *
	 * @return the type cuisine
	 */
	public String getTypeCuisine() {
		return typeCuisine;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * Gets the id association.
	 *
	 * @return the id association
	 */
	public UUID getIdAssociation() {
		return idAssociation;
	}

	/**
	 * **************************************************************
	 * Mutateurs
	 * **************************************************************.
	 *
	 * @param pNom the nom
	 * @return the string
	 */
	public String setNom(String pNom) {
		nom = pNom;
		return nom;
	}

	/**
	 * Sets the adresse.
	 *
	 * @param pAdresse the adresse
	 * @return the string
	 */
	public String setAdresse(Adresse pAdresse) {
		adresse = pAdresse;
		return adresse.toString();
	}

	/**
	 * Sets the telephone.
	 *
	 * @param pTelephone the telephone
	 * @return the string
	 */
	public String setTelephone(String pTelephone) {
		telephone = pTelephone;
		return telephone;
	}

	/**
	 * Sets the type cuisine.
	 *
	 * @param pTypeCuisine the type cuisine
	 * @return the string
	 */
	public String setTypeCuisine(String pTypeCuisine) {
		typeCuisine = pTypeCuisine;
		return typeCuisine;
	}

	/**
	 * Sets the id.
	 *
	 * @param pId the id
	 * @return the int
	 */
	public UUID setId(UUID pId) {
		id = pId;
		return id;
	}

	/**
	 * Sets the id association.
	 *
	 * @param idAssociation the new id association
	 */
	public void setIdAssociation(UUID idAssociation) {
		this.idAssociation = idAssociation;
	}
	
	
	/**
	 * @return the menu
	 */
	public object.Menu getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}
