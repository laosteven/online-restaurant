/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Restaurateur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package object;

import java.util.UUID;

import donneGenerique.Adresse;

/**************************************************************
 * Acteur: Restaurateur
 **************************************************************/
public class Restaurateur extends Utilisateur {

	/**************************************************************
	 * Attributs 
	 **************************************************************/
	/*private String nom = "";
	private String prenom = "";*/
	private Adresse adresse;
	private UUID id = null;

	/**************************************************************
	 * Constructeur: recueille prenom, nom et adresse.
	 *
	 * @param pPrenom Prenom du restaurateur
	 * @param pNom Nom du restaurateur
	 * @param pAdresse 
	 ***************************************************************/
	public Restaurateur(String pPrenom, String pNom, Adresse adresseRef) {

		prenom = pPrenom;
		nom = pNom;
		adresse = adresseRef;

	}

	/**************************************************************
	 * Instantiates a new restaurateur.
	 *
	 * @param pPrenom the prenom
	 * @param pNom the nom
	 * @param pAdresse the adresse
	 * @param pId the id
	 **************************************************************/
	public Restaurateur(String pPrenom, String pNom, Adresse adresseRef, UUID pId, UUID idUser) {

		this.prenom = pPrenom;
		this.nom = pNom;
		this.adresse = adresseRef;
		this.id = pId;
		this.idUtilisateur = idUser;
	}

	/**************************************************************
	 * Instancie un nouveau restaurateur
	 **************************************************************/
	public Restaurateur() {
		
	}
	
	/**************************************************************
	 * Accesseurs
	 **************************************************************/

	public Adresse getAdresse() {
		return adresse;
	}

	public UUID getId() {
		return id;
	}

	/**************************************************************
	 * Mutateurs
	 **************************************************************/
	public Adresse setAdresse(Adresse adresseRef) {
		adresse = adresseRef;
		return adresse;
	}

	public UUID setId(UUID pId) {
		id = pId;
		return pId;
	}

}
