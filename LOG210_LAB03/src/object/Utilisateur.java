/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		Utilisateur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package object;

import java.util.UUID;

import object.Hierarchy.HierachyType;

/**************************************************************
 * Classe qui defini les attributs d'un utilisateur
 **************************************************************/
public class Utilisateur {

	/**************************************************************
	 * Attributs 
	**************************************************************/
	protected String prenom = "";
	protected String nom = "";
	protected String nomUtilisateur = "";
	protected String motDePasse = "";
	protected UUID idUtilisateur;
	protected String hierarchyName;
	protected String dateNaissance;
	protected String numeroTel;
	protected String courriel;
	 
	/**************************************************************
	 * Accesseurs
	 **************************************************************/
	public String getHierarchyName() {
		return hierarchyName;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public String getNom() {
		return nom;
	}
	
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	
	public String getMotDePasse() {
		return this.motDePasse;
	}
	
	public UUID getIdUtilisateur() {
		return idUtilisateur;
	}
	
	public String getDateNaissance() {
		return dateNaissance;
	}

	public String getNumeroTel() {
		return numeroTel;
	}

	public String getCourriel() {
		return courriel;
	}
	
	/**************************************************************
	 * Mutateurs
	 **************************************************************/
	public void setHierarchyName(HierachyType hierarchyName) {
		this.hierarchyName = hierarchyName.toString();
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	
	public void setMotDePasse(String pMotDePasse) {
		this.motDePasse = pMotDePasse;
	}
	
	public void setIdUtilisateur(UUID pIdUtilisateur) {
		this.idUtilisateur = pIdUtilisateur;
	}	

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	
	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}
	
	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}

}
