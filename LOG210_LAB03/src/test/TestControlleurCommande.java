package test;

import static org.junit.Assert.*;

import java.util.UUID;

import modele.ModeleCommander;
import modele.ModeleMenu;

import object.Client;
import object.Commande;
import object.ItemCommande;
import object.ItemMenu;
import object.Restaurant;

import org.joda.time.DateTime;
import org.junit.Test;


import utility.Utility;

import commande.Cmd_Restaurant;

import controlleur.ControlleurCommande;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import donneGenerique.Adresse;

public class TestControlleurCommande {
	private Client  client ;
	private ModeleCommander model;
	
	public TestControlleurCommande() {
	
		client = new Client();
		
		try {
			model = new ModeleCommander(Utility.convertToObjectList(new Cmd_Restaurant().selectionnerETRetour()), client);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private ControlleurCommande controlleurCommande=new ControlleurCommande(model);
	@Test
	public void testAjouterItemCarnet() {
		//fail("Not yet implemented");
	ItemMenu itemTest=new ItemMenu("Big Mac", "big", 2.05);
	controlleurCommande.ajouterItemCarnetTest(4, itemTest);
	model.getCommande().setListEnd();
	ItemCommande itemCommand�Commande=(ItemCommande) model.getCommande().getObject();
	assertTrue(itemCommand�Commande.getItemCommand�().getIdItemMenu().equals(itemTest.getIdItemMenu()));
	assertTrue(itemCommand�Commande.getQuantit�()==4);
	
	
	
		
	}

	

	@Test
	public void testPasserCommande() {
		//fail("Not yet implemented");
		
		
		
		Restaurant test=new Restaurant("mcdo", new Adresse("35/Peel/MTL/QC/j4k8g9"), "4444444488", "test");
		String messString=controlleurCommande.passerCommandeTEST(DateTime.now().toString(), test, new Adresse("35/StCath/MTL/QC/j4k8g9"));
		Commande cmdPasser=controlleurCommande.getTestCommande();
		assertTrue("Erreur aucun num�ro de confirmation a �t� recu",messString!=null );
		assertTrue("Erreur les adresses sont diff�rtentes",cmdPasser.getAdresseLivraison().toStringSpaced().equals( new Adresse("35/StCath/MTL/QC/j4k8g9").toStringSpaced() ));
		assertTrue("Restaurant diff�rent", cmdPasser.getRestaurant().getNom().compareTo(test.getNom())==0);
		
		
		
		
		
	}

}
