package test;

import static org.junit.Assert.*;

import org.junit.Test;

import controlleur.ControlleurLivreur;

import donneGenerique.Adresse;

public class TestControlleurLivreur {

	public TestControlleurLivreur() {
	testTestcreerURL();
	}

	@Test
	public void testTestcreerURL() {
		Adresse livreur=new Adresse("1487/Peel/Montr�al/Qu�bec/j7s8w3"); 
		Adresse restaurant=new Adresse("1859/St-Denis/Montr�al/Qu�bec/j8s8w4");
		Adresse client=new Adresse("2087/Ste-Catherine/Montr�al/Qu�bec/j9s8w5"); 
		ControlleurLivreur testLivreur=new ControlleurLivreur();
		String urlGenere=testLivreur.TestcreerURL(livreur, restaurant, client);
		assertTrue("Erreur dans l'url g�n�r�", ("http://maps.google.com/maps?saddr=" +
		livreur.toStringSpaced()+
		"&daddr=" + 
		restaurant.toStringSpaced() +
		"&mrad=" +
		client.toStringSpaced()).equals(urlGenere));
	}

}
