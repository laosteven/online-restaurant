package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import modele.ObjetListe;
import object.AdresseClient;
import object.Client;
import object.Commande;
import object.Commande.CommandeStatus;
import object.ItemCommande;
import object.Menu;
import object.Restaurant;
import object.Hierarchy.HierachyType;

import org.joda.time.DateTime;
import org.junit.Test;

import commande.Cmd_Client;
import commande.Cmd_Commande;
import commande.Cmd_Menu;
import commande.Cmd_Connexion;
import donneGenerique.Adresse;
import donneGenerique.DonneeGlobal;
import object.ItemMenu;

public class TestJean {

	@Test
	public void RetrieveClient() throws Exception {
		
		new Cmd_Connexion().connecter("root","admin");
		
		assertTrue(DonneeGlobal.getInstance().getUtilisateurConnecte() != null);
		
		new Cmd_Connexion().connecter("fuck","yeah");
		
		assertTrue(DonneeGlobal.getInstance().getUtilisateurConnecte() == null);
		
	}
	
	@Test
	public void MenuTest() throws Exception{
		
		Menu menu = new Menu();
		menu.setNom("classic");
		menu.setIdMenu(UUID.randomUUID());
		
		List<Object> itemList = new ArrayList<Object>();
		
		itemList.add(new ItemMenu(UUID.randomUUID(), "bigmac", "getfat", 10.0));
		itemList.add(new ItemMenu(UUID.randomUUID(), "quarterpound", "getfuckingfat", 10.0));
		
		menu.setListeItemMenu(utility.Utility.convertToObjectList((ArrayList<Object>) (itemList)));
		
		new Cmd_Menu().ajouter(menu, UUID.fromString("2ab10833-3904-4648-b304-c87e350449ee"));
		
		new Cmd_Menu().supprimer(menu);
		
	}
	
	@Test
	public void CommandeTest() throws Exception{
		
		Menu menu = new Menu();
		menu.setNom("classic");
		menu.setIdMenu(UUID.randomUUID());
		
		List<Object> itemList = new ArrayList<Object>();
		
		itemList.add(new ItemMenu(UUID.randomUUID(), "bigmac", "getfat", 10.0));
		itemList.add(new ItemMenu(UUID.randomUUID(), "quarterpound", "getfuckingfat", 10.0));
		
		menu.setListeItemMenu(utility.Utility.convertToObjectList((ArrayList<Object>) (itemList)));
		
		Restaurant resto = new Restaurant();
		resto.setId(UUID.fromString("2ab10833-3904-4648-b304-c87e350449ee"));
		resto.setMenu(menu); // menu disponible pour ce resto
		
		List<ItemCommande> menuChoisi = new ArrayList<ItemCommande>();
		ItemCommande itemchoisi = new ItemCommande((ItemMenu)resto.getMenu().getListeItemMenu().getNextNoeudValue(), 2);
		menuChoisi.add(itemchoisi); //repas et quantit�
		
		Client client = new Client();
		client.setId(UUID.fromString("7f3ae357-089d-4e5b-9e20-f09c46228c55"));
		
		AdresseClient adr = new AdresseClient();
		adr.setIdAdresse(UUID.fromString("453ee358-089d-4e5b-9e20-f09c46228c55"));
		adr.setParD�faut(true);
		client.AddNewAdresse(adr.getAdresse());
		
		Commande cmd = new Commande();
		
		cmd.setRestaurant(resto); // resto participant
		cmd.setListItem((ObjetListe) menuChoisi); // menu choisi
		cmd.setIdCommande(UUID.randomUUID());
		//cmd.setClientCommande(client);//client qui call la commande
		cmd.setDateCommande(new DateTime(2013,11,2,18,10,0).toString());//date de la commande
		cmd.setStatus(CommandeStatus.Initialized);
		cmd.setAdresseLivraison(client.getAdresseDefaut().getAdresse());
		cmd.setNoConfirmation(UUID.randomUUID());
			
		
		new Cmd_Commande().enregistrerCommande(cmd);
			
	}
	
	@Test
	public void LivraisonTest() throws Exception{
	
		
		
	}
	
	@Test
	public void CreateClientTest() throws Exception{
		
		Client client  = new Client();
		
		client.setCourriel("kekchose@gmail.com");
		client.setDateNaissance("2012/06/06");
		client.setNumeroTel("456 987 1236");
		client.setId(UUID.randomUUID());
		client.setIdUtilisateur(UUID.randomUUID());
		
		Adresse adr = new Adresse();
		adr.setCodePostal("j8c6h2");
		adr.setIdAdresse(UUID.randomUUID());
		adr.setNo("25");
		adr.setProvince("qu�bec");
		adr.setRue("avenue des glands");
		adr.setVille("lorraine");
		
		client.AddNewAdresse(adr);
		
		client.setMotDePasse("123456");
		client.setNomUtilisateur("test");
		
		client.setHierarchyName(HierachyType.Client);
		client.setNom("david");
		client.setPrenom("grimard");
		
		new Cmd_Client().ajouter(client);
		
		Client retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		assertTrue(retrievedClient.getCourriel().contentEquals(client.getCourriel()));
		
		new Cmd_Client().Suprimmer(client);
		
	}
	
	@Test
	public void TestUseCase5() throws Exception{
		
		new Cmd_Commande().CommandePreparationStart(UUID.fromString("1934232d-9984-4314-98d4-c2c209d39810"));
		
		new Cmd_Commande().selectionnerCommande();
		
		new Cmd_Commande().CommandePreparationEnd(UUID.fromString("1934232d-9984-4314-98d4-c2c209d39810"));
		
		new Cmd_Commande().selectionnerCommande();
	}

}
