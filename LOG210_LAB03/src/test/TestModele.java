package test;

import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;

import commande.Cmd_Menu;
import object.Commande;
import object.ItemCommande;
import object.ItemMenu;
import object.Livreur;
import object.Menu;
import object.Restaurant;
import modele.ModeleCommander;
import modele.ModeleLivreur;
import modele.ModeleMenu;
import modele.ObjetListe;

public class TestModele {
	
	@Test
	public void TestModeleMenu() throws Exception{
	Restaurant mcdo = new Restaurant();
	ItemMenu item = new ItemMenu();
	ItemMenu item2 = new ItemMenu();
	item.setDescription("Bonjour");
	item.setIdItemMenu(UUID.fromString("1934232d-9984-4314-78d4-c2c209d39810"));
	item.setNom("pizza");
	item.setPrix(12.90);
	mcdo.setNom("mcdo");
	ModeleMenu model = new ModeleMenu(mcdo);
	model.addItem(item);
	item2=(ItemMenu) (mcdo.getMenu().getListeItemMenu().getObject());
	assertTrue(item2.getPrix().equals(12.90));
	assertTrue(item2.getNom().equals("pizza"));
	assertTrue(item2.getDescription().equals("Bonjour"));
	assertTrue(item2.getIdItemMenu().equals(UUID.fromString("1934232d-9984-4314-78d4-c2c209d39810")));
	
	model.removeItemAtIndex(0);
	}
	@Test
	public void TestModeleCommande() throws Exception{
		
	ModeleCommander commande = new ModeleCommander();
	ItemCommande item = new ItemCommande();
	ItemCommande item2 = new ItemCommande();
	item.setIdItemCommande(UUID.fromString("1934672d-9984-4314-78d4-c2c209d39810"));
	item.setQuantit�(2);
	commande.addItemCommande(item);
	item2 = (ItemCommande) commande.getCommande().getObject();
	assertTrue(item2.getIdItemCommande().equals(UUID.fromString("1934672d-9984-4314-78d4-c2c209d39810")));
	assertTrue(item2.getQuantit�()==2);

		
	}
	@Test
	public void TestModeleLivreur() throws Exception{
		
	Commande co = new Commande();
	co.setIdCommande(UUID.fromString("1934672d-9984-7595-78d4-c2c209d39810"));
	ObjetListe liste = new ObjetListe();
	Commande co2 = new Commande();
	liste.addBeginning(co);
	Livreur livreur = new Livreur();
	Livreur livreur2 = new Livreur();
	livreur.setNom("John");
	ModeleLivreur model = new ModeleLivreur(livreur,liste);
	livreur2 =model.getLivreur();
	co2 = (Commande) liste.getObject();
	assertTrue(livreur2.getNom().equals("John"));
	assertTrue(co2.getIdCommande().equals(UUID.fromString("1934672d-9984-7595-78d4-c2c209d39810")));
	}
	}
	
	


