package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import modele.ObjetListe;
import object.AdresseClient;
import object.Client;
import object.Commande;
import object.ItemCommande;
import object.ItemMenu;
import object.Menu;
import object.Restaurant;
import object.Restaurateur;
import object.Commande.CommandeStatus;
import object.Hierarchy.HierachyType;

import org.joda.time.DateTime;
import org.junit.Test;

import utility.Utility;
import commande.Cmd_Client;
import commande.Cmd_Commande;
import commande.Cmd_Menu;
import commande.Cmd_Restaurant;
import commande.Cmd_Restaurateur;
import donneGenerique.Adresse;

public class UseCase {

	@Test
	public void UseCase1_GererRestaurant() throws Exception {

		Cmd_Restaurant resto = new Cmd_Restaurant();
		
		List<Restaurant> restoList = new ArrayList<Restaurant>();
		
		restoList = resto.selectionnerETRetour();
		
		assertTrue(restoList.get(0) != null);
		
	}
	
	@Test
	public void UseCase2_GererRestaurateur() throws Exception {

		Cmd_Restaurateur resto = new Cmd_Restaurateur();
		
		List<Restaurateur> restoList = new ArrayList<Restaurateur>();
		
		restoList = resto.selectionEtRetour();
		
		assertTrue(restoList.get(0) != null);
		
	}
	
	@Test
	public void UseCase1_CreerCompte() throws Exception {

		Client client  = new Client();
		
		client.setCourriel("kekchose@gmail.com");
		client.setDateNaissance("2012/06/06");
		client.setNumeroTel("456 987 1236");
		client.setId(UUID.randomUUID());
		client.setIdUtilisateur(UUID.randomUUID());
		
		Adresse adr = new Adresse();
		adr.setCodePostal("j8c6h2");
		adr.setIdAdresse(UUID.randomUUID());
		adr.setNo("25");
		adr.setProvince("qu�bec");
		adr.setRue("avenue des glands");
		adr.setVille("lorraine");
		
		client.AddNewAdresse(adr);
		
		client.setMotDePasse("123456");
		client.setNomUtilisateur("test");
		
		client.setHierarchyName(HierachyType.Client);
		client.setNom("david");
		client.setPrenom("grimard");
		
		new Cmd_Client().ajouter(client);
		
		Client retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		assertTrue(retrievedClient.getCourriel().contentEquals(client.getCourriel()));
		
		new Cmd_Client().Suprimmer(client);
		
	}
	
	@Test
	public void UseCase2_GererCompte() throws Exception {

		Client client  = new Client();
		client.setCourriel("kekchose@gmail.com");
		client.setDateNaissance("2012/06/06");
		client.setNumeroTel("456 987 1236");
		client.setId(UUID.randomUUID());
		client.setIdUtilisateur(UUID.randomUUID());
		
		Adresse adr = new Adresse();
		adr.setCodePostal("j8c6h2");
		adr.setIdAdresse(UUID.randomUUID());
		adr.setNo("25");
		adr.setProvince("qu�bec");
		adr.setRue("avenue des glands");
		adr.setVille("lorraine");
		
		client.AddNewAdresse(adr);
		
		client.setMotDePasse("123456");
		client.setNomUtilisateur("test");
		
		client.setHierarchyName(HierachyType.Client);
		client.setNom("david");
		client.setPrenom("grimard");
		
		new Cmd_Client().ajouter(client);
		
		Client retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		assertTrue(retrievedClient.getCourriel().contentEquals(client.getCourriel()));
		
		client.setCourriel("allo");
		
		new Cmd_Client().modifier(client);
		
		retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		assertTrue(retrievedClient.getCourriel().contentEquals(client.getCourriel()));
		
		new Cmd_Client().Suprimmer(client);
		
	}
	
	@Test
	public void UseCase3_PasserCommande() throws Exception {

		Menu menu = new Menu();
		menu.setNom("classic");
		menu.setIdMenu(UUID.randomUUID());
		
		List<Object> itemList = new ArrayList<Object>();
		
		itemList.add(new ItemMenu(UUID.randomUUID(), "bigmac", "getfat", 10.0));
		itemList.add(new ItemMenu(UUID.randomUUID(), "quarterpound", "getfuckingfat", 10.0));
		
		menu.setListeItemMenu(utility.Utility.convertToObjectList((ArrayList<Object>) (itemList)));
		
		Restaurant resto = new Restaurant();
		resto.setId(UUID.fromString("2ab10833-3904-4648-b304-c87e350449ee"));
		resto.setMenu(menu); // menu disponible pour ce resto
		
		List<ItemCommande> menuChoisi = new ArrayList<ItemCommande>();
		ItemCommande itemchoisi = new ItemCommande((ItemMenu)resto.getMenu().getListeItemMenu().getNextNoeudValue(), 2);
		menuChoisi.add(itemchoisi); //repas et quantit�
		
		
		Client client = new Client();
		client.setId(UUID.fromString("7f3ae357-089d-4e5b-9e20-f09c46228c55"));
		
		Adresse adr = new Adresse();
		adr.setIdAdresse(UUID.fromString("453ee358-089d-4e5b-9e20-f09c46228c55"));
		//adr.setParD�faut(true);
		//client.AddNewAdresse(adr.getAdresse());
		
		Commande cmd = new Commande();
		
		cmd.setRestaurant(resto); // resto participant
		cmd.setListItem(Utility.convertToObjectList(menuChoisi)); // menu choisi
		cmd.setIdCommande(UUID.randomUUID());
		cmd.setAdresseLivraison(adr);//client qui call la commande
		cmd.setDateCommande(new DateTime(2013,11,2,18,10,0).toString());//date de la commande
		cmd.setStatus(CommandeStatus.Initialized);
		//cmd.setAdresseLivraison(client.getAdresseDefaut().getAdresse());
		cmd.setNoConfirmation(UUID.randomUUID());
			
		
		new Cmd_Commande().enregistrerCommande(cmd);
		
		new Cmd_Commande().suprimmerCommande(cmd);
		
		assertTrue(true);
	}
	
	@Test
	public void UseCase3_NouvelleAdresse() throws Exception{
		
		Client client  = new Client();
		
		client.setCourriel("kekchose@gmail.com");
		client.setDateNaissance("2012/06/06");
		client.setNumeroTel("456 987 1236");
		client.setId(UUID.randomUUID());
		client.setIdUtilisateur(UUID.randomUUID());
		
		Adresse adr = new Adresse();
		adr.setCodePostal("j8c6h2");
		adr.setIdAdresse(UUID.randomUUID());
		adr.setNo("25");
		adr.setProvince("qu�bec");
		adr.setRue("avenue des glands");
		adr.setVille("lorraine");
		
		client.AddNewAdresse(adr);
		
		client.setMotDePasse("123456");
		client.setNomUtilisateur("test");
		
		client.setHierarchyName(HierachyType.Client);
		client.setNom("david");
		client.setPrenom("grimard");
		
		new Cmd_Client().ajouter(client);
		
		Client retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		//assertTrue(retrievedClient.getAdresseDefaut().getParD�faut() == client.getAdresseDefaut().getParD�faut());
		
		Adresse adr1 = new Adresse();
		adr1.setCodePostal("5");
		adr1.setIdAdresse(UUID.randomUUID());
		adr1.setNo("1");
		adr1.setProvince("2");
		adr1.setRue("1234567890");
		adr1.setVille("4");
		
		new Cmd_Client().AjouterNouvelleAdresseDefaut(client, adr1);
		
		retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
		
		assertTrue(retrievedClient.getAdresseDefaut().getAdresse().getRue().equals(adr1.getRue()));
	
		new Cmd_Client().Suprimmer(client);
	}
	
	@Test
	public void UseCase3_ModifierAdresseDefaut() throws Exception{
		
		Client client  = new Client();
		
		try {
			
			client.setCourriel("kekchose@gmail.com");
			client.setDateNaissance("2012/06/06");
			client.setNumeroTel("456 987 1236");
			client.setId(UUID.randomUUID());
			client.setIdUtilisateur(UUID.randomUUID());
			
			Adresse adr = new Adresse();
			adr.setCodePostal("j8c6h2");
			adr.setIdAdresse(UUID.randomUUID());
			adr.setNo("25");
			adr.setProvince("qu�bec");
			adr.setRue("avenue des glands");
			adr.setVille("lorraine");
			
			client.AddNewAdresse(adr);
			
			client.setMotDePasse("123456");
			client.setNomUtilisateur("test");
			
			client.setHierarchyName(HierachyType.Client);
			client.setNom("david");
			client.setPrenom("grimard");
			
			new Cmd_Client().ajouter(client);
			
			Client retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
			
			//assertTrue(retrievedClient.getAdresseDefaut().getParD�faut() == client.getAdresseDefaut().getParD�faut());
			
			Adresse adr1 = new Adresse();
			adr1.setCodePostal("5");
			adr1.setIdAdresse(UUID.randomUUID());
			adr1.setNo("1");
			adr1.setProvince("2");
			adr1.setRue("1234567890");
			adr1.setVille("4");
			
			new Cmd_Client().AjouterNouvelleAdresseDefaut(client, adr1);
			
			retrievedClient = new Cmd_Client().selectionner(client.getIdUtilisateur());
			
			assertTrue(retrievedClient.getAdresseDefaut().getAdresse().getRue().equals(adr1.getRue()));
			
			
			new Cmd_Client().ModifierAdresseDefaut(client, adr.getIdAdresse());
			
			assertTrue(true);
			
		} catch (Exception e) {
			
		}
		
		
		new Cmd_Client().Suprimmer(client);
	}
	
	@Test
	public void UseCase4_GererMenu() throws Exception {

		Menu menu = new Menu();
		menu.setNom("classic");
		menu.setIdMenu(UUID.randomUUID());
		
		List<Object> itemList = new ArrayList<Object>();
		
		itemList.add(new ItemMenu(UUID.randomUUID(), "bigmac", "getfat", 10.0));
		itemList.add(new ItemMenu(UUID.randomUUID(), "quarterpound", "getfuckingfat", 10.0));
		
		menu.setListeItemMenu(utility.Utility.convertToObjectList((ArrayList<Object>) (itemList)));
		
		new Cmd_Menu().ajouter(menu, UUID.fromString("2ab10833-3904-4648-b304-c87e350449ee"));
		
		new Cmd_Menu().supprimer(menu);
		
	}
	
	@Test
	public void UseCase5_PreparerCommande() throws Exception {
		Menu menu = new Menu();
		menu.setNom("classic");
		menu.setIdMenu(UUID.randomUUID());
		
		List<Object> itemList = new ArrayList<Object>();
		
		itemList.add(new ItemMenu(UUID.randomUUID(), "bigmac", "getfat", 10.0));
		itemList.add(new ItemMenu(UUID.randomUUID(), "quarterpound", "getfuckingfat", 10.0));
		
		menu.setListeItemMenu(utility.Utility.convertToObjectList((ArrayList<Object>) (itemList)));
		
		Restaurant resto = new Restaurant();
		resto.setId(UUID.fromString("2ab10833-3904-4648-b304-c87e350449ee"));
		resto.setMenu(menu); // menu disponible pour ce resto
		
		List<ItemCommande> menuChoisi = new ArrayList<ItemCommande>();
		ItemCommande itemchoisi = new ItemCommande((ItemMenu)resto.getMenu().getListeItemMenu().getNextNoeudValue(), 2);
		menuChoisi.add(itemchoisi); //repas et quantit�
		
		
		Client client = new Client();
		client.setId(UUID.fromString("7f3ae357-089d-4e5b-9e20-f09c46228c55"));
		
		Adresse adr = new Adresse();
		adr.setIdAdresse(UUID.fromString("453ee358-089d-4e5b-9e20-f09c46228c55"));
		//adr.setParD�faut(true);
		//client.AddNewAdresse(adr.getAdresse());
		
		Commande cmd = new Commande();
		
		cmd.setRestaurant(resto); // resto participant
		cmd.setListItem(Utility.convertToObjectList(menuChoisi)); // menu choisi
		cmd.setIdCommande(UUID.randomUUID());
		cmd.setAdresseLivraison(adr);//client qui call la commande
		cmd.setDateCommande(new DateTime(2013,11,2,18,10,0).toString());//date de la commande
		cmd.setStatus(CommandeStatus.Initialized);
		//cmd.setAdresseLivraison(client.getAdresseDefaut().getAdresse());
		cmd.setNoConfirmation(UUID.randomUUID());
			
		
		new Cmd_Commande().enregistrerCommande(cmd);
		
		new Cmd_Commande().CommandePreparationStart(cmd.getIdCommande());
		
		new Cmd_Commande().CommandePreparationEnd(cmd.getIdCommande());
		
		new Cmd_Commande().suprimmerCommande(cmd);
		assertTrue(true);
		
	}

}
