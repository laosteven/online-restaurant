package test;

import static org.junit.Assert.*;

import modele.ModeleMenu;

import object.ItemMenu;
import object.Restaurant;

import org.junit.Test;

import controlleur.ControlleurCreationUtilisateur;
import controlleur.ControlleurMenu;

public class testControleurMenu {

	Restaurant restaurant=new Restaurant();
	ModeleMenu modeleMenu=new ModeleMenu(restaurant);
	ControlleurMenu testControleur = new ControlleurMenu(modeleMenu);

	@Test
	public void testAssignerNom() {
	
		testControleur.assignerNom("Menu de test");
		//assertTrue(testControleur.getMessageErreur(), !testControleur.assignerNom("Menu de test"));
		assertTrue("erreur dans le nom", modeleMenu.getMenuRestaurant().getNom().equals("Menu de test"));
	}

	@Test
	
	public void testNouveauPlat() {
		Boolean[] erreurSurvenu = new Boolean[2];
		erreurSurvenu[0] = false;
		erreurSurvenu[1] = true;
		ItemMenu itemTest=new ItemMenu("test", "test ajout", 10.00);
		Boolean[] erreur=testControleur.TestnouveauPlat(itemTest);
		assertTrue(testControleur.getMessageErreur(),erreur[0]==false);
		assertTrue(testControleur.getMessageErreur(),erreur[1]==true);
	}

	@Test
	public void testEnregistrerMenu() {
		testControleur.assignerNom("Menu de test");
		assertTrue(testControleur.getMessageErreur(),testControleur.TestenregistrerMenu());
	}

	
	@Test
	public void testModifierItem() {
		testControleur.assignerNom("Menu de test");
		Boolean[] erreurSurvenu = new Boolean[2];
		erreurSurvenu[0] = false;
		erreurSurvenu[1] = true;
		ItemMenu itemTest=new ItemMenu("testModifier", "test ", 12.00);
		Boolean[] erreur = testControleur.TestmodifierItem(0,itemTest);
		assertTrue(testControleur.getMessageErreur(),erreur[0] == false);
		assertTrue(testControleur.getMessageErreur(),erreur[1] == true);
	}

	

}
