package test;

import static org.junit.Assert.*;

import java.util.UUID;

import org.junit.Test;

import controlleur.ControlleurCreationUtilisateur;

public class testControlleurCreationUtilisateur {

	
private ControlleurCreationUtilisateur testControleur=new ControlleurCreationUtilisateur();

	@Test
	public void testCreeUtilisateur() {
		//(UUID idUtilisateur, UUID idClient, String nomRef, String prenomRef,
				//String nomUtilisateurRef, String numTelephoneRef, String motDePasseRef,
				//String confirmPwdRef, String dateNaissanceRef, String courrielRef, Boolean flag)
		
		try {
			assertFalse(testControleur.getMessageErreur(), testControleur.TestcreeUtilisateur("Smith", "John","JohnSmith", "458-787-9987","Passw0rd", "Passw0rd", "1987/11/24", "test@test.com"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue("Erreur de nom",testControleur.getNom().equals("Smith"));
		assertTrue("Erreur de prenom",testControleur.getPrenom().equals("John"));
		assertTrue("Erreur de nom d'utilisateur",testControleur.getNomUtilisateur().equals("JohnSmith"));
		assertTrue("Erreur de no de telephone",testControleur.getNumTelephone().equals("458-787-9987"));
		assertTrue("Erreur de mot de passe",testControleur.getMotDePasse().equals("Passw0rd"));
		assertTrue("Erreur de nom d'utilisateur",testControleur.getDateNaissance().equals("1987/11/24"));
		assertTrue("Erreur de no de telephone",testControleur.getCourriel().equals("test@test.com"));
		assertTrue("Erreur de mot de passe",testControleur.getConfirmationPwd().equals("Passw0rd"));
		
		
	}

	public testControlleurCreationUtilisateur() {
	testCreeUtilisateur();
	}



}
