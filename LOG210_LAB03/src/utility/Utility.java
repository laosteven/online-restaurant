package utility;

import java.util.ArrayList;
import java.util.List;
import object.AfficheObject;
import object.ItemMenu;
import object.Restaurant;
import modele.ObjetListe;

public class Utility {
	
	public static ObjetListe convertToObjectList(List<?> list){
		
		ObjetListe objectList = new ObjetListe();
		
		for (Object object : list) {
			objectList.addEnd(object);
		}
		
		objectList.setListBeginning();
		
		return objectList;
		
	}
	
	public static ObjetListe convertToAffichageObjectList(List<?> list){
		
		ObjetListe objectList = new ObjetListe();
		
		for (Object object : list) {
			objectList.addEnd(new AfficheObject(object, true));
		}
		
		objectList.setListBeginning();
		
		return objectList;
		
	}
	
	public static List<ItemMenu> ObjectRestaurantToItemMenuList(ObjetListe list){
		
		List<ItemMenu> itemList = new ArrayList<ItemMenu>();
		
		ObjetListe obj = ((Restaurant)list.getObjectAndIterate()).getMenu().getListeItemMenu();
		
		while(obj.getObject() != null){
			
			itemList.add((ItemMenu)obj.getObjectAndIterate());			
		}
		
		return itemList;
	}
	
}
