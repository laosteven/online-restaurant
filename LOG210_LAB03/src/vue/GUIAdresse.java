/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIAdresse.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-11-08 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import commande.Cmd_Client;
import controlleur.ControlleurCreationUtilisateur;
import donneGenerique.Adresse;
import modele.ModeleCommander;
import modele.ModeleLivreur;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.UUID;

/***************************************************************
 * Fenetre pour recuperation de donnees d'adresse
 ***************************************************************/
public class GUIAdresse{

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private Adresse nouvAdresse;
	private boolean adrFonctionnelle;
	private JTextField txtNumero;
	private JTextField txtRue;
	private JTextField txtVille;
	private JTextField txtProvince;
	private JTextField txtCodePostal;
	
	private ModeleLivreur modeleLivreur;
	private ModeleCommander modeleCommander;
	private ControlleurCreationUtilisateur controleurCreation;
	
	private GUILivreur GUILivr;
	private GUIClientConfirm GUIConfirm;
	
	/***************************************************************
	 * Accesseur
	 ***************************************************************/
	public boolean getAdrFonctionnelle(){
		return adrFonctionnelle;
	}

	/***************************************************************
	 * Constructeur
	 * 
	 * Creation d'adresse pour un client
	 * @wbp.parser.constructor 
	 ***************************************************************/
	public GUIAdresse(GUIClientConfirm GUIConfirmRef, ModeleCommander modeleCommanderRef) {
		
		GUIConfirm = GUIConfirmRef;
		modeleCommander = modeleCommanderRef;
		initComposant();

	}

	/***************************************************************
	 * Constructeur
	 * 
	 * Creation d'adresse pour un livreur
	 * @wbp.parser.constructor 
	 ***************************************************************/
	public GUIAdresse(GUILivreur GUILivrRef, ModeleLivreur modeleLivreurRef) {

		GUILivr = GUILivrRef;
		modeleLivreur = modeleLivreurRef;
		initComposant();

	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Adresse");
		frame.setResizable(false);
		frame.setBounds(100, 100, 225, 240);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Message d'instruction supplementaire
		JLabel lblInstruction = new JLabel(
				"Remplissez les informations suivantes:");
		lblInstruction.setBounds(10, 11, 183, 14);
		contentPane.add(lblInstruction);

		// Titre pour numero d'adresse
		JLabel lblNumero = new JLabel("Numero :");
		lblNumero.setBounds(10, 39, 46, 14);
		contentPane.add(lblNumero);

		// Zone de texte pour numero d'adresse
		txtNumero = new JTextField();
		txtNumero.setBounds(84, 36, 127, 20);
		contentPane.add(txtNumero);
		txtNumero.setColumns(10);

		// Titre pour la rue
		JLabel lblRue = new JLabel("Rue :");
		lblRue.setBounds(10, 67, 46, 14);
		contentPane.add(lblRue);

		// Zone de texte pour la rue
		txtRue = new JTextField();
		txtRue.setBounds(84, 64, 127, 20);
		contentPane.add(txtRue);
		txtRue.setColumns(10);

		// Titre pour la ville
		JLabel lblVille = new JLabel("Ville :");
		lblVille.setBounds(10, 95, 46, 14);
		contentPane.add(lblVille);

		// Zone de texte pour la ville
		txtVille = new JTextField();
		txtVille.setColumns(10);
		txtVille.setBounds(84, 92, 127, 20);
		contentPane.add(txtVille);

		// Titre pour province
		JLabel lblProvince = new JLabel("Province :");
		lblProvince.setBounds(10, 123, 48, 14);
		contentPane.add(lblProvince);

		// Zone de texte pour province
		txtProvince = new JTextField();
		txtProvince.setColumns(10);
		txtProvince.setBounds(84, 120, 127, 20);
		contentPane.add(txtProvince);

		// Titre pour code postal
		JLabel lblCodePostal = new JLabel("Code postal :");
		lblCodePostal.setBounds(10, 151, 64, 14);
		contentPane.add(lblCodePostal);

		// Zone de texte pour code postal
		txtCodePostal = new JTextField();
		txtCodePostal.setColumns(10);
		txtCodePostal.setBounds(84, 148, 127, 20);
		contentPane.add(txtCodePostal);

		// Bouton d'annulation
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	
				// Si l'utilisateur est livreur
				if(modeleLivreur != null) {
					new GUIConnexion();
				}
				
				frame.dispose();
				
			}
		});
		btnAnnuler.setBounds(10, 179, 89, 23);
		contentPane.add(btnAnnuler);

		// Bouton de confirmation
		JButton btnConfirmer = new JButton("Confirmer");
		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				creerAdresse();
			}
		});
		btnConfirmer.setBounds(122, 179, 89, 23);
		contentPane.add(btnConfirmer);

		// Ajout d'ecouteur pour chaque JTextField
		JTextField[] txtTab = { txtNumero, txtRue, txtVille, txtProvince,
				txtCodePostal };
		ecouteurTextField(txtTab);
		controleurCreation = new controlleur.ControlleurCreationUtilisateur();
	
		// Affichage de la fenetre
		frame.validate();
		frame.setVisible(true);

	}

	/***************************************************************
	 * Ajout d'ecoute pour chaque JTextField
	 ***************************************************************/
	public void ecouteurTextField(JTextField[] txtTab){
	
		for (JTextField txtTabRef : txtTab)
			txtTabRef.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					creerAdresse();
				}
			});

	}

	/***************************************************************
	 * Sequence de confirmation
	 * Assigne l'adresse selon l'utilisateur connecte
	 ***************************************************************/
	public void confirmer() {
		
		// Verification du niveau d'administratif de l'utilisateur


		// Si l'utilisateur est un client
		if(modeleLivreur == null) {
			
			modeleCommander.getClientEnCours().AddNewAdresse(nouvAdresse);
			
			// Nouvelle adresse par defaut pour le client
			try {
				new Cmd_Client().AjouterNouvelleAdresseDefaut(
						modeleCommander.getClientEnCours(), nouvAdresse);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			
			// L'adresse est enregistre
			JOptionPane.showMessageDialog(frame, "Adresse enregistree.");
			
			GUIConfirm.update(null, null);
			
		} 
		
		else {
			
			// Mise a jour de l'emplacement du livreur actuel
			modeleLivreur.getLivreur().setAdresseActuelle(nouvAdresse);
			
			// L'adresse est enregistre
			JOptionPane.showMessageDialog(frame, "Adresse enregistree.");
			
			// Mise en page de la fenetre
			GUILivr.debute(getAdrFonctionnelle(), modeleLivreur);
			
		}

		frame.dispose();

	}
	
	/***************************************************************
	 * Creation d'une nouvelle adresse
	 ***************************************************************/
	public void creerAdresse(){

		// Verifie si les champs sont vides
		if(txtNumero.getText().isEmpty() &&
			txtRue.getText().isEmpty() &&
			txtVille.getText().isEmpty() &&
			txtProvince.getText().isEmpty() && 
			txtCodePostal.getText().isEmpty()) {
			
			JOptionPane.showMessageDialog(frame, "Veuillez remplir " +
					"tous les champs avant de poursuivre.");
		
		} else if(!controleurCreation.verifieCodePostal(txtCodePostal.getText())) {
			
			JOptionPane.showMessageDialog(frame, "Votre code postal " +
					"est invalide.");
			
		} else {
			
			// Creation d'une nouvelle adresse
			nouvAdresse = new Adresse(
					UUID.randomUUID(),
					txtNumero.getText(), 
					txtRue.getText(), 
					txtVille.getText(),
					txtProvince.getText(), 
					txtCodePostal.getText());
			
			adrFonctionnelle = true;
			
			// Enregistrement dans base de donnee
			confirmer();
				
		}
		
	}
	
}
