/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIAjoutPlat.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import controlleur.ControlleurMenu;
import object.ItemMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;

/***************************************************************
 * Fenetre pour l'enregistrement / modification du plat
 ***************************************************************/
public class GUIAjoutPlat {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private JTextField txtNom;
	private JLabel lblDescription;
	private JButton btnEnregistrer;
	private JButton btnAnnuler;
	private JTextField txtMontant;
	private JTextPane txtDescription;
	private controlleur.ControlleurMenu controleur;
	private JFrame frameParent;
	private boolean modifier = false;
	private ItemMenu itemAmodifier;
	private int indexModif;
	private JSeparator sep1;
	private JSeparator sep2;
	private JSeparator sep3;

	/***************************************************************
	 * Constructeur par defaut
	 * 
	 * @wbp.parser.constructor
	 ***************************************************************/
	public GUIAjoutPlat() {

		initComposant();

	}
	
	/***************************************************************
	 * Constructeur: Creation d'un nouveau plat
	 * 
	 * @wbp.parser.constructor
	 ***************************************************************/
	public GUIAjoutPlat(ControlleurMenu controleurAjouterMenu, JFrame frameRef) {

		controleur = controleurAjouterMenu;
		frameParent = frameRef;
		initComposant();

	}

	/***************************************************************
	 * Constructeur: Modification d'un plat enregistre
	 * 
	 * @param test
	 ***************************************************************/
	public GUIAjoutPlat(ItemMenu itemModifier, int indexModifier,
			JFrame frameRef, ControlleurMenu controleurAjouterMenu) {

		modifier = true;
		itemAmodifier = itemModifier;
		indexModif = indexModifier;
		frameParent = frameRef;
		controleur = controleurAjouterMenu;
		initComposant();
		txtDescription.setText(itemModifier.getDescription());
		txtMontant.setText(itemModifier.getPrix().toString());
		txtNom.setText(itemModifier.getNom());
	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation de la fenetre
		frame = new JFrame();
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setBounds(100, 100, 370, 310);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Instruction
		JLabel lblInstruction = new JLabel(
				"Veuillez remplir les informations suivantes :");
		lblInstruction.setBounds(10, 11, 304, 14);
		contentPane.add(lblInstruction);
		
		sep1 = new JSeparator();
		sep1.setBounds(10, 36, 342, 2);
		contentPane.add(sep1);

		// Titre pour le plat
		JLabel lblNom = new JLabel("Nom du plat :");
		lblNom.setBounds(10, 52, 64, 14);
		contentPane.add(lblNom);

		// Zone de texte pour le nom
		txtNom = new JTextField();
		txtNom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				enregistrement();

			}
		});
		txtNom.setBounds(84, 49, 154, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);

		// Titre pour le montant
		JLabel lblMontant = new JLabel("Montant :");
		lblMontant.setBounds(248, 52, 47, 14);
		contentPane.add(lblMontant);

		// Zone de texte pour le montant
		txtMontant = new JTextField();
		txtMontant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				enregistrement();

			}
		});
		txtMontant.setColumns(10);
		txtMontant.setBounds(305, 49, 47, 20);
		contentPane.add(txtMontant);
		
		sep2 = new JSeparator();
		sep2.setBounds(10, 77, 342, 2);
		contentPane.add(sep2);

		// Titre de la description
		lblDescription = new JLabel("Description :");
		lblDescription.setBounds(10, 93, 60, 14);
		contentPane.add(lblDescription);

		// Zone de texte pour la description
		txtDescription = new JTextPane();
		txtDescription.setBounds(84, 90, 268, 136);
		contentPane.add(txtDescription);

		// Bouton d'annulation
		btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				annulation();
			}
		});
		
		sep3 = new JSeparator();
		sep3.setBounds(10, 237, 342, 2);
		contentPane.add(sep3);
		btnAnnuler.setBounds(10, 250, 89, 23);
		contentPane.add(btnAnnuler);

		// Bouton d'enregistrement
		btnEnregistrer = new JButton("Enregistrer");
		btnEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				enregistrement();
			}
		});
		btnEnregistrer.setBounds(263, 250, 89, 23);
		contentPane.add(btnEnregistrer);

	}

	/***************************************************************
	 * Sequence d'enregistrement
	 ***************************************************************/
	public void enregistrement() {
		
		ItemMenu plat;
		Boolean[] added;
		
		if (modifier) {
			
			plat = new ItemMenu(itemAmodifier.getIdItemMenu(),
					txtNom.getText(), txtDescription.getText(),
					Double.parseDouble(txtMontant.getText()));
			added = controleur.modifierItem(indexModif, plat);

		} else {
			Double prix=null;
			if(!txtMontant.getText().isEmpty())
			{prix=Double.parseDouble(txtMontant.getText());}
			//rajouter handle de mettre des lettres
			
			plat = new ItemMenu(txtNom.getText(), txtDescription.getText(),prix
					);
			added = controleur.nouveauPlat(plat);
		}

		if (added[0]) {
			JOptionPane.showMessageDialog(frame, controleur.getMessageErreur());
		}

		if (added[1]) {
			
			frameParent.setVisible(true);
			frame.dispose();
			
		}
		
	}

	/***************************************************************
	 * Sequence d'annulation
	 ***************************************************************/
	public void annulation() {

		// Verifie si les cases sont vides
		if (!(isVideNom() && isVideDescription() && isVideMontant())) {

			// Confirme si l'utilisateur veut quitter
			int choix = JOptionPane
					.showConfirmDialog(
							frame,
							"Etes-vous sur de quitter sans enregistrer?"
									+ " Vous risquez de perdre vos informations inscrites.",
							"Attention!", JOptionPane.YES_NO_OPTION);

			// Fermer la fenetre
			if (choix == JOptionPane.YES_OPTION)
				frame.dispose();

		} else {
			frameParent.setVisible(true);

			// Si aucune case n'est remplie
			frame.dispose();

		}

	}

	/***************************************************************
	 * Determine si la case de nom est vide
	 * 
	 * @return Si le nom est remplie
	 ***************************************************************/
	public boolean isVideNom() {

		return txtNom.getText().isEmpty();

	}

	/***************************************************************
	 * Determine si le montant est correct
	 * 
	 * @return Si le montant est verifie
	 ***************************************************************/
	public boolean isVideMontant() {

		return txtMontant.getText().isEmpty()
				|| txtMontant.getText().equals("0.00");

	}

	/***************************************************************
	 * Verifie si la case de description est vide
	 * 
	 * @return Si la description n'est pas remplie
	 ***************************************************************/
	public boolean isVideDescription() {

		return txtDescription.getText().isEmpty();

	}

}
