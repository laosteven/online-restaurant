/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIClient.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import commande.Cmd_Restaurant;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;

import controlleur.ControlleurCommande;
import dataAccessLayer.SqlController;
import dataAccessLayer.SqlManager;
import modele.ModeleCommander;
import modele.ObjetListe;
import object.Client;
import object.Restaurant;
import object.ItemMenu;
import utility.Utility;

/***************************************************************
 * Fenetre pour le client
 ***************************************************************/
public class GUIClientCommande implements Observer {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;

	private Boolean indexChanged=false;

	private modele.ModeleCommander modele;
	private controlleur.ControlleurCommande controlleur;

	private DefaultListModel<String> listModelRestaurant = new DefaultListModel<String>();
	private DefaultListModel<String> listModelMenu = new DefaultListModel<String>();

	private JList<String> lResto = new JList<String>(listModelRestaurant);
	private JList<String> lMenu = new JList<String>(listModelMenu);
	private boolean isDysplaing=false;

	/***************************************************************
	 * Constructeur par defaut
	 * 
	 * @wbp.parser.constructor
	 ***************************************************************/
	public GUIClientCommande(ModeleCommander donneModele) {

		initComposant();
		modele = donneModele;
		controlleur = new ControlleurCommande(modele);
		modele.addObserver(this);
		update(null,null);
	}

	/***************************************************************
	 * Methode pour la creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Prise de commande | LOG210 - Iteration 2 ");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 700, 400);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Panneau Restaurant
		JPanel pRestaurant = new JPanel();
		pRestaurant.setLayout(null);
		pRestaurant.setBorder(new TitledBorder(null, "Restaurants",
		TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pRestaurant.setBounds(10, 11, 330, 311);
		contentPane.add(pRestaurant);

		// Panneau Restaurant avec barre de defilement pour faciliter la
		// navigation
		JScrollPane spRestaurant = new JScrollPane(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spRestaurant.setBounds(10, 24, 310, 276);
		pRestaurant.add(spRestaurant);
		
		// Ecouteur de la liste de Restaurant
		lResto.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				
				indexChanged=true;
				controlleur.selectedRestaurantIndexChange(lResto
						.getSelectedIndex());
			
				
				lMenu.clearSelection();
				

			}
		});
		lResto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spRestaurant.setViewportView(lResto);

		// Panneau Menu
		JPanel pMenu = new JPanel();
		pMenu.setLayout(null);
		pMenu.setBorder(new TitledBorder(null, "Menu",
		TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pMenu.setBounds(354, 11, 330, 311);
		contentPane.add(pMenu);

		// Panneau Menu avec barre de defilement
		JScrollPane spMenu = new JScrollPane(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spMenu.setBounds(10, 24, 310, 276);
		pMenu.add(spMenu);

		// Tableau de menu disponible pour un restaurant selectionne
		lMenu.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spMenu.setViewportView(lMenu);
		
		// Ecouteur de la liste de Menu
		lMenu.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (!isDysplaing) {
					controlleur.selectedMenuIndexChange(lMenu
							.getSelectedIndex());
				}
				
			}
		});

		// Bouton de deconnexion
		JButton btnQuitter = new JButton("Deconnexion");
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Verifier si la commande est vide
				modele = null;
				controlleur = null;
				new GUIConnexion();
				frame.dispose();

			}
		});
		btnQuitter.setBounds(10, 333, 115, 23);
		contentPane.add(btnQuitter);
		
		// Bouton de modification du compte (Inactif pour le moment)
		JButton btnModifier = new JButton("Modifier compte");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client client;
				try {
					client = new SqlController(new SqlManager()).Selectionner_Client(modele.getClientEnCours().getIdUtilisateur());
					ModeleCommander model = new ModeleCommander(Utility.convertToObjectList(new Cmd_Restaurant().selectionnerETRetour()), client);

					modele = model;
					update(null,null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				new GUIClientNouv(modele.getClientEnCours());
				

			}
		});
		btnModifier.setBounds(231, 333, 109, 23);
		contentPane.add(btnModifier);

		// Selection d'une commande et ajout dans la tableau de commande
		JButton btnSelection = new JButton("Passer une commande ");
		btnSelection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// S'assure qu'un restaurant est selectionne de la liste
				if(!lResto.isSelectionEmpty()){
					
					// Cache la fenetre pour garder la simplicite
					frame.setVisible(false);
					new GUIClientMenu(frame,modele);	
					
				}
				
				
			}
		});
		btnSelection.setBounds(539, 333, 145, 23);
		contentPane.add(btnSelection);

		// Bouton de description du plat selectionne
		JButton btnDescription = new JButton("Description du plat");
		btnDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Affichage de la description
				controlleur.afficherDescription();

			}
		});
		btnDescription.setBounds(406, 333, 123, 23);
		contentPane.add(btnDescription);

		// Validation et affichage
		frame.validate();
		frame.setVisible(true);

	}

	// Afficher liste de resto et menu
	public void displayRestaurantList(ObjetListe restaurantList) {
		restaurantList.setListBeginning();

		listModelRestaurant.clear();

		while (restaurantList.getObject() != null) {

			Restaurant restaurant = (Restaurant) restaurantList
					.getObjectAndIterate();
			String nom = restaurant.getNom();
			listModelRestaurant.addElement(nom);

		}

	}

	public void displayMenu(Restaurant restaurantSelectionner) {
		
		isDysplaing=true;
		object.Menu menuRestaurant = restaurantSelectionner.getMenu();
		ObjetListe listMenu = menuRestaurant.getListeItemMenu();
		listMenu.setListBeginning();
		listModelMenu.clear();

		menuRestaurant.getListeItemMenu().getNbElement();
		while (listMenu.getObject() != null) {

			ItemMenu item = (ItemMenu) listMenu.getObjectAndIterate();
			String nom = item.getNom();
			String prix = item.getPrix().toString();
			listModelMenu.addElement(nom + ' ' + prix);

		}

		isDysplaing=false;
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		if(!indexChanged){
			displayRestaurantList(modele.getListRestaurant());
			}

		if(modele.getSelectedRestaurant() != null) {
			displayMenu(modele.getSelectedRestaurant());
		}

	}
}