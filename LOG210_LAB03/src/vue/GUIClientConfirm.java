/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIClientConfirm.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JList;

import commande.Cmd_Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import controlleur.ControlleurCommande;
import object.AdresseClient;
import object.ItemCommande;
import donneGenerique.Adresse;
import modele.ModeleCommander;
import modele.ObjetListe;

/***************************************************************
 * Fenetre de confirmation de commande pour le client
 ***************************************************************/
public class GUIClientConfirm implements Observer {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JFrame parent;
	private JPanel contentPane;
	
	private JTextField txtMontant;
	
	private ModeleCommander modeleCommander;
	private ControlleurCommande controlleur;
	
	private JSpinner sDate;
	
	ArrayList<Adresse> listAdresse = new ArrayList<Adresse>();
	private DefaultComboBoxModel<String> listComboBoxAdresse = new DefaultComboBoxModel<String>();
	private JComboBox<String> cbAdresse = new JComboBox<>(listComboBoxAdresse);
	private DefaultComboBoxModel<String> model;

	private DefaultListModel<String> listModelCarnet = new DefaultListModel<String>();
	private JList<String> lCarnet = new JList<String>(listModelCarnet);

	/***************************************************************
	 * Constructeur par defaut
	 * 
	 * @wbp.parser.constructor
	 ***************************************************************/
	public GUIClientConfirm(JFrame parentRef) {

		parent = parentRef;
		initComposant();
		cbAdresse.getItemCount();

	}

	/***************************************************************
	 * Constructeur avec modele de commande des clients
	 * 
	 * @param parentRef
	 * @param modeleDone
	 ***************************************************************/
	public GUIClientConfirm(JFrame parentRef, ModeleCommander modeleDone) {

		parent = parentRef;
		modeleCommander = modeleDone;
		controlleur = new ControlleurCommande(modeleCommander);
		modeleCommander.addObserver(this);
		initComposant();
		update(null, null);

	}

	/***************************************************************
	 * Methode de fermeture de fenetre
	 ***************************************************************/
	private void close() {

		parent.setVisible(true);
		frame.dispose();
		modeleCommander.deleteObserver(this);

	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Confirmation de commande | LOG210 - Iteration 2");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 355, 445);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Message d'instruction
		JLabel lblInstruction = new JLabel(
				"<html><p align = \"justify\">Pour confirmer "
						+ "la commande, veuillez verifiez si les informations"
						+ " suivantes sont correctes:</p><html>");
		lblInstruction.setBounds(10, 11, 329, 23);
		contentPane.add(lblInstruction);

		// Separateur instruction - liste
		JSeparator sep1 = new JSeparator();
		sep1.setBounds(10, 45, 329, 2);
		contentPane.add(sep1);

		// Panneau de carnet
		JPanel pCarnet = new JPanel();
		pCarnet.setBorder(new TitledBorder(null, "Carnet de commande",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pCarnet.setBounds(10, 58, 329, 205);
		contentPane.add(pCarnet);
		pCarnet.setLayout(null);

		// Panneau de defilement pour la liste de carnet
		JScrollPane spCarnet = new JScrollPane();
		spCarnet.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spCarnet.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spCarnet.setBounds(10, 22, 309, 172);
		pCarnet.add(spCarnet);

		// Liste de carnet
		spCarnet.setViewportView(lCarnet);

		// Titre du montant
		JLabel lblMontant = new JLabel("Montant total :");
		lblMontant.setBounds(10, 274, 72, 14);
		contentPane.add(lblMontant);

		// Texte du montant
		txtMontant = new JTextField();
		txtMontant.setEditable(false);
		txtMontant.setHorizontalAlignment(SwingConstants.RIGHT);
		txtMontant.setText("0.00 $");
		txtMontant.setBounds(250, 271, 86, 20);
		contentPane.add(txtMontant);
		txtMontant.setColumns(10);

		// Separateur liste - adresse
		JSeparator sep2 = new JSeparator();
		sep2.setBounds(10, 299, 329, 2);
		contentPane.add(sep2);

		// Titre date de livraison
		JLabel lblDate = new JLabel("Date de livraison (mm/jj/aa):");
		lblDate.setBounds(10, 315, 145, 14);
		contentPane.add(lblDate);

		// Spinner pour confirmation de la date de livraison
		sDate = new JSpinner();
		sDate.setModel(new SpinnerDateModel(new Date(), null, null,
				Calendar.DAY_OF_WEEK));
		sDate.setBounds(168, 312, 171, 20);
		contentPane.add(sDate);

		// Titre de l'adresse
		JLabel lblAdresse = new JLabel("Adresse :");
		lblAdresse.setBounds(10, 343, 150, 14);
		contentPane.add(lblAdresse);

		// ComboBox de l'adresse
		final JComboBox<String> cbAdresse = new JComboBox<String>();
		String[] items = adresseToString(modeleCommander.getClientEnCours()
				.getListAdresse());
		model = new DefaultComboBoxModel<String>(items);
		cbAdresse.setModel(model);

		cbAdresse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int count = cbAdresse.getSelectedIndex();
				modeleCommander.getClientEnCours().getListAdresse()
						.setListBeginning();

				for (int i = 0; i < count; i++) {
					modeleCommander.getClientEnCours().getListAdresse()
							.getObjectAndIterate();
				}

				AdresseClient adr = (AdresseClient) modeleCommander
						.getClientEnCours().getListAdresse().getObject();
				modeleCommander.getClientEnCours().SetAdresseDefaut(
						adr.getAdresse());

				try {
					new Cmd_Client().ModifierAdresseDefaut(
							modeleCommander.getClientEnCours(),
							adr.getIdAdresse());
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});
		cbAdresse.setBounds(168, 340, 171, 20);
		contentPane.add(cbAdresse);

		// Separateur adresse - commande
		JSeparator sep3 = new JSeparator();
		sep3.setBounds(10, 369, 329, 2);
		contentPane.add(sep3);

		// Bouton pour annuler la commande
		JButton btnAnnuler = new JButton("Retour");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				close();
			}
		});
		btnAnnuler.setBounds(10, 382, 89, 23);
		contentPane.add(btnAnnuler);

		// Bouton pour confirmer la commande
		JButton btnConfirmer = new JButton("Confirmer");
		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Affichage du numero de confirmation de la commande
				String message = controlleur.passerCommande(sDate.getValue()
						.toString());
				JOptionPane.showMessageDialog(frame, message);
				
				// Fermeture de la fenetre
				close();

			}
		});
		btnConfirmer.setBounds(250, 382, 89, 23);
		contentPane.add(btnConfirmer);
		
		// Bouton d'ajout d'adresse
		JButton btnNouvelleAdresse = new JButton("Nouvelle Adresse");
		btnNouvelleAdresse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				creationAdresse();
			}
		});
		btnNouvelleAdresse.setBounds(109, 382, 131, 23);
		contentPane.add(btnNouvelleAdresse);
		
		// Validation et affichage
		frame.validate();
		frame.setVisible(true);
		
	}

	@Override
	public void update(Observable o, Object arg) {
		
		String[] items = adresseToString(modeleCommander.getClientEnCours()
				.getListAdresse());
		model = new DefaultComboBoxModel<String>(items);

		// Nettoyage de la liste
		lCarnet.removeAll();
		listModelCarnet.removeAllElements();
		
		// Montant total de la commande
		txtMontant.setText(modeleCommander.getTotal().toString() + " $");

		// Initialiser l'ordre de la liste d'adresse
		modeleCommander.getClientEnCours().getListAdresse().setListBeginning();

		// Tant que la liste d'adresse n'est pas vide
		while (modeleCommander.getClientEnCours().getListAdresse().getObject() != null) {

			// On recupere l'adresse
			AdresseClient adresseLivraison = (AdresseClient) modeleCommander
					.getClientEnCours().getListAdresse().getObjectAndIterate();

			// On rajoute dans la liste du carnet
			cbAdresse.addItem(adresseLivraison.getAdresse().toStringSpaced());

		} // Fin du 'while'

		modeleCommander.getClientEnCours().getListAdresse().setListBeginning();
		modeleCommander.getClientEnCours().getAdresseDefaut().getAdresse();

		// Initialiser l'ordre de la liste de commande
		modeleCommander.getCommande().setListBeginning();
		while (modeleCommander.getCommande().getObject() != null) {

			// On recupere les commandes
			ItemCommande item = (ItemCommande) (modeleCommander.getCommande()
					.getObjectAndIterate());

			// Ajout des commandes dans le carnet
			listModelCarnet.addElement(item.toString());

		} // Fin du 'while'
		
		frame.setVisible(true);
		
	}

	/***************************************************************
	 * Modification des adresses en String pour le comboBox
	 ***************************************************************/
	private String[] adresseToString(ObjetListe adresse) {
		
		adresse.setListBeginning();
		String[] adresseTbl = new String[adresse.getNbElement()];

		int index = 0;
		while (adresse.getObject() != null) {
			AdresseClient add = (AdresseClient) adresse.getObjectAndIterate();

			if (add.getAdrParDéfaut() && index != 0) {

				String temp = adresseTbl[0];
				adresseTbl[0] = add.getAdresse().toStringSpaced();
				adresseTbl[index] = temp;

			} else {

				adresseTbl[index] = add.getAdresse().toStringSpaced();
			}
			
			index++;
			
		}

		return adresseTbl;
		
	}
	
	/***************************************************************
	 * Creation d'adresse
	 ***************************************************************/
	public void creationAdresse() {

		new GUIAdresse(this, modeleCommander);
		
	}
	
}
