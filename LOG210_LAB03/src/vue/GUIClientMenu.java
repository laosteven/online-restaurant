/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIMenu.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import modele.ModeleCommander;
import modele.ObjetListe;
import object.ItemCommande;
import object.ItemMenu;
import javax.swing.UIManager;
import controlleur.ControlleurCommande;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

/***************************************************************
 * Fenetre pour le client
 ***************************************************************/
public class GUIClientMenu implements Observer {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private JButton btnConfirme;
	
	private modele.ModeleCommander modele;
	private controlleur.ControlleurCommande controlleur;
	private JTextField txtMontant;
	private JFrame frameParent;
	private DefaultListModel<String> listModelCarnet = new DefaultListModel<String>();
	private JList<String> lCarnet = new JList<String>(listModelCarnet);
	private DefaultListModel<String> listModelMenu = new DefaultListModel<String>();
	private JList<String> lMenu = new JList<String>(listModelMenu);
	private boolean ajoutEnCours = false;

	/***************************************************************
	 * Constructeur par defaut
	 * @wbp.parser.constructor
	 ***************************************************************/
	public GUIClientMenu(JFrame parent, ModeleCommander modeleParent) {
		
		frameParent = parent;
		
		initComposant();

		modele = modeleParent;
		controlleur = new ControlleurCommande(modele);
		modele.addObserver(this);
		update(null,null);
		txtMontant.setEditable(false);
		
	}

	/***************************************************************
	 * Fermeture de la fenetre active et ouverture de la 
	 * fenetre du client
	 ***************************************************************/
	private void close() {
		
		// Detachement d'observateur
		modele.deleteObserver(this);
		controlleur.supprimerCommande();
		
		// Remet GUIClient a visible
		frameParent.setVisible(true);
		
		// Disposition de la fenetre
		frame.dispose();
		
	}
	
	/***************************************************************
	 * Methode pour la creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Prise de commande | LOG210 - Iteration 2 ");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				close();
			}
		});
		frame.setBounds(100, 100, 690, 400);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Panneau de Menu
		JPanel pMenu = new JPanel();
		pMenu.setLayout(null);
		pMenu.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Menu",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pMenu.setBounds(10, 11, 329, 316);
		contentPane.add(pMenu);

		// Panneau avec barre de defilement pour Menu
		JScrollPane spMenu = new JScrollPane(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spMenu.setBounds(10, 24, 309, 247);
		pMenu.add(spMenu);
		lMenu.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(!ajoutEnCours){
				controlleur.selectedMenuIndexChange(lMenu.getSelectedIndex());}
			}
		});
		spMenu.setViewportView(lMenu);

		// Bouton de description
		JButton btnDescription = new JButton("Description du plat");
		btnDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Affichage du plat selectionne
				controlleur.afficherDescription();

			}
		});
		btnDescription.setBounds(10, 282, 121, 23);
		pMenu.add(btnDescription);

		// Bouton d'ajout
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ajoutEnCours=true;
				
				// Ajouter le plat selectionne dans le carnet de commande
				controlleur.ajouterItemCarnet();
				
				ajoutEnCours=false;

			}
		});
		btnAjouter.setBounds(230, 282, 89, 23);
		pMenu.add(btnAjouter);

		// Panneau de carnet de commnade
		JPanel pCarnet = new JPanel();
		pCarnet.setLayout(null);
		pCarnet.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Carnet de commande",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pCarnet.setBounds(349, 11, 329, 316);
		contentPane.add(pCarnet);

		// Panneau defilement pour la liste du carnet de commmande
		JScrollPane spCarnet = new JScrollPane(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spCarnet.setBounds(10, 24, 309, 247);
		pCarnet.add(spCarnet);
		spCarnet.setViewportView(lCarnet);

		// Bouton supprimer
		JButton btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Si un objet est selectionne 
				if(!lCarnet.isSelectionEmpty()){
					
					// L'index selectionne sera retire
					listModelCarnet.remove(lCarnet.getSelectedIndex());
					controlleur.supprimerItemCarnet(lCarnet.getSelectedIndex());
					
				}
				
			}
		});
		btnSupprimer.setBounds(10, 282, 89, 23);
		pCarnet.add(btnSupprimer);

		// Titre pour le montant
		JLabel lblMontant = new JLabel("Montant :");
		lblMontant.setBounds(192, 288, 47, 14);
		pCarnet.add(lblMontant);

		// Texte du montant
		txtMontant = new JTextField();
		txtMontant.setHorizontalAlignment(SwingConstants.RIGHT);
		txtMontant.setText("0.00 $");
		txtMontant.setBounds(249, 285, 70, 20);
		pCarnet.add(txtMontant);
		txtMontant.setColumns(10);

		// Bouton pour annuler une commande
		JButton btnAnnuler = new JButton("Annuler la commande");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Si le carnet contient des commandes actives
				if(!listModelCarnet.isEmpty()){
					
					// Confirmation de l'utilisateur
					int choix = JOptionPane.showConfirmDialog(
						    frame,
						    "Etes-vous sur d'annuler votre commande? " +
						    "Vous risquez de perdre vos donnees.",
						    "Attention",
						    JOptionPane.YES_NO_OPTION);
					
					// Si oui, fenetre active se ferme
					if(choix == JOptionPane.YES_OPTION) {
						modele.getCommande().clearList();
						close();
					}
					
				}
				else
					close();
				
			}
		});
		btnAnnuler.setBounds(10, 338, 135, 23);
		contentPane.add(btnAnnuler);

		btnConfirme = new JButton("Confirmer la commande");
		btnConfirme.setEnabled(false);
		btnConfirme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.setVisible(false);
				new GUIClientConfirm(frame, modele);
				
			}
		});
		btnConfirme.setBounds(529, 338, 145, 23);
		contentPane.add(btnConfirme);

		// Validation et affichage
		frame.validate();
		frame.setVisible(true);

	}

	/***************************************************************
	 * Affichage du carnet de commande que le client a choisi dans
	 * le restaurant
	 * 
	 * @param commandeClient La liste que le client desire commander
	 ***************************************************************/
	public void displayCarnet() {

		ObjetListe listCommande = modele.getCommande();
		listCommande.setListBeginning();
		listModelCarnet.clear();

		// Tant que le plat selectionne existe
		while (listCommande.getObject() != null) {

			ItemCommande item = (ItemCommande) listCommande
					.getObjectAndIterate();
			String nom = item.getItemCommand�().getNom();
			int quantite = (item.getQuantit�());
			Double prix = quantite * item.getItemCommand�().getPrix();

			listModelCarnet
					.addElement(quantite + " * " + nom + " (" + prix + " $)");

		}

	}

	/***************************************************************
	 * Affichage du menu du restaurant
	 ***************************************************************/
	public void displayMenuItem() {

		object.Menu menuRestaurant = modele.getSelectedRestaurant().getMenu();
		ObjetListe listMenu = menuRestaurant.getListeItemMenu();
		listMenu.setListBeginning();
		listModelMenu.clear();

		// Tant que le menu n'est pas vide
		while (listMenu.getObject() != null) {

			ItemMenu item = (ItemMenu) listMenu.getObjectAndIterate();
			String nom = item.getNom();
			String prix = item.getPrix().toString();
			listModelMenu.addElement(nom + ' ' + prix);

		}

	}

	/***************************************************************
	 * Methode d'actualisation MVC
	 ***************************************************************/
	@Override
	public void update(Observable arg0, Object arg1) {

		// Affichage du menu et du carnet de commande
		displayMenuItem();
		displayCarnet();
		
		// Affichage du montant total de la commande
		txtMontant.setText((modele.getTotal()).toString());
		
		// Verifie si le client a pris une commande
		if(txtMontant.getText().equals("0.0"))
			btnConfirme.setEnabled(false);
		else
			btnConfirme.setEnabled(true);
		
	}
	
}