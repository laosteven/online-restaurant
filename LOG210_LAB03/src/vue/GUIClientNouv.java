/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIInscription.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-10-06 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import donneGenerique.Adresse;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.UUID;
import object.Client;


/****************************************************************
 * Fenetre d'inscription au systeme
 ****************************************************************/
/**
 * @author Bearminator
 *
 */
/**
 * @author Bearminator
 *
 */
public class GUIClientNouv {

	/****************************************************************
	 * Attributs
	 ****************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private Client client;

	// Nom et prenom du client
	private JTextField txtNom;
	private JTextField txtPrenom;

	// Adresse
	private JTextField txtNumero;
	private JTextField txtRue;
	private JTextField txtVille;
	private JTextField txtProvince;
	private JTextField txtCodePostal;
	private String adrMemoire;
	private boolean adrExistant;
	
	private JTextField txtNaissance;
	private JTextField txtCourriel;
	private JTextField txtPhone1;

	private JPasswordField pswPrincipal;
	private JPasswordField pswRetape;
	private String mdpPrincipal;
	private String mdpSecondaire;
	private Boolean modifier=false;

	private controlleur.ControlleurCreationUtilisateur controleurCreation;

	/****************************************************************
	 * Modification d'un compte client
	 ****************************************************************/
	public GUIClientNouv(Client client1) {

		// Recuperation de donnees
		this.client = client1;
		modifier=true;
		controleurCreation = new controlleur.ControlleurCreationUtilisateur();
		
		// Creation de fenetre
		initComposant();
		
		// Affichage d'informations du client
		txtNom.setText(client.getNom());
		txtPrenom.setText(client.getPrenom());
		txtNumero.setText(client.getNumeroTel());
		txtNaissance.setText(correctionNaissance());
		txtCourriel.setText(client.getCourriel());
		txtPhone1.setText(client.getNumeroTel());
		
		// Recuperation de l'adresse du client
		Adresse defAdd = client.getAdresseDefaut().getAdresse();
		
		// Affichage de l'adresse par defaut
		txtNumero.setText(defAdd.getNo());
		txtRue.setText(defAdd.getRue());
		txtVille.setText(defAdd.getVille());
		// FIXME
		txtProvince.setText(defAdd.getCodePostal());
		txtCodePostal.setText(defAdd.getProvince());
		
		// Informations non modifiables
		txtPrenom.setEnabled(false);
		txtNom.setEnabled(false);
		txtNaissance.setEnabled(false);
		txtCourriel.setEnabled(false);
				
	}
	
	/****************************************************************
	 * Correction de la date de naissance
	 * @return Format acceptable de la date de naissance
	 ****************************************************************/
	public String correctionNaissance(){
		
		// Recuperation de date de naissance
		String naissance = client.getDateNaissance();
		
		// Remplacement des tirets par des barres obliques
		naissance.replaceAll("-", "/");
		
		// Separation par caracteres vides
		String[] separe = naissance.split("\\s");
		
		return separe[0];
		
	}

	
	/****************************************************************
	 * Creation d'un compte client
	 * @wbp.parser.constructor 
	 ****************************************************************/
	public GUIClientNouv() {

		controleurCreation = new controlleur.ControlleurCreationUtilisateur();		
		initComposant();
	}
	
	
	
	/****************************************************************
	 * Creation de la fenetre
	 ****************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Nouveau compte");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setBounds(100, 100, 300, 670);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Breve explication du formulaire
		JLabel lblInstruct1 = new JLabel(
				"<html><p align = \"justify\">Pour creer un nouveau compte, "
						+ "veuillez remplir les informations suivantes :</p></html>");
		lblInstruct1.setHorizontalAlignment(SwingConstants.TRAILING);
		lblInstruct1.setBounds(10, 11, 270, 28);
		contentPane.add(lblInstruct1);

		// Separateur pour delimiter les zones de texte
		JSeparator sep1 = new JSeparator();
		sep1.setBounds(10, 50, 274, 2);
		contentPane.add(sep1);

		// Titre pour le nom
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(10, 63, 110, 14);
		contentPane.add(lblNom);

		// Zone de texte pour le nom
		txtNom = new JTextField();
		txtNom.setBounds(130, 60, 150, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);

		// Titre pour le prenom
		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(10, 91, 110, 14);
		contentPane.add(lblPrenom);

		// Zone de texte pour le prenom
		txtPrenom = new JTextField();
		txtPrenom.setColumns(10);
		txtPrenom.setBounds(130, 88, 150, 20);
		contentPane.add(txtPrenom);

		JSeparator sep9 = new JSeparator();
		sep9.setBounds(10, 116, 270, 2);
		contentPane.add(sep9);

		// Titre pour la date
		JLabel lblDate = new JLabel(
				"<html>Date de naissance : <br> (AAAA-MM-JJ)</html>");
		lblDate.setBounds(10, 129, 95, 28);
		contentPane.add(lblDate);

		// Zone de texte pour la date de naissance
		txtNaissance = new JTextField();
		txtNaissance.setColumns(10);
		txtNaissance.setBounds(130, 126, 150, 20);
		contentPane.add(txtNaissance);

		JSeparator sep3 = new JSeparator();
		sep3.setBounds(10, 170, 270, 2);
		contentPane.add(sep3);

		// Titre pour l'adresse
		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setBounds(10, 186, 110, 14);
		contentPane.add(lblAdresse);

		// Ajouter une nouvelle adresse
		JButton btnAjoutAdr = new JButton("Ajouter nouvelle adresse");
		btnAjoutAdr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				creationAdresse();

			}

		});
		btnAjoutAdr.setBounds(127, 351, 153, 23);
		contentPane.add(btnAjoutAdr);

		JLabel lblNumero = new JLabel("Numero :");
		lblNumero.setBounds(10, 211, 110, 14);
		contentPane.add(lblNumero);

		JLabel lblRue = new JLabel("Rue :");
		lblRue.setBounds(10, 239, 110, 14);
		contentPane.add(lblRue);

		JLabel lblVille = new JLabel("Ville :");
		lblVille.setBounds(10, 267, 110, 14);
		contentPane.add(lblVille);

		JLabel lblProvince = new JLabel("Province :");
		lblProvince.setBounds(10, 295, 110, 14);
		contentPane.add(lblProvince);

		JLabel lblCodePostal = new JLabel("Code postal :");
		lblCodePostal.setBounds(10, 323, 110, 14);
		contentPane.add(lblCodePostal);

		// Zone de texte pour l'adresse
		txtNumero = new JTextField();
		txtNumero.setColumns(10);
		txtNumero.setBounds(130, 208, 150, 20);
		contentPane.add(txtNumero);

		txtRue = new JTextField();
		txtRue.setColumns(10);
		txtRue.setBounds(130, 236, 150, 20);
		contentPane.add(txtRue);

		txtVille = new JTextField();
		txtVille.setColumns(10);
		txtVille.setBounds(130, 264, 150, 20);
		contentPane.add(txtVille);

		txtProvince = new JTextField();
		txtProvince.setColumns(10);
		txtProvince.setBounds(130, 292, 150, 20);
		contentPane.add(txtProvince);

		txtCodePostal = new JTextField();
		txtCodePostal.setColumns(10);
		txtCodePostal.setBounds(130, 320, 150, 20);
		contentPane.add(txtCodePostal);

		JSeparator sep4 = new JSeparator();
		sep4.setBounds(10, 385, 270, 2);
		contentPane.add(sep4);

		// Titre pour le courriel
		JLabel lblCourriel = new JLabel("Courriel :");
		lblCourriel.setBounds(10, 400, 110, 14);
		contentPane.add(lblCourriel);

		// Zone de texte pour le courriel
		txtCourriel = new JTextField();
		txtCourriel.setColumns(10);
		txtCourriel.setBounds(130, 397, 150, 20);
		contentPane.add(txtCourriel);

		JSeparator sep5 = new JSeparator();
		sep5.setBounds(10, 425, 270, 2);
		contentPane.add(sep5);

		// Titre pour telephone
		JLabel lblTelephone = new JLabel("Telephone :");
		lblTelephone.setBounds(10, 438, 110, 14);
		contentPane.add(lblTelephone);

		// Prefixe regional
		txtPhone1 = new JTextField();
		txtPhone1.setBounds(130, 435, 150, 20);
		contentPane.add(txtPhone1);
		txtPhone1.setColumns(10);

		JSeparator sep6 = new JSeparator();
		sep6.setBounds(10, 463, 270, 2);
		contentPane.add(sep6);

		// Instruction pour le mot de passe
		JLabel lblInstruct2 = new JLabel(
				"<html><p align = \"justify\">Veuillez inscrire votre "
						+ "mot de passe (minimum 5 caracteres) et le retapez une "
						+ "nouvelle fois dans la case qui le suit:</p></html>");
		lblInstruct2.setHorizontalAlignment(SwingConstants.TRAILING);
		lblInstruct2.setBounds(10, 476, 270, 40);
		contentPane.add(lblInstruct2);

		JSeparator sep7 = new JSeparator();
		sep7.setBounds(10, 527, 270, 2);
		contentPane.add(sep7);

		// Titre pour mot de passe
		JLabel lblPasse = new JLabel("Mot de passe :");
		lblPasse.setBounds(10, 543, 110, 14);
		contentPane.add(lblPasse);

		// Zone de texte pour mot de passe
		pswPrincipal = new JPasswordField();
		pswPrincipal.setBounds(130, 540, 150, 20);
		contentPane.add(pswPrincipal);

		// Titre pour reconfirmation du mot de passe
		JLabel lblRetape = new JLabel("Retapez :");
		lblRetape.setBounds(10, 568, 110, 14);
		contentPane.add(lblRetape);

		// Zone de texte pour la confirmation du mot de passe
		pswRetape = new JPasswordField();
		pswRetape.setBounds(130, 565, 150, 20);
		contentPane.add(pswRetape);

		JSeparator sep8 = new JSeparator();
		sep8.setBounds(10, 594, 270, 2);
		contentPane.add(sep8);

		// Bouton pour annuler l'inscription
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				int choix = JOptionPane.YES_OPTION;

				// Pour eviter que l'utilisateur perde ses donnees
				if (!isVide()) {

					choix = JOptionPane.showConfirmDialog(frame,
							"Vous risquez de perdre les informations inscrites, "
									+ "etes-vous sur?", "Confirmation",
							JOptionPane.YES_NO_OPTION);

				}

				// Confirmation: ferme la fenetre d'inscription
				if (choix == JOptionPane.YES_OPTION)
					frame.dispose();

			}
		});
		btnAnnuler.setBounds(10, 607, 89, 23);
		contentPane.add(btnAnnuler);

		// Bouton pour confirmer l'inscription
		JButton btnConfirmer = new JButton("Confirmer");
		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				confirmation();
			}
		});
		btnConfirmer.setBounds(191, 607, 89, 23);
		contentPane.add(btnConfirmer);

		// Tableau de zone de texte
		JTextField[] txts = { txtNom, txtPrenom, txtNumero, txtCourriel,
				txtPhone1, txtRue, txtVille, txtProvince,
				txtCodePostal, txtNaissance };

		// Ecouteur de cle pour chaque zone de texte
		for (int i = 0; i < txts.length; i++)
			ecouteurCle(txts[i]);

		// Ecouteurs de cle pour chaque zone de mot de passe
		pswPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				confirmation();
			}
		});
		pswRetape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				confirmation();
			}
		});

		// Validation et affichage de la fenetre
		frame.validate();
		frame.setVisible(true);

	}

	/****************************************************************
	 * Confirmation des donnees
	 ****************************************************************/
	public void confirmation() {

		// Definition de variable
		int choix = JOptionPane.NO_OPTION;

		// Verifie si toutes les cases sont remplies
		if (isVide())
			JOptionPane.showMessageDialog(frame,
					"Certaines cases ne sont pas remplies.", "Attention",
					JOptionPane.WARNING_MESSAGE);

		// Verifie si les deux mots de passe sont identiques
		else {

			if (!Arrays.equals(pswPrincipal.getPassword(),
					pswRetape.getPassword())
					|| pswPrincipal.getPassword().length < 5)

				JOptionPane.showMessageDialog(frame,
						"Le mot de passe est invalide.", "Attention",
						JOptionPane.WARNING_MESSAGE);

			else {

				// Verifie si les cases d'adresses sont vides
				if(!isVideAdresse())
					adrMemoire = concatenerAdr();
				
				// Si tout est respecte, on demande un confirmation
				choix = JOptionPane.showConfirmDialog(frame,
						"Verifiez que les informations suivantes sont correctes:"
								+ "\n\nNom : " + txtNom.getText()
								+ "\nPrenom : " + txtPrenom.getText()
								+ "\n\nAdresse : " + adrMemoire
								+ "\nNaissance : " + txtNaissance.getText()
								+ "\nCourriel : " + txtCourriel.getText()
								+ "\nTelephone : " + concatenerTel(),
						"Confirmation", JOptionPane.YES_NO_OPTION);

				if (choix == JOptionPane.YES_OPTION) {
		
					// Si aucune adresse n'a ete enregistre ou
					// si un champ d'adresse est remplie
					if(!adrExistant || !isVideAdresse())
						creationAdresse();
					
					// Creation de l'utilisateur
					creationUtilisateur();
					
				}
			}

		}

	}
	
	/****************************************************************
	 * Creation d'un nouveau utilisateur
	 ****************************************************************/
	public void creationUtilisateur() {
		
		// Recuperer le premier mot de passe
		char[] passeChar1 = pswPrincipal.getPassword();
		mdpPrincipal = String.valueOf(passeChar1);

		// Recuperer le deuxieme mot de passe
		char[] passeChar2 = pswPrincipal.getPassword();
		mdpSecondaire = String.valueOf(passeChar2);

		Boolean compteCree = false;
		
		try {
			compteCree = controleurCreation.creeUtilisateur(
					client == null ? UUID.randomUUID() : client.getIdUtilisateur(),
					client == null ? UUID.randomUUID() : client.getId(),		
					txtNom.getText(), 
					txtPrenom.getText(),
					txtPrenom.getText() + txtNom.getText() ,
					txtPhone1.getText(), 
					mdpPrincipal, 
					mdpSecondaire,
					txtNaissance.getText(), 
					txtCourriel.getText(),
					this.modifier);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (compteCree) {

			JOptionPane.showMessageDialog(this.frame,
					(Object) controleurCreation.getMessageErreur());

		} else {

			if(txtCourriel.getText().isEmpty())
				JOptionPane.showMessageDialog(frame,
						"Le compte est cree!");
			else
				JOptionPane.showMessageDialog(frame,
						"Le compte est cree! Verifiez votre courriel.");

			frame.setVisible(false);
			frame.dispose();

		}
		
	}
	
	/****************************************************************
	 * Creation d'une adresse de l'utilisateur
	 ****************************************************************/
	public void creationAdresse(){
		
		// Creation d'adresse
		Boolean adresseCree = controleurCreation.creeAdresse(
					UUID.randomUUID(), 
					UUID.randomUUID(),
					txtNumero.getText(), txtRue.getText(),
					txtVille.getText(), txtProvince.getText(),
					txtCodePostal.getText());

		// Si l'adresse n'a pas ete cree
		if (adresseCree) {

			// Afficher l'erreur
			JOptionPane.showMessageDialog(frame,
					(Object) controleurCreation.getMessageErreur());

		} else {

			// Rajouter une confirmation aussi
			JOptionPane
					.showMessageDialog(frame, "L'adresse '"
							+ concatenerAdr()
							+ "' a �t� ajout� � votre compte.");

			// Conserver l'adresse en memoire
			adrMemoire = concatenerAdr();
			
			// Vider les cases
			txtNumero.setText("");
			txtRue.setText("");
			txtProvince.setText("");
			txtCodePostal.setText("");
			txtVille.setText("");
			
			// Confirmer qu'une adresse existe pour l'utilisateur
			adrExistant = true;

		}
		
	}

	/****************************************************************
	 * Ajout des ecouteurs de cle pour chaque zone de texte
	 * 
	 * @param text
	 *            Zone de texte prise en reference
	 ****************************************************************/
	public void ecouteurCle(JTextField text) {

		text.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmation();
			}
		});

	}

	/****************************************************************
	 * Verifie si toutes les cases sont remplies
	 * 
	 * @return Confirmation des cases remplies
	 ****************************************************************/
	public boolean isVide() {

		return txtNom.getText().isEmpty() 
				&& txtPrenom.getText().isEmpty()
				&& txtNaissance.getText().isEmpty()
				&& txtCourriel.getText().isEmpty()
				&& txtPhone1.getText().isEmpty();

	}

	/****************************************************************
	 * Dans le cas ou le client desire enregistrer une nouvelle adresse,
	 * s'assurer que les cases sont belles et biens remplies
	 * 
	 * @return
	 ****************************************************************/
	public boolean isVideAdresse() {

		return txtNumero.getText().isEmpty() 
				&& txtRue.getText().isEmpty()
				&& txtVille.getText().isEmpty()
				&& txtProvince.getText().isEmpty()
				&& txtCodePostal.getText().isEmpty();

	}

	/****************************************************************
	 * Concatene les 3 cases de texte du numero de telephone
	 * 
	 * @return Le numero de telephone complet
	 ****************************************************************/
	public String concatenerTel() {

		return "( " + txtPhone1.getText() + " ) ";

	}

	/****************************************************************
	 * Concatene les cases de texte de l'adresse
	 * 
	 * @return L'adresse complet du client
	 ****************************************************************/
	public String concatenerAdr() {

		return txtNumero.getText() + " " + txtRue.getText() + " "
				+ txtVille.getText() + " " + txtProvince.getText() + " "
				+ txtCodePostal.getText();

	}
}
