/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIConnection.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import commande.Cmd_Connexion;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/******************************************************************
 * Fenetre d'authentification
 * ******************************************************************/
public class GUIConnexion {

	/**************************************************************
	 * Attributs
	 **************************************************************/
	private JFrame frame;

	/** The content pane. */
	private JPanel contentPane;

	/** The lbl utilisateur. */
	private JLabel lblUtilisateur;

	/** The txt utilisateur. */
	private JTextField txtUtilisateur;

	/** The txt mot de passe. */
	private JPasswordField txtMotDePasse;

	/** The nom. */
	private String nom;

	/** The passe. */
	private String passe;

	/**************************************************************
	 * Demarrage du systeme.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 **************************************************************/
	public static void main(String[] args) throws Exception {

		new GUIConnexion();

	}

	/**************************************************************
	 * Precondition / Constructeur
	 **************************************************************/
	public GUIConnexion() {

		// Ajustement du Look and Feel
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException erreur) {
			erreur.printStackTrace();
		}

		// Creation de la fenetre
		initComposant();

		txtUtilisateur.setText("root");
		txtMotDePasse.setText("123456");
	}

	/**************************************************************
	 * Creation de la fenetre
	 **************************************************************/
	public void initComposant() {

		// Configuration du frame
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Authentification");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 275, 250);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		/*
		// TODO Bouton d'acces au GUIClient
		JButton btnClient = new JButton("C");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cmd_Restaurant commandeRestaurant=new Cmd_Restaurant();
				try {
					//ModeleCommander modele=new ModeleCommander(commandeRestaurant.SelectionnerETRetour());
			
				//	new GUIClient(modele);
					new GUIClientResto();///enelver une fois les gets de la bd pour les menus marchent
					frame.dispose();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		btnClient.setBounds(216, 57, 45, 23);
		contentPane.add(btnClient);
		
		// TODO Bouton d'acces au GUILivreur
		JButton btnLivreur = new JButton("L");
		btnLivreur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new GUILivreur();
				frame.dispose();
				
			}
		});
		btnLivreur.setBounds(10, 57, 45, 23);
		contentPane.add(btnLivreur);
*/
		// Logo du systeme
		JLabel lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setIcon(new ImageIcon(GUIConnexion.class
				.getResource("/com/sun/java/swing/plaf/"
						+ "motif/icons/DesktopIcon.gif")));
		lblLogo.setBounds(10, 11, 251, 69);
		contentPane.add(lblLogo);

		// Ajout d'un separateur
		JSeparator sep1 = new JSeparator();
		sep1.setBounds(10, 91, 254, 2);
		contentPane.add(sep1);

		// Titre pour la case d'utilisateur
		lblUtilisateur = new JLabel("Utilisateur:");
		lblUtilisateur.setBounds(10, 106, 80, 20);
		contentPane.add(lblUtilisateur);

		// Zone de texte pour le nom de l'utilisateur
		txtUtilisateur = new JTextField();
		txtUtilisateur.setBounds(100, 106, 161, 20);
		contentPane.add(txtUtilisateur);
		txtUtilisateur.setColumns(10);

		// Titre pour le mot de passe
		JLabel lblMotDePasse = new JLabel("Mot de passe:");
		lblMotDePasse.setBounds(10, 137, 80, 20);
		contentPane.add(lblMotDePasse);

		// Zone de texte pour le mot de passe
		txtMotDePasse = new JPasswordField();
		txtMotDePasse.setBounds(100, 137, 161, 20);
		contentPane.add(txtMotDePasse);

		// Deuxieme separateur qui delimite les boutons
		JSeparator sep2 = new JSeparator();
		sep2.setBounds(10, 174, 251, 2);
		contentPane.add(sep2);

		// Bouton d'inscription
		JButton btnNouveau = new JButton("Creer compte");
		btnNouveau.setBounds(10, 187, 109, 23);
		contentPane.add(btnNouveau);

		// Ecouteur d'action pour le bouton 'Creer compte'
		btnNouveau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new GUIClientNouv();

			}
		});

		// Bouton de connexion
		JButton btnConnecter = new JButton("Connecter");
		btnConnecter.setBounds(172, 187, 89, 23);
		contentPane.add(btnConnecter);

		// Ecouteur de clavier pour mot de passe
		txtUtilisateur.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent clavier) {

				// Prise en reference de la touche du clavier
				int boutonClic = clavier.getKeyCode();

				if (boutonClic == KeyEvent.VK_ENTER)

					// On authentifie l'utilisateur
					authentification();
			}
		});

		// Ecouteur d'action pour le bouton 'connecter'
		btnConnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				// On authentifie l'utilisateur
				authentification();

			} // Fin de 'actionPerformed'

		}); // Fin de 'addActionListener'
		
		// Ecouteur de clavier pour mot de passe
		txtUtilisateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authentification();
			}
		});
		txtMotDePasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authentification();
			}
		});

		// Validation et affichage
		frame.validate();
		frame.setVisible(true);

	}
public void LivreurConnecte()
{

	
	
	
	}
/*

public void clientConnect()
{
	Cmd_Restaurant restaurant=new Cmd_Restaurant();
	Restaurant resto = new Restaurant();

	try {
		ModeleCommander modele=new ModeleCommander(restaurant.selectionnerETRetour(resto.getId()), new Cmd_Client().selectionner());
		new GUIClientResto(modele);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	
}
*/
	/****************************************************************
	 * Sequence d'authentification pour l'utilisateur
	 ****************************************************************/
	public void authentification() {

		// Pour recuperer le nom d'utilisateur
		nom = txtUtilisateur.getText();

		// Recuperer le mot de passe
		char[] passeChar = txtMotDePasse.getPassword();
		passe = String.valueOf(passeChar);

		try {
			if (new Cmd_Connexion().connecter(nom,passe)) {
				frame.dispose();
			};
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

}
