package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import controlleur.ControlleurMenu;
import modele.ObjetListe;
import modele.ModeleMenu;
import object.Restaurant;
import object.ItemMenu;
import javax.swing.UIManager;


public class GUICreeMenu implements Observer{
	
	private JFrame frmAjouterUnMenu;
	private JPanel contentPane;
	private DefaultListModel<String> listModelCarnet = new DefaultListModel<String>();
	private DefaultListModel<String> listModelMenu = new DefaultListModel<String>();
	private JList<String> lMenu = new JList<String>(listModelMenu);
	private JTextField txtNom;
	private JTextField txtRestaurant;
	private ControlleurMenu controleurAjouterMenu;
	private ModeleMenu modeleNouveauMenu;

	/***************************************************************
	 * Constructeur par defaut
	@wbp.parser.constructor
	 ***************************************************************/

	public GUICreeMenu(Restaurant restaurant) {
		
		modeleNouveauMenu = new ModeleMenu(restaurant);
		modeleNouveauMenu.addObserver(this);
		controleurAjouterMenu=new ControlleurMenu(modeleNouveauMenu);
		initComposant();
		
		update(null,null);
		

	}

	/***************************************************************
	 * Fermeture de la fenetre active et ouverture de la 
	 * fenetre du client
	 ***************************************************************/
	private void close() {
		
		// Detachement d'observateur
		
		// Disposition de la fenetre
		frmAjouterUnMenu.dispose();
		
		// Remet GUIClient a visible
		

	}
	
	/***************************************************************
	 * Methode pour la creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frmAjouterUnMenu = new JFrame();
		frmAjouterUnMenu.setTitle("Ajouter un menu | LOG210 - Iteration 2 ");
		frmAjouterUnMenu.setResizable(false);
		frmAjouterUnMenu.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmAjouterUnMenu.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				close();
			}
		});
		frmAjouterUnMenu.setBounds(100, 100, 690, 400);
		frmAjouterUnMenu.setLocationRelativeTo(null);

		// Creation du panneau principal
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAjouterUnMenu.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Panneau de Menu
		JPanel pMenu = new JPanel();
		pMenu.setLayout(null);
		pMenu.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Plat du menu", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pMenu.setBounds(355, 11, 329, 316);
		contentPane.add(pMenu);

		// Panneau avec barre de defilement pour Menu
		JScrollPane spMenu = new JScrollPane(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spMenu.setBounds(10, 24, 309, 247);
		pMenu.add(spMenu);
		
		spMenu.setViewportView(lMenu);
		
		JButton btnModifier = new JButton("Modifier plat");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(lMenu.getSelectedIndex()!=-1)
				{
					modeleNouveauMenu.getMenuRestaurant().getListeItemMenu().setObject(lMenu.getSelectedIndex());
					ItemMenu platAmodifier=(ItemMenu)modeleNouveauMenu.getMenuRestaurant().getListeItemMenu().getObject();
					frmAjouterUnMenu.setVisible(false);
					new GUIAjoutPlat(platAmodifier, lMenu.getSelectedIndex(), frmAjouterUnMenu, controleurAjouterMenu);
					
				}	
			}
		});
		btnModifier.setBounds(10, 282, 112, 23);
		pMenu.add(btnModifier);
		
		JButton btnSupprimer = new JButton("Supprimer plat");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lMenu.getSelectedIndex()!=-1)
				{controleurAjouterMenu.supprimerItem(lMenu.getSelectedIndex());}
			}
		});
		btnSupprimer.setBounds(207, 282, 112, 23);
		pMenu.add(btnSupprimer);

		// Bouton pour annuler une commande
		JButton btnAnnuler = new JButton("Annuler l'ajout du menu");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// TODO Annuler la commande au restaurant
				
				// Si le carnet contient des commandes actives
				if(!listModelCarnet.isEmpty()){
					
					// Confirmation de l'utilisateur
					int choix = JOptionPane.showConfirmDialog(
						    frmAjouterUnMenu,
						    "Etes-vous sur d'annuler l'ajout du menu " +
						    "Vos Donnez ne serons pas enregistrés.",
						    "Attention",
						    JOptionPane.YES_NO_OPTION);
					
					// Si oui, fenetre active se ferme
					if(choix == JOptionPane.YES_OPTION)
					//	modele.getCommande().clearList();
						//frameParent.setVisible(true); 
						close();	
				}				
			}
		});
		btnAnnuler.setBounds(10, 338, 163, 23);
		contentPane.add(btnAnnuler);

		JButton btnConfirme = new JButton("Enregistrer le menu");
		btnConfirme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(!controleurAjouterMenu.enregistrerMenu())
				{
					JOptionPane.showMessageDialog(frmAjouterUnMenu, controleurAjouterMenu.getMessageErreur());
					
				}	
				close();
			}
		});
		btnConfirme.setBounds(529, 338, 145, 23);
		contentPane.add(btnConfirme);
		
		JPanel pConfig = new JPanel();
		pConfig.setLayout(null);
		pConfig.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Configuration du menu", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pConfig.setBounds(10, 11, 329, 316);
		contentPane.add(pConfig);
		
		JLabel lblNomDuMenu = new JLabel("Nom du menu :");
		lblNomDuMenu.setBounds(10, 74, 72, 14);
		pConfig.add(lblNomDuMenu);
		
		JLabel lblRestaurant = new JLabel("Restaurant :");
		lblRestaurant.setBounds(10, 43, 89, 14);
		pConfig.add(lblRestaurant);
		
		txtNom = new JTextField();
		txtNom.setEditable(false);
		txtNom.setBounds(115, 71, 105, 20);
		pConfig.add(txtNom);
		txtNom.setColumns(10);
		
		txtRestaurant = new JTextField();
		txtRestaurant.setEditable(false);
		txtRestaurant.setColumns(10);
		txtRestaurant.setBounds(115, 40, 105, 20);
		pConfig.add(txtRestaurant);
		
		JButton btnAssigner = new JButton("Assigner un nom du menu");
		btnAssigner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(!controleurAjouterMenu.assignerNom(JOptionPane.showInputDialog("Entrer le nom du menu", ""))){
					JOptionPane.showMessageDialog(frmAjouterUnMenu, controleurAjouterMenu.getMessageErreur());
				}
				
			}
		});
		btnAssigner.setBounds(115, 248, 204, 23);
		pConfig.add(btnAssigner);
		
		JButton btnAjouterPlat = new JButton("Ajouter un nouveau plat");
		btnAjouterPlat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frmAjouterUnMenu.setVisible(false);
				new GUIAjoutPlat(controleurAjouterMenu,frmAjouterUnMenu);
			}
		});
		btnAjouterPlat.setBounds(115, 282, 204, 23);
		pConfig.add(btnAjouterPlat);

		// Validation et affichage
		frmAjouterUnMenu.validate();
		frmAjouterUnMenu.setVisible(true);
	}

	/***************************************************************
	 * Affichage du carnet de commande que le client a choisi dans
	 * le restaurant
	 * 
	 * @param commandeClient La liste que le client desire commander
	 ***************************************************************/

	/***************************************************************
	 * Affichage du menu du restaurant
	 ***************************************************************/
	public void displayMenuItem() {

		txtRestaurant.setText(modeleNouveauMenu.getRestaurantModifier().getNom());
		object.Menu menuRestaurant = modeleNouveauMenu.getMenuRestaurant();
		ObjetListe listMenu = menuRestaurant.getListeItemMenu();
		listMenu.setListBeginning();
		listModelMenu.clear();
		txtNom.setText(modeleNouveauMenu.getMenuRestaurant().getNom());

		while (listMenu.getObject() != null) {

			ItemMenu item = (ItemMenu) listMenu.getObjectAndIterate();
			String nom = item.getNom();
			String prix = item.getPrix().toString();
			listModelMenu.addElement(nom + ' ' + prix);
		}
	}

	/***************************************************************
	 * Methode d'actualisation MVC
	 ***************************************************************/
	public void update(Observable arg0, Object arg1) {

		displayMenuItem();
	}
}
