/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIEtatCommande.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-11-07 Version initiale
 ***************************************************************/
package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

import commande.Cmd_Commande;

import controlleur.ControlleurCommande;

import object.Commande;
import object.Commande.CommandeStatus;
import object.Utilisateur;

import java.util.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;

/***************************************************************
 * Interface de gestion d'etat des commandes du client
 ***************************************************************/
public class GUIEtatCommande implements Observer{

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	
	private DefaultListModel<String> lModelCommmande = new DefaultListModel<String>();
	private DefaultListModel<String> lModelPreparation = new DefaultListModel<String>();
	private DefaultListModel<String> lModelPret = new DefaultListModel<String>();
	
	private JList<String> lCommande = new JList<String>(lModelCommmande); 
	private JList<String> lPret = new JList<String>(lModelPreparation);
	private JList<String> lPreparation = new JList<String>(lModelPret);

	private List<Commande> listCmd;

	/***************************************************************
	 * Constructeur
	 ***************************************************************/
	public GUIEtatCommande(Utilisateur restaurateur) {

		initComposant();
		update(null, null);
		
	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation de la fenetre
		frame = new JFrame();
		frame.setTitle("Etat de commande | LOG210 - Iteration 3");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setBounds(100, 100, 680, 370);
		
		// Panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		frame.setLocationRelativeTo(null);

		// Creation du panneau de commande
		JPanel pCommande = new JPanel();
		pCommande.setLayout(null);
		pCommande.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Commande a preparer",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pCommande.setBounds(10, 11, 210, 285);
		contentPane.add(pCommande);

		// Panneau avec barre de defilement pour la liste de commande
		JScrollPane spCommande = new JScrollPane();
		spCommande
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spCommande
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spCommande.setBounds(10, 22, 190, 252);
		pCommande.add(spCommande);
		lCommande.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
			
				// Reference
				JList<?> lCommandeRef = (JList<?>) arg0.getSource();
				
				// Lorsque l'index de la liste est clique deux fois
				if(arg0.getClickCount() == 2){
					
					// Prise en reference de l'index selectionne
					int index = lCommandeRef.locationToIndex(arg0.getPoint());
					
					// FIXME Modification du statut
					try {
						new Cmd_Commande().CommandePreparationStart(listCmd.get(index).getIdCommande());
					} catch (Exception e) {

					}
					
					// Ajustement du statut
					listCmd.get(index).setStatus(CommandeStatus.Preparation);
					
					// Mise a jour
					update(null, null);
					
				}
				
			}
			
		});

		// Liste de commande
		lCommande.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spCommande.setViewportView(lCommande);

		// Panneau d'etat en preparation
		JPanel pPreparation = new JPanel();
		pPreparation.setLayout(null);
		pPreparation.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "En preparation",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pPreparation.setBounds(230, 11, 210, 285);
		contentPane.add(pPreparation);

		// Panneau avec barre de defilement pour la liste de commande en
		// preparation
		JScrollPane spPreparation = new JScrollPane();
		spPreparation
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spPreparation
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spPreparation.setBounds(10, 22, 190, 252);
		pPreparation.add(spPreparation);
		lPreparation.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				// Reference
				JList<?> lPreparationRef = (JList<?>) arg0.getSource();
				
				// Lorsque l'index de la liste est clique deux fois
				if(arg0.getClickCount() == 2){
					
					// Prise en reference de l'index selectionne
					int index = lPreparationRef.locationToIndex(arg0.getPoint());
					
					// FIXME Modification du statut
					try {
						new Cmd_Commande().CommandePreparationEnd(listCmd.get(index).getIdCommande());
					} catch (Exception e) {

					}
					
					// Ajustement du statut
					listCmd.get(index).setStatus(CommandeStatus.Ready);
					
					// Mise a jour
					update(null, null);
					
				}
				
			}
		});

		// Liste de commande en preparation
		spPreparation.setViewportView(lPreparation);

		// Panneau de commande prete a etre livre
		JPanel pPret = new JPanel();
		pPret.setLayout(null);
		pPret.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Pret",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pPret.setBounds(450, 11, 210, 285);
		contentPane.add(pPret);

		// Panneau avec barre de defilement pour la liste de commande prete
		JScrollPane spPret = new JScrollPane();
		spPret.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spPret.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spPret.setBounds(10, 22, 190, 252);
		pPret.add(spPret);

		// Liste de commande prete
		spPret.setViewportView(lPret);

		// Bouton de fermeture
		JButton btnFermer = new JButton("Fermer");
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnFermer.setBounds(10, 307, 89, 23);
		contentPane.add(btnFermer);
		
		// Affichage
		frame.validate();
		frame.setVisible(true);

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		// Nettoyage des listes
		lModelCommmande.clear();
		lModelPreparation.clear();
		lModelPret.clear();
		
		// Recuperation des commandes actives
		try {
			listCmd = new Cmd_Commande().selectionnerCommande();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Affichage des commandes selon l'etat
		for(int i = 0; i < listCmd.size(); i ++ ){
			
			// Commande a preparer
			if(listCmd.get(i).getStatus().equals(CommandeStatus.Ordered))
				lModelCommmande.addElement(listCmd.get(i).getIdCommande().toString());
			
			// En preparation
			else if(listCmd.get(i).getStatus().equals(CommandeStatus.Preparation))
				lModelPret.addElement(listCmd.get(i).getIdCommande().toString());
			
			// Commande prete
			else if(listCmd.get(i).getStatus().equals(CommandeStatus.Ready))
				lModelPreparation.addElement(listCmd.get(i).getIdCommande().toString());
					
		}
		
	}
	
}
