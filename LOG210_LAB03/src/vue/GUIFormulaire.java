/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIFormulaire.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/
package vue;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import modele.ObjetListe;
import commande.Cmd_Restaurant;
import commande.Cmd_Restaurateur;
import donneGenerique.Adresse;
import donneGenerique.DonneeGlobal;
import object.AfficheObject;
import object.Restaurant;
import object.Restaurateur;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.UUID;
import java.util.ArrayList;

/******************************************************************
 * Fenetre de formulaire
 ******************************************************************/
public class GUIFormulaire {

	/**************************************************************
	 * Attribut
	 **************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private JTextField txtPrenom;
	private JTextField txtNom;

	private Adresse nouvAdresse;
	private JTextField txtNumero;
	private JTextField txtRue;
	private JTextField txtVille;
	private JTextField txtProvince;
	private JTextField txtCodePostal;

	private JTextField txtTelephone;
	private JTextField txtType;
	private JComboBox<String> comboTitre;
	private JComboBox<String> comboAssocier;
	private JButton btnAssocier;
	private JButton btnMenu =new JButton();

	private Boolean modificationActive = false;
	private Restaurant restaurantAModifier = null;
	private Restaurateur restaurateurAModifier = null;

	private DonneeGlobal globalList = DonneeGlobal.getInstance();
	private ArrayList<Integer> listRestaurantAssociation = new ArrayList<Integer>();

	/**************************************************************
	 * Constructeur
	 * 
	 * @wbp.parser.constructor
	 * 
	 * @param indexRef
	 *            the index ref
	 **************************************************************/

	public GUIFormulaire(int indexRef) {
		initComposant(indexRef);
	}

	/**************************************************************
	 * Instantiates a new gUI formulaire.
	 * 
	 * @param indexRef
	 *            the index ref
	 * @param restaurantModifier
	 *            the restaurant modifier
	 **************************************************************/
	public GUIFormulaire(int indexRef, Restaurant restaurantModifier) {

		initComposant(indexRef);
		comboTitre.setSelectedIndex(1);
		txtNom.setText(restaurantModifier.getNom());
		txtTelephone.setText(restaurantModifier.getTelephone());
		txtType.setText(restaurantModifier.getTypeCuisine());
		txtCodePostal.setText(restaurantModifier.getAdresse().getCodePostal());
		txtNumero.setText(restaurantModifier.getAdresse().getNo());
		txtProvince.setText(restaurantModifier.getAdresse().getProvince());
		txtRue.setText(restaurantModifier.getAdresse().getRue());
		txtVille.setText(restaurantModifier.getAdresse().getVille());
		comboAssocier.setSelectedIndex(0);

		ObjetListe restaurateurListe = globalList.getRestaurateurList();
		restaurateurListe.setListBeginning();
		int indexRestaurateur = -1;
		int indexListe = 0;

		while (((AfficheObject) restaurateurListe.getObject()) != null) {

			Restaurateur restaurateur = (Restaurateur) ((AfficheObject) restaurateurListe
					.getObjectAndIterate()).getMainObject();

			if (restaurateur.getId() == restaurantModifier.getIdAssociation()) {
				indexRestaurateur = indexListe;
			}

			indexListe++;

		}
		comboAssocier.setSelectedIndex(indexRestaurateur);

		modificationActive = true;
		restaurantAModifier = restaurantModifier;

	}

	/**************************************************************
	 * Instantiates a new gUI formulaire.
	 * 
	 * @param indexRef
	 *            the index ref
	 * @param restaurateurModifier
	 *            the restaurateur modifier
	 **************************************************************/
	public GUIFormulaire(int indexRef, Restaurateur restaurateurModifier) {

		initComposant(indexRef);

		comboTitre.setSelectedIndex(0);
		
		txtNom.setText(restaurateurModifier.getNom());
		txtPrenom.setText(restaurateurModifier.getPrenom());
		txtCodePostal.setText(restaurateurModifier.getAdresse().getCodePostal());
		txtNumero.setText(restaurateurModifier.getAdresse().getNo());
		txtProvince.setText(restaurateurModifier.getAdresse().getProvince());
		txtRue.setText(restaurateurModifier.getAdresse().getRue());
		txtVille.setText(restaurateurModifier.getAdresse().getVille());
		
		//txtNumero.setText(restaurateurModifier.getAdresse()


		modificationActive = true;
		restaurateurAModifier = restaurateurModifier;

	}

	/**************************************************************
	 * Creation de la fenetre
	 * 
	 * @param index
	 *            the index
	 **************************************************************/
	public void initComposant(int index) {

		// Creation du panneau
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Formulaire");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setBounds(100, 100, 310, 525);
		frame.setLocationRelativeTo(null);

		// Panneau de contenu
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Titre de l'objet
		JLabel lblTitre = new JLabel("Titre :");
		lblTitre.setBounds(10, 11, 77, 23);
		contentPane.add(lblTitre);

		// Liste d'objet disponible
		comboTitre = new JComboBox<String>();

		comboTitre.setMaximumRowCount(3);
		comboTitre.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Restaurateur", "Restaurant", "Livreur" }));
		comboTitre.setSelectedIndex(index);
		comboTitre.setBounds(97, 11, 192, 22);

		// Ecouteur de souris
		comboTitre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				// Gestion de condition
				verifie();

			}
		});
		contentPane.add(comboTitre);

		// Titre d'association
		JLabel lblAssocier = new JLabel("Associe a :");
		lblAssocier.setBounds(10, 45, 77, 23);
		contentPane.add(lblAssocier);

		// Liste d'association disponible
		comboAssocier = new JComboBox<String>();

		comboAssocier.setBounds(97, 45, 141, 23);
		contentPane.add(comboAssocier);

		// Bouton d'association
		btnAssocier = new JButton("+");
		btnAssocier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				listRestaurantAssociation.add(comboAssocier.getSelectedIndex());

				JOptionPane
						.showMessageDialog(
								frame,
								"Le restaurateur a ete associ� a "
								+ comboAssocier.getSelectedItem()
								+ ". \nPour annuler l'association, cliquez annuler. "
								+ "\n\nPour qu'elle prenne effet, vous devez enregistrer le formulaire");

			}
		});
		btnAssocier.setBounds(248, 44, 41, 25);
		contentPane.add(btnAssocier);

		JSeparator sep1 = new JSeparator();
		sep1.setBounds(10, 79, 279, 2);
		contentPane.add(sep1);

		// Titre pour le zone de texte 'Prenom'
		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(10, 89, 77, 23);
		contentPane.add(lblPrenom);

		// Zone de texte 'Prenom'
		txtPrenom = new JTextField();
		txtPrenom.setName("");
		txtPrenom.setBounds(97, 89, 192, 23);
		contentPane.add(txtPrenom);
		txtPrenom.setColumns(10);

		// Titre pour le zone de texte 'Nom'
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(10, 123, 77, 23);
		contentPane.add(lblNom);

		// Zone de texte 'Nom'
		txtNom = new JTextField();
		txtNom.setColumns(10);
		txtNom.setBounds(97, 123, 192, 23);
		contentPane.add(txtNom);

		// Section 'Adresse'
		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setBounds(10, 170, 39, 14);
		contentPane.add(lblAdresse);

		// Titre du numero d'adresse
		JLabel lblNumero = new JLabel("Numero :");
		lblNumero.setBounds(10, 199, 44, 14);
		contentPane.add(lblNumero);

		// Zone de texte du numero d'adresse
		txtNumero = new JTextField();
		txtNumero.setColumns(10);
		txtNumero.setBounds(97, 195, 192, 23);
		contentPane.add(txtNumero);

		// Titre la rue
		JLabel lblRue = new JLabel("Rue :");
		lblRue.setBounds(10, 232, 39, 14);
		contentPane.add(lblRue);

		// Zone de texte de la rue
		txtRue = new JTextField();
		txtRue.setColumns(10);
		txtRue.setBounds(97, 229, 192, 23);
		contentPane.add(txtRue);

		// Titre pour la ville
		JLabel lblVille = new JLabel("Ville :");
		lblVille.setBounds(10, 267, 39, 14);
		contentPane.add(lblVille);

		// Zone de texte de la ville
		txtVille = new JTextField();
		txtVille.setColumns(10);
		txtVille.setBounds(97, 263, 192, 23);
		contentPane.add(txtVille);

		// Titre pour la province
		JLabel lblProvince = new JLabel("Province :");
		lblProvince.setBounds(10, 301, 48, 14);
		contentPane.add(lblProvince);

		// Zone de texte de la province
		txtProvince = new JTextField();
		txtProvince.setColumns(10);
		txtProvince.setBounds(97, 297, 192, 23);
		contentPane.add(txtProvince);

		// Titre du code postal
		JLabel lblCodePostal = new JLabel("Code postal :");
		lblCodePostal.setBounds(10, 335, 64, 14);
		contentPane.add(lblCodePostal);

		// Zone de texte du code postal
		txtCodePostal = new JTextField();
		txtCodePostal.setColumns(10);
		txtCodePostal.setBounds(97, 331, 192, 23);
		contentPane.add(txtCodePostal);

		JSeparator sep3 = new JSeparator();
		sep3.setBounds(10, 365, 279, 2);
		contentPane.add(sep3);

		// Titre pour zone de texte 'Telephone'
		JLabel lblTelephone = new JLabel("Telephone :");
		lblTelephone.setBounds(10, 378, 77, 23);
		contentPane.add(lblTelephone);

		// Zone de texte 'Telephone'
		txtTelephone = new JTextField();
		txtTelephone.setColumns(10);
		txtTelephone.setBounds(97, 378, 192, 23);
		contentPane.add(txtTelephone);

		// Titre pour zone de texte 'Type'
		JLabel lblType = new JLabel("Type : ");
		lblType.setBounds(10, 412, 77, 23);
		contentPane.add(lblType);

		// Zone de texte 'Type'
		txtType = new JTextField();
		txtType.setColumns(10);
		txtType.setBounds(97, 412, 192, 23);
		contentPane.add(txtType);

		JSeparator sep4 = new JSeparator();
		sep4.setBounds(10, 446, 279, 2);
		contentPane.add(sep4);

		// Ecouteur du bouton 'Annuler'
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.dispose();

			}
		});
		btnAnnuler.setBounds(10, 462, 69, 23);
		contentPane.add(btnAnnuler);

		// Ecouteur du bouton 'Enregistrer'
		JButton btnEnregistrer = new JButton("Enregistrer");
		btnEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				creation();
			}
		});
		btnEnregistrer.setBounds(204, 462, 85, 23);
		contentPane.add(btnEnregistrer);

		// Autorisation de case
		verifie();
		//comboAssocier.setSelectedIndex(-1);

		btnMenu = new JButton("Nouveau menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(modificationActive)
					new GUICreeMenu(restaurantAModifier);
				
			}
		});
		btnMenu.setEnabled(false);
		btnMenu.setBounds(89, 462, 105, 23);
		contentPane.add(btnMenu);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 157, 279, 2);
		contentPane.add(separator);

		// Tableau de zone de texte
		JTextField[] txts = { txtPrenom, txtNom, txtNumero, txtRue, txtVille,
				txtProvince, txtCodePostal, txtTelephone, txtType };

		// Creation d'ecouteur de cle pour chaque zone de texte
		for (int i = 0; i < txts.length; i++)
			ecouteurCle(txts[i]);

		// Actualiser
		frame.repaint();
		frame.setVisible(true);

	}

	/**************************************************************
	 * Creation d'un nouveau restaurateur / restaurant
	 **************************************************************/
	public void creation() {

		// Enregistrer information
		if (comboTitre.getSelectedItem().equals("Restaurateur")) {

			// Les champs ne sont pas completes
			if (txtPrenom.getText().isEmpty() 
					|| txtNom.getText().isEmpty() 
					|| isEmptyAdresse()) {

				JOptionPane.showMessageDialog(frame,
						"Les champs ne sont pas completes");

			} else {

				// Creation d'une nouvelle adresse pour le restaurateur
				nouvelleAdresse();
				
				// On recupere les donnees
				Restaurateur refAteur = new Restaurateur(txtPrenom.getText(),
						txtNom.getText(), nouvAdresse,
						UUID.randomUUID(),UUID.randomUUID());

				// Un nouveau restaurateur est cree
				if (modificationActive) {
					
					try {
						refAteur.setId(restaurateurAModifier.getId());
						refAteur.setIdUtilisateur(restaurateurAModifier.getIdUtilisateur());
						refAteur.getAdresse().setIdAdresse(restaurateurAModifier.getAdresse().getIdAdresse());
						new Cmd_Restaurateur().modifier(refAteur);
						JOptionPane.showMessageDialog(frame,
								"Le restaurateur a �t� modifi�");
					} catch (Exception e1) {
						e1.printStackTrace();
					}

				} else {

					try {
						new Cmd_Restaurateur().ajouter(refAteur);
					} catch (Exception event1) {
						event1.printStackTrace();
					}

					// On confirme l'enregistrement
					if (!(comboAssocier.getSelectedIndex()==-1)) {
						Iterator<Integer> iterator = listRestaurantAssociation
								.iterator();
						associerRestaurantARestaurateur(refAteur, iterator);
						JOptionPane
								.showMessageDialog(frame,
										"Information enregistree avec " +
										"association � un ou plusieurs restaurants");

					} else {
						JOptionPane
								.showMessageDialog(frame,
										"Information enregistree avec " +
										"association � aucun restaurateur");

					}

					// On ferme la fenetre
					frame.dispose();

				}

			}

			// frame.dispose();

		} // Fin d'enregistrement du restaurateur

		// Si on rajoute un restaurant
		else if (comboTitre.getSelectedItem().equals("Restaurant")) {

			// Les champs ne sont pas completes
			if (txtNom.getText().isEmpty() 
					|| isEmptyAdresse()
					|| txtTelephone.getText().isEmpty()
					|| txtType.getText().isEmpty()) {

				JOptionPane.showMessageDialog(frame,
						"Les champs ne sont pas completes");

			} else {
				
				// Creation d'une nouvelle adresse pour le restaurant
				nouvelleAdresse();

				// Regarder si ca fait du sens avec la bd
				UUID idRestaurateur = UUID
						.fromString("00000000-0000-0000-0000-000000000000");

				if (comboAssocier.getSelectedIndex() != -1) {
					idRestaurateur = restaurateurChoisie(
							comboAssocier.getSelectedIndex()).getId();
				}
				else
				{
					
					JOptionPane
					.showMessageDialog(frame,
							"Information enregistree avec " +
							"association � aucun restaurateur");
					
				}
				
				Restaurant refAnt = new Restaurant(
						txtNom.getText(),
						nouvAdresse, 
						txtTelephone.getText(),
						txtType.getText(), 
						UUID.randomUUID(), idRestaurateur);
				refAnt.setAdresse(nouvAdresse);

				// On creer le nouveau restaurant
				if (modificationActive) {
					try {
						refAnt.setIdAssociation(restaurateurChoisie(
								comboAssocier.getSelectedIndex()).getId());
						refAnt.getAdresse().setIdAdresse(restaurantAModifier.getAdresse().getIdAdresse());
						refAnt.setId(restaurantAModifier.getId());
						new Cmd_Restaurant().modifier(refAnt);
						JOptionPane.showMessageDialog(frame,
								"Le restaurant a �t� modifi�");
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					try {
						new Cmd_Restaurant().ajouter(refAnt);
					} catch (Exception e1) {
						e1.printStackTrace();
					}

					// On confirme l'enregistrement
					JOptionPane.showMessageDialog(frame,
							"Information enregistree");
				}

				// On ferme la fenetre
				frame.dispose();

			}

		} // Fin d'enregistrement du restaurant

	}

	/**************************************************************
	 * Rajoute un ecouteur de cle pour chaque zone de texte
	 **************************************************************/
	public void ecouteurCle(JTextField text) {

		text.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				creation();
			}
		});

	}

	/**************************************************************
	 * Verifie si les cases d'adresse sont vides
	 **************************************************************/
	public boolean isEmptyAdresse() {

		if (txtNumero.getText().isEmpty() 
				|| txtRue.getText().isEmpty()
				|| txtVille.getText().isEmpty()
				|| txtProvince.getText().isEmpty()
				|| txtCodePostal.getText().isEmpty())
			return true;

		return false;

	}
	
	/**************************************************************
	 * Creation d'une nouvelle adresse
	 **************************************************************/
	public void nouvelleAdresse(){
		
		nouvAdresse = new Adresse(UUID.randomUUID(),
				txtNumero.getText(), 
				txtRue.getText(),
				txtVille.getText(),
				txtProvince.getText(),
				txtCodePostal.getText()
				);
		
	}

	/**************************************************************
	 * Verifie le choix selectionne dans le JComboBox
	 **************************************************************/
	public void verifie() {

		// Prise en reference
		String choix = (String) comboTitre.getSelectedItem();

		// Gestion de condition
		if (comboTitre.getSelectedItem() == null)
			frame.repaint();

		else if (choix.equals("Restaurateur")) {
			edifice(false);
			populerRestaurant();
			comboAssocier.setEnabled(true);
			comboAssocier.setSelectedIndex(-1);
			btnAssocier.setEnabled(true);
			btnMenu.setEnabled(false);

		} else if (choix.equals("Restaurant")) {
			edifice(true);
			comboAssocier.setEnabled(true);
			populerRestaurateur();
			comboAssocier.setSelectedIndex(-1);
			btnAssocier.setEnabled(true);
			
			if(modificationActive)
				btnMenu.setEnabled(true);

		} else if (choix.equals("Livreur")) {
			edifice(false);
			comboAssocier.setEnabled(true);
			populerRestaurant();
			btnAssocier.setEnabled(true);
			btnMenu.setEnabled(false);

		}

	}

	/**************************************************************
	 * Determine si l'objet est un edifice ou non.
	 * 
	 * @param confirme
	 *            the confirme
	 **************************************************************/
	public void edifice(boolean confirme) {

		txtPrenom.setEditable(!confirme);
		txtTelephone.setEditable(confirme);
		txtType.setEditable(confirme);

	}

	/**************************************************************
	 * Recueille et donne la liste de restaurant
	 **************************************************************/
	private void populerRestaurant() {

		// Recuperation de la liste
		ObjetListe restaurantListe = globalList.getRestaurantList();
		restaurantListe.setListBeginning();

		// Creation des variables + tableau
		int index = 0;
		String[] liste = new String[restaurantListe.getNbElement()];

		// Definition de la liste
		while (((AfficheObject) restaurantListe.getObject()) != null) {

			if (((AfficheObject) restaurantListe.getObject()).isDisplayed()) {

				// On recueille les objets
				Restaurant restaurant = (Restaurant) ((AfficheObject) restaurantListe
						.getObjectAndIterate()).getMainObject();

				// Definir le nom du restaurant dans le tableau
				liste[index] = restaurant.getNom();
				index++;

			} else {
				restaurantListe.nextObject();
			}

		}

		// Le JComboBox est mis-a-jour
		comboAssocier.setModel(new DefaultComboBoxModel<String>(liste));

	}

	/**************************************************************
	 * Recueille et donne la liste de restaurateur
	 **************************************************************/
	private void populerRestaurateur() {

		// Recuperation de la liste
		ObjetListe restaurateurListe = globalList.getRestaurateurList();
		restaurateurListe.setListBeginning();

		// Creation des variables + tableau
		int index = 0;
		String[] liste = new String[restaurateurListe.getNbElement()];

		// Definition de la liste
		while (((AfficheObject) restaurateurListe.getObject()) != null) {

			Restaurateur restaurateur = (Restaurateur) ((AfficheObject) restaurateurListe
					.getObjectAndIterate()).getMainObject();

			// Definir le nom du restaurateur dans le tableau
			liste[index] = restaurateur.getNom() + ' '
					+ restaurateur.getPrenom();
			index++;

		}

		// Le JComboBox est mis-a-jour
		comboAssocier.setModel(new DefaultComboBoxModel<String>(liste));

	}

	/**************************************************************
	 * Restaurateur choisi
	 * 
	 * @param indexChoisie
	 *            the index choisie
	 * @return the restaurateur
	 **************************************************************/
	private Restaurateur restaurateurChoisie(int indexChoisie) {

		globalList.getRestaurateurList().setObject(indexChoisie);

		Restaurateur restaurateurRetour = (Restaurateur) ((AfficheObject) globalList
				.getRestaurateurList().getObjectToAssociate()).getMainObject();

		return restaurateurRetour;

	}

	private void associerRestaurantARestaurateur(
			Restaurateur restaurateurAAssocier, Iterator<Integer> iterator) {

		ObjetListe listRestaurant = globalList.getRestaurantList();

		while (iterator.hasNext()) {

			int selectedIndex = (int) iterator.next();
			listRestaurant.setObject(selectedIndex);

			AfficheObject restaurantSelected = (AfficheObject) listRestaurant
					.getObject();

			Restaurant restaurantAmodifier = ((Restaurant) restaurantSelected
					.getMainObject());

			restaurantAmodifier.setIdAssociation(restaurateurAAssocier.getId());

			try {

				new Cmd_Restaurant().modifier(restaurantAmodifier);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
}
