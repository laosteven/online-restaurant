/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #2
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUILivreur.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package vue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import object.Commande;
import controlleur.ControlleurLivreur;
import modele.ModeleLivreur;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

/***************************************************************
 * Interface du livreur
 ***************************************************************/
public class GUILivreur implements Observer {

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private Boolean flag = true;
	private ModeleLivreur modeleLivreur;
	private ControlleurLivreur controlleur;
	private DefaultListModel<String> listModelNomID = new DefaultListModel<String>();
	private JList<String> lNomID = new JList<String>(listModelNomID);
	
	private DefaultListModel<String> listModelAdresse = new DefaultListModel<String>();
	private JList<String> lAdresse = new JList<String>(listModelAdresse);
	
	/***************************************************************
	 * Constructeur
	 ***************************************************************/
	public GUILivreur(ModeleLivreur model) {

		// Creation d'adresse
		new GUIAdresse(this, model);

	}
	
	public void debute(boolean adrFonctionnelle, ModeleLivreur model) {
		
		// Si une adresse est valide
		if(adrFonctionnelle){
			
			// Creation de la fenetre
			initComposant();

			modeleLivreur = model;
	
			// Instantation du controlleur
			controlleur = new ControlleurLivreur(modeleLivreur);
			
			modeleLivreur.addObserver(this);
			update(null,null);
			
		} else {
			
			// Si le livreur annule
			new GUIConnexion();
			
		}
		
	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Carnet de commande | LOG210 - Iteration 2");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 600, 473);
		frame.setLocationRelativeTo(null);

		// Panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		// Instruction
		JLabel lblInstruction = new JLabel(
				"Veuillez selectionner une commande :");
		lblInstruction.setBounds(10, 11, 389, 14);
		contentPane.add(lblInstruction);

		// Panneau avec barre de defilement
		JScrollPane spTache = new JScrollPane();
		spTache.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spTache.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spTache.setBounds(10, 35, 574, 364);
		contentPane.add(spTache);

		// Liste des noms de restaurants
		lNomID.setPreferredSize(new Dimension(250, 0));
		
		
		// Ecouteur de la liste de Restaurant
		lNomID.addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						
						if(flag){
						lNomID.clearSelection();
						
						controlleur.selectedCommandeIndexChange(lNomID
								.getSelectedIndex());
						}
					}
				});
		
		
		lNomID.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spTache.setRowHeaderView(lNomID);
		
		// Liste des adresses correspondantes
		lAdresse.setEnabled(false);
		spTache.setViewportView(lAdresse);
		
		// Bouton de deconnexion
		JButton btnDeconnexion = new JButton("Deconnexion");
		btnDeconnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new GUIConnexion();
				frame.dispose();

			}
		});
		btnDeconnexion.setBounds(10, 410, 95, 23);
		contentPane.add(btnDeconnexion);

		// Bouton pour calcul du trajet avec Google Maps
		JButton btnCalcul = new JButton("Calculer trajet");
		btnCalcul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				frame.setVisible(false);
				new GUILivreurTrajet(frame, controlleur, modeleLivreur);

			}

		});
		btnCalcul.setBounds(483, 410, 101, 23);
		contentPane.add(btnCalcul);

		// Affichage de la fenetre
		frame.validate();
		frame.setVisible(true);
		
		

	}

	@Override
	public void update(Observable o, Object arg) {

		flag = false;
		
		// Etablie le premier noeud de la commande
		modeleLivreur.getListCommande().setListBeginning();
		
		// Assure que la la liste n'envoie pas des objets vides
		while (modeleLivreur.getListCommande().getObject() != null) {
			
			// Creation des commandes
			Commande item = (Commande) (modeleLivreur.getListCommande()
					.getObjectAndIterate());

			// Affichage du nom et du ID du restaurant
				listModelNomID.addElement(
						item.getNoConfirmation() + 
						" | " + 
						item.getRestaurant().getNom());
		
			
			// Affichage de l'adresse du restaurant
			listModelAdresse.addElement(
					item.getAdresseLivraison().toStringSpaced());
			
		}

		flag = true;
	}
}
