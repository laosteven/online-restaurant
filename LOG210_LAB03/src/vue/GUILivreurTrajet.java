/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUILivreurTrajet.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package vue;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;

import modele.ModeleLivreur;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;

import commande.Cmd_Livreur;
import controlleur.ControlleurLivreur;

/***************************************************************
 * Affichage de trajet pour le livreur
 * 
 * Le calcul du livreur est fait a l'aide de la librairie 
 * JxBrowser:
 * 
 * JxBrowser - TeamDev Ltd., [En Ligne], 
 * http://www.teamdev.com/jxbrowser/ (Page consultee le 1
 * novembre 2013)
 * 
 * ainsi que l'outil cartographique de Google Maps:
 * 
 * Google Maps, [En Ligne], https://maps.google.com/ (Page
 * consultee le 1 novembre 2013)
 ***************************************************************/
public class GUILivreurTrajet implements Observer {

	/***************************************************************
	 * Attribut
	 ***************************************************************/
	private JFrame frame;
	private JFrame frmLivreur;
	private ControlleurLivreur controlleurlivreur;
	private ModeleLivreur modeleLivreur;

	/***************************************************************
	 * Constructeur par defaut
	 ***************************************************************/
	public GUILivreurTrajet(JFrame frmLivreurRef, 
			ControlleurLivreur livreurRef, ModeleLivreur modele) {

		frmLivreur = frmLivreurRef;
		controlleurlivreur = livreurRef;
		modeleLivreur = modele;
		initComposant();

	}

	/***************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Initialisation du fureteur
		Browser fureteur = BrowserFactory.create();

		// Creation du JFrame
		frame = new JFrame();
		frame.setTitle("Calcul du trajet | LOG210 - Iteration #2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.add(fureteur.getView().getComponent(), BorderLayout.CENTER);

		// Panneau de boutons
		JPanel pBouton = new JPanel();
		pBouton.setPreferredSize(new Dimension(10, 65));
		frame.getContentPane().add(pBouton, BorderLayout.SOUTH);
		pBouton.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 13));

		// Bouton d'annulation
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frmLivreur.setVisible(true);
				frame.dispose();

			}
		});
		btnAnnuler.setPreferredSize(new Dimension(200, 40));
		pBouton.add(btnAnnuler);

		// Bouton d'acceptation
		JButton btnAccepter = new JButton("Accepter");
		btnAccepter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					if(controlleurlivreur.accepter()){
						
						new Cmd_Livreur().
						EffectuerLivraison(modeleLivreur.
								getCommandeSelectionner().
								getIdCommande(), 
								modeleLivreur.getLivreur());
						
						JOptionPane.showMessageDialog(null,"La " +
								"commande choisie vous a ete affecte");
						
					}
						
					else{
						
						JOptionPane.showMessageDialog(null, 
								"La commande choisie ne vous a pas ete" +
								" affecte, un autre livreur l'a deja choisie");
						
					}
						
				} catch (Exception e) {
					
					e.printStackTrace();
				}

			}
		});
		btnAccepter.setPreferredSize(new Dimension(200, 40));
		pBouton.add(btnAccepter);

		// Affichage de la fenetre
		frame.validate();
		frame.setVisible(true);

		// Affichage du trajet
		afficherTrajet(fureteur);

	}

	/***************************************************************
	 * Affichage du trajet par un lien vers Google Maps
	 * @param fureteurRef Le fureteur en action
	 ***************************************************************/
	private void afficherTrajet(Browser fureteurRef) {

		// Redirection a la page desiree
		fureteurRef.loadURL(controlleurlivreur.creerURL());

	}

	@Override
	public void update(Observable o, Object arg) {

	}

}
