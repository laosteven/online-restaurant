/****************************************************************
 * Cours: 				LOG210 
 * Session: 			A2013
 * Groupe: 				01
 * Projet: 				Laboratoire #1
 * Etudiants:  			David Grimard
 * 						Jean Henocq
 * 						Steven Lao
 * 						Guillaume Lepine 
 * Professeur: 			Yvan Ross
 * Nom du fichier: 		GUIPrincipal.java
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-09-19 Version initiale
 ***************************************************************/

package vue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.GridLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

import modele.ModeleLivreur;
import modele.ObjetListe;
import commande.Cmd_Restaurant;
import commande.Cmd_Restaurateur;
import donneGenerique.DonneeGlobal;
import object.AfficheObject;
import object.Restaurant;
import object.Restaurateur;

/******************************************************************
 * Fenetre principale du logiciel
 ******************************************************************/
public class GUIPrincipal implements Observer {

	/************************************************************** 
	 * Attributs 
	 **************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private JButton btnGestionEtat;
	
	private JPanel pRestaurateur;
	private JTextField txtRechAteur;
	private JTextField txtNomAteur;
	private JTextField txtPrnmAteur;
	private JTextField txtAdrAteur;
	
	private JTextField txtNomAnt;
	private JTextField txtAssocieAnt;
	private JTextField txtAdrAnt;
	private JTextField txtTelAnt;
	private JTextField txtTypeAnt;
	
	private JTextField txtNomLivreur;
	private JTextField txtAdrLivreur;
	private JTextField txtTelLivreur;
	private JTextField txtRechLivreur;
	private JTextField txtPrenomLivreur;
	
	private JTextField txtRechResaurant;

	private DefaultListModel<String> listModelRestaurant = new DefaultListModel<String>();
	private DefaultListModel<String> listModelRestaurateur = new DefaultListModel<String>();
	private DefaultListModel<String> listModelOccupe = new DefaultListModel<String>();
	
	private JList<String> jlistRestaurateur = new JList<String>(
			listModelRestaurateur);
	private JList<String> jlistRestaurant = new JList<String>(
			listModelRestaurant);
	private JList<String> jlistGestion = new JList<String>(listModelOccupe);
	
	private DonneeGlobal globalList = DonneeGlobal.getInstance();
	private ModeleLivreur modeleLivreur;

	private boolean enableSelectedIndexChange = true;
	private Restaurant selectedRestaurant;
	private Restaurateur selectedRestaurateur;

	/**************************************************************
	 * Constructeur par defaut
	 * @wbp.parser.constructor 
	 ***************************************************************/
	public GUIPrincipal() {
		
		globalList.getRestaurantList().addObserver(this);
		globalList.getRestaurateurList().addObserver(this);
		globalList.getRestaurantList().changeEnded(); 
		globalList.getRestaurateurList().changeEnded(); 
		
		// On initialise les composantes de la fenetre
		initComposant();
		
		// Verification du niveau d'administratif de l'utilisateur
		switch (globalList.getUtilisateurConnecte().getHierarchyName()) {

		// Si un restaurateur se connecte dans le systeme
		case "Restaurateur":

			// Il ne peut modifier les informations d'un restaurateur
			pRestaurateur.setVisible(false);
			pRestaurateur.setEnabled(false);
			tabbedPane.remove(pRestaurateur);

			break;

		case "Entrepreneur":

			// Refuser l'acces de la fenetre de gestion d'etat
			btnGestionEtat.setEnabled(false);
			
			break;
			
		}
	
	}

	/**************************************************************
	 * Creation de la fenetre
	 ***************************************************************/
	public void initComposant() {

		// Creation de la fenetre
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Systeme de gestion | LOG210 - Iteration 2");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 710, 460);
		frame.setLocationRelativeTo(null);

		// Creation de la barre de menu
		creerMenu();

		// Creation du panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		// Creation des panneaux d'onglet
		creerOnglet();

		// Validation et affichage
		frame.validate();
		frame.setVisible(true);
		jlistRestaurateur.setModel(listModelRestaurateur);

	}

	/**************************************************************
	 * Creation du menu du systeme
	 ***************************************************************/
	public void creerMenu() {

		// Creation du JMenuBar
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		// Menu 'Fichier'
		JMenu mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);
		JMenuItem mntmNewMenuItem = new JMenuItem("Creer formulaire");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new GUIFormulaire(-1);

			}
		});
		mntmNewMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				InputEvent.CTRL_MASK));
		mnFichier.add(mntmNewMenuItem);

		// Ajout d'un separateur
		JSeparator sep1 = new JSeparator();
		mnFichier.add(sep1);

		// MenuItem 'Fichier' > 'Deconnexion'
		JMenuItem mntmDeconnexion = new JMenuItem("Deconnexion");
		mntmDeconnexion.setMnemonic(KeyEvent.VK_D);
		mntmDeconnexion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,
				InputEvent.CTRL_MASK));
		mnFichier.add(mntmDeconnexion);

		// Ecouteur d'action, le logiciel retourne au login prompt
		mntmDeconnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {

				new GUIConnexion();
				frame.dispose();

			}
		});

		// Ajout d'un separateur
		JSeparator sep2 = new JSeparator();
		mnFichier.add(sep2);

		// MenuItem 'Fichier' > 'Quitter'
		JMenuItem mntmQuitter = new JMenuItem("Quitter");
		mntmQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
				InputEvent.ALT_MASK));
		mnFichier.add(mntmQuitter);

		// Quitter l'application
		mntmQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// MenuItem 'Aide'
		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		// MenuItem 'Aide' > 'A Propos'
		JMenuItem mntmPropos = new JMenuItem("A Propos");
		mntmPropos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				JOptionPane.showMessageDialog(frame, "<html>"
						+ "Cours:   LOG210 <br>" + "Session: A2013 <br>"
						+ "Groupe:  01 <br>" + "Projet: Laboratoire #1 <br>"
						+ "Etudiants: <ul>" + "<li>David Grimard</li>"
						+ "<li>Jean Henocq</li>" + "<li>Steven Lao</li>"
						+ "<li>Guillaume Lepine</li>" + "</ul>"
						+ "Professeur : Yvan Ross" + "</html>", "A propos",
						JOptionPane.INFORMATION_MESSAGE, null);

			}
		});
		mnAide.add(mntmPropos);

	} // Fin du 'creerMenu()'

	/**************************************************************
	 * Creation des onglets
	 ***************************************************************/
	public void creerOnglet() {

		// Creation du panneau d'onglet qui contient tous les
		// acteurs disponibles
		 tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane);

		// Panneau d'accueil
		JPanel pAccueil = new JPanel();
		tabbedPane.addTab("Accueil", (Icon) null, pAccueil, "Page d'accueil");
		pAccueil.setLayout(null);
		creerAccueil(pAccueil);

		/************************************************************
		 * Panneau du restaurateur
		 ************************************************************/
		pRestaurateur = new JPanel();
		tabbedPane.addTab("Restaurateur", null, pRestaurateur,
				"Privilege pour le restaurateur");
		pRestaurateur.setLayout(null);

		JSeparator sepAteur = new JSeparator();
		sepAteur.setOrientation(SwingConstants.VERTICAL);
		sepAteur.setBounds(335, 11, 1, 360);
		pRestaurateur.add(sepAteur);

		// Panneau de formulaire du restaurateur
		JPanel pFormAteur = new JPanel();
		pFormAteur.setBorder(new TitledBorder(null, "Informations",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pFormAteur.setBounds(10, 11, 315, 360);
		pRestaurateur.add(pFormAteur);
		pFormAteur.setLayout(null);

		// Titre pour le zone de texte 'Nom'
		JLabel lblNomAteur = new JLabel("Nom :");
		lblNomAteur.setBounds(10, 25, 62, 14);
		pFormAteur.add(lblNomAteur);

		// Zone de texte 'nom'
		txtNomAteur = new JTextField();
		txtNomAteur.setText("");
		txtNomAteur.setEditable(false);
		txtNomAteur.setBounds(82, 22, 223, 17);
		pFormAteur.add(txtNomAteur);
		txtNomAteur.setColumns(10);

		// Titre pour le zone de texte 'prenom'
		JLabel lblPrnmAteur = new JLabel("Prenom :");
		lblPrnmAteur.setBounds(10, 53, 62, 14);
		pFormAteur.add(lblPrnmAteur);

		// Zone de texte 'prenom'
		txtPrnmAteur = new JTextField();

		txtPrnmAteur.setEditable(false);
		txtPrnmAteur.setColumns(10);
		txtPrnmAteur.setBounds(82, 50, 223, 17);
		pFormAteur.add(txtPrnmAteur);

		// Titre pour le zone de texte 'adresse'
		JLabel lblAdrAteur = new JLabel("Adresse :");
		lblAdrAteur.setBounds(10, 81, 62, 14);
		pFormAteur.add(lblAdrAteur);

		// Zone de texte 'Adresse'
		txtAdrAteur = new JTextField();
		txtAdrAteur.setText("");
		txtAdrAteur.setEditable(false);
		txtAdrAteur.setColumns(10);
		txtAdrAteur.setBounds(82, 78, 223, 17);
		pFormAteur.add(txtAdrAteur);
		
		// Titre de gestion
		JLabel lblGestion = new JLabel("Occupe :");
		lblGestion.setBounds(10, 109, 62, 14);
		pFormAteur.add(lblGestion);
		
		// Panneau de deroulement
		JScrollPane spGestion = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spGestion.setBounds(82, 106, 223, 40);
		pFormAteur.add(spGestion);
		
		// Liste de restaurant qu'un restaurateur gere
		spGestion.setViewportView(jlistGestion);
		jlistGestion.setVisibleRowCount(4);
		jlistGestion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Supprimer pour restaurateur
		JButton btnSuppAteur = new JButton("Supprimer");
		btnSuppAteur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (selectedRestaurateur != null
						& jlistRestaurateur.getSelectedIndex() != -1) {

					try {
						new Cmd_Restaurateur().supprimer(selectedRestaurateur);
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						new Cmd_Restaurateur().supprimer(selectedRestaurateur);
					} catch (Exception event1) {
						event1.printStackTrace();
					}

				}
			}
		});
		btnSuppAteur.setBounds(10, 326, 89, 23);
		pFormAteur.add(btnSuppAteur);

		// Modifier pour restaurateur
		JButton btnModAteur = new JButton("Modifier");
		btnModAteur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (selectedRestaurateur != null) {
					new GUIFormulaire(0, selectedRestaurateur);
				}

			}
		});

		// Ajouter pour restaurateur
		JButton btnAjoutAteur = new JButton("Ajouter");
		btnAjoutAteur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new GUIFormulaire(0);
			}
		});
		btnAjoutAteur.setBounds(216, 326, 89, 23);
		pFormAteur.add(btnAjoutAteur);
		btnModAteur.setBounds(117, 326, 89, 23);
		pFormAteur.add(btnModAteur);
		
		// Panneau pour la liste de restaurant
		JPanel pListeAnt = new JPanel();
		pListeAnt.setBorder(new TitledBorder(null, "Restaurants",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pListeAnt.setBounds(346, 42, 343, 329);
		pRestaurateur.add(pListeAnt);
		pListeAnt.setLayout(null);

		// Liste de restaurant
		jlistRestaurateur.setBounds(10, 19, 323, 216);
		//pListeAnt.add(jlistRestaurateur);
		jlistRestaurateur.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Panneau avec barre de defilement pour la liste de restarant
		JScrollPane spResto = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		spResto.setViewportView(jlistRestaurateur);
		spResto.setBounds(10, 21, 323, 297);
		pListeAnt.add(spResto);

		// Titre pour barre de recherche de restaurateur
		JLabel lblRechAnt = new JLabel("Recherche :");
		lblRechAnt.setBounds(346, 11, 65, 20);
		pRestaurateur.add(lblRechAnt);

		// Barre de recherche pour restaurateur
		txtRechAteur = new JTextField();
		txtRechAteur.setBounds(421, 11, 268, 20);
		
		// Ecouteur de document
		txtRechAteur.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {

				// Recuperation de la liste de restaurant
				ObjetListe restaurateurList = globalList.getRestaurateurList();
				restaurateurList.setListBeginning();

				Restaurateur searchRestaurateur;

				// Definition des restaurants
				while (((AfficheObject) restaurateurList.getObject()) != null) {

					searchRestaurateur = (Restaurateur) ((AfficheObject) restaurateurList
							.getObject()).getMainObject();

					if (searchRestaurateur.getNom().toLowerCase()
							.contains(txtRechAteur.getText())) {

						((AfficheObject) restaurateurList.getObject())
								.setDispalyed(true);

					} else if (searchRestaurateur.getPrenom().toLowerCase()
							.contains(txtRechAteur.getText())) {

						((AfficheObject) restaurateurList.getObject())
								.setDispalyed(true);

					} else if (searchRestaurateur.getAdresse()
							.toStringSpaced()
							.toLowerCase()
							.contains(txtRechAteur.getText())) {

						((AfficheObject) restaurateurList.getObject())
								.setDispalyed(true);

					} else {

						((AfficheObject) restaurateurList.getObject())
								.setDispalyed(false);

					}

					restaurateurList.nextObject();

				} // Fin du 'while'

				restaurateurList.changeEnded();

			} // Fin du 'changedUpdate'

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				changedUpdate(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				changedUpdate(arg0);
			}

		}); // Fin du 'documentListener'

		pRestaurateur.add(txtRechAteur);
		txtRechAteur.setColumns(10);

		// Ecouteur de selection dans la liste
		jlistRestaurateur.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {

				if (enableSelectedIndexChange == true) {

					// Recuperation de la source
					JList<?> source = (JList<?>) event.getSource();

					// Recuperation de la liste de restaurateur
					ObjetListe restaurateurList = globalList
							.getRestaurateurList();
					restaurateurList.setListBeginning();

					selectedRestaurateur = (Restaurateur) ((AfficheObject) restaurateurList
							.getObject()).getMainObject();

					int selectedIndex = source.getSelectedIndex();
					int displayedObjectFound = 0;

					while (((AfficheObject) restaurateurList.getObject()) != null
							&& displayedObjectFound != selectedIndex + 1) {

						if (((AfficheObject) restaurateurList.getObject())
								.isDisplayed()) {

							selectedRestaurateur = (Restaurateur) ((AfficheObject) restaurateurList
									.getObjectAndIterate()).getMainObject();
							
							displayedObjectFound++;

						} else
							restaurateurList.nextObject();

					}
					if (selectedRestaurateur != null) {

						txtNomAteur.setText(selectedRestaurateur.getNom());
						txtPrnmAteur.setText(selectedRestaurateur.getPrenom());
						txtAdrAteur.setText(selectedRestaurateur.getAdresse().toStringSpaced());

					}

				}

			}

		});

		// Panneau du restaurant
		JPanel pRestaurant = new JPanel();
		tabbedPane.addTab("Restaurant", null, pRestaurant,
				"Privilege pour le restaurant");
		pRestaurant.setLayout(null);

		JSeparator sepAnt = new JSeparator();
		sepAnt.setOrientation(SwingConstants.VERTICAL);
		sepAnt.setBounds(335, 11, 1, 360);
		pRestaurant.add(sepAnt);

		// Panneau de formulaire pour un restaurant
		JPanel pFormAnt = new JPanel();
		pFormAnt.setLayout(null);
		pFormAnt.setBorder(new TitledBorder(null, "Formulaire",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pFormAnt.setBounds(10, 11, 315, 360);
		pRestaurant.add(pFormAnt);

		// Titre pour le zone de texte 'nom'
		JLabel lblNomAnt = new JLabel("Nom :");
		lblNomAnt.setBounds(10, 25, 62, 14);
		pFormAnt.add(lblNomAnt);

		// Zone de texte 'nom'
		txtNomAnt = new JTextField();
		txtNomAnt.setEditable(false);
		txtNomAnt.setColumns(10);
		txtNomAnt.setBounds(82, 22, 223, 17);
		pFormAnt.add(txtNomAnt);

		// Titre pour le zone de texte 'Associe a'
		JLabel lblAssocieAnt = new JLabel("Associer a :");
		lblAssocieAnt.setBounds(10, 53, 62, 14);
		pFormAnt.add(lblAssocieAnt);

		// Zone de texte 'Associe a'
		txtAssocieAnt = new JTextField();
		txtAssocieAnt.setEditable(false);
		txtAssocieAnt.setColumns(10);
		txtAssocieAnt.setBounds(82, 50, 223, 17);
		pFormAnt.add(txtAssocieAnt);

		// Titre pour le zone de texte 'Adresse'
		JLabel lblAdrAnt = new JLabel("Adresse :");
		lblAdrAnt.setBounds(10, 81, 62, 14);
		pFormAnt.add(lblAdrAnt);

		// Zone de texte 'Adresse'
		txtAdrAnt = new JTextField();
		txtAdrAnt.setEditable(false);
		txtAdrAnt.setColumns(10);
		txtAdrAnt.setBounds(82, 78, 223, 17);
		pFormAnt.add(txtAdrAnt);

		// Titre pour le zone de texte 'Telephone'
		JLabel lblTelAnt = new JLabel("Telephone :");
		lblTelAnt.setBounds(10, 109, 62, 14);
		pFormAnt.add(lblTelAnt);

		// Zone de texte 'Telephone'
		txtTelAnt = new JTextField();
		txtTelAnt.setEditable(false);
		txtTelAnt.setColumns(10);
		txtTelAnt.setBounds(82, 106, 223, 17);
		pFormAnt.add(txtTelAnt);

		// Titre pour le zone de texte 'Type'
		JLabel lblTypeAnt = new JLabel("Type :");
		lblTypeAnt.setBounds(10, 137, 62, 14);
		pFormAnt.add(lblTypeAnt);

		// Zone de texte 'Type'
		txtTypeAnt = new JTextField();
		txtTypeAnt.setEditable(false);
		txtTypeAnt.setColumns(10);
		txtTypeAnt.setBounds(82, 134, 223, 17);
		pFormAnt.add(txtTypeAnt);

		// Supprimer un restaurant
		JButton btnSuppAnt = new JButton("Supprimer");
		btnSuppAnt.setBounds(10, 326, 89, 23);
		btnSuppAnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (selectedRestaurant != null
						& jlistRestaurant.getSelectedIndex() != -1) {
					try {
						new Cmd_Restaurant().supprimer(selectedRestaurant);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					try {
						new Cmd_Restaurant().supprimer(selectedRestaurant);
					} catch (Exception event1) {
						event1.printStackTrace();
					}
				}
			}
		});
		pFormAnt.add(btnSuppAnt);

		// Ajouter un restaurant
		JButton btnAjoutAnt = new JButton("Ajouter");
		btnAjoutAnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUIFormulaire(1);
			}
		});
		btnAjoutAnt.setBounds(216, 326, 89, 23);
		pFormAnt.add(btnAjoutAnt);

		// Modifier un restaurant
		JButton btnModAnt = new JButton("Modifier");
		btnModAnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedRestaurant != null) {
					new GUIFormulaire(1, selectedRestaurant);
				}
			}
		});
		btnModAnt.setBounds(117, 326, 89, 23);
		pFormAnt.add(btnModAnt);

		// Panneau de la liste de livreur
		JPanel pListeRestaurant = new JPanel();
		pListeRestaurant.setLayout(null);
		pListeRestaurant.setBorder(new TitledBorder(null, "Liste",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pListeRestaurant.setBounds(346, 42, 343, 270);
		pRestaurant.add(pListeRestaurant);

		// Ecouteur de liste
		jlistRestaurant.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {

				if (enableSelectedIndexChange == true) {

					JList<?> source = (JList<?>) event.getSource();

					ObjetListe restaurantListe = globalList.getRestaurantList();

					restaurantListe.setListBeginning();

					selectedRestaurant = (Restaurant) ((AfficheObject) restaurantListe
							.getObject()).getMainObject();

					int selectedIndex = source.getSelectedIndex();
					int displayedObjectFound = 0;

					while (((AfficheObject) restaurantListe.getObject()) != null
							&& displayedObjectFound != selectedIndex + 1) {

						if (((AfficheObject) restaurantListe.getObject())
								.isDisplayed()) {

							selectedRestaurant = (Restaurant) ((AfficheObject) restaurantListe
									.getObjectAndIterate()).getMainObject();
							displayedObjectFound++;

						} else
							restaurantListe.nextObject();

					}
					if (selectedRestaurant != null) {
						String adresse=selectedRestaurant.getAdresse().toStringSpaced();
						txtNomAnt.setText(selectedRestaurant.getNom());
						txtAdrAnt.setText(adresse);
						txtTelAnt.setText(String.valueOf(selectedRestaurant
								.getTelephone()));
						txtTypeAnt.setText(selectedRestaurant.getTypeCuisine());

						Restaurateur restoAssocie = trouveRestaurateurID(selectedRestaurant
								.getIdAssociation());
						if (restoAssocie != null) {
							txtAssocieAnt.setText(restoAssocie.getNom() + ' '
									+ restoAssocie.getPrenom());
						} else {
							txtAssocieAnt.setText(" ");
						}

					}

				}

			}

		});
		jlistRestaurant.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jlistRestaurant.setBounds(10, 19, 323, 240);
		pListeRestaurant.add(jlistRestaurant);

		// Titre pour la barre de recherche de restaurant
		JLabel lblRechVreur = new JLabel("Recherche :");
		lblRechVreur.setBounds(346, 11, 65, 20);
		pRestaurant.add(lblRechVreur);

		// Barre de recherche de livreur
		txtRechResaurant = new JTextField();
		txtRechResaurant.setColumns(10);
		txtRechResaurant.setBounds(421, 11, 268, 20);
		pRestaurant.add(txtRechResaurant);
		
		JPanel pGestionEtat = new JPanel();
		pGestionEtat.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Commandes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pGestionEtat.setBounds(346, 321, 343, 50);
		pRestaurant.add(pGestionEtat);
		pGestionEtat.setLayout(null);
		
		btnGestionEtat = new JButton("Gestion d'etat de commande");
		btnGestionEtat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				new GUIEtatCommande(
						globalList.getUtilisateurConnecte());
				
			}
		});
		btnGestionEtat.setBounds(164, 16, 169, 23);
		pGestionEtat.add(btnGestionEtat);

		// Panneau du livreur
		// Le code suivant ne presente aucun commentaire
		JPanel pLivreur = new JPanel();
		tabbedPane.addTab("Livreur", null, pLivreur,
				"Privilege pour le livreur");
		pLivreur.setLayout(null);
		
		JSeparator sepLivreur = new JSeparator();
		sepLivreur.setOrientation(SwingConstants.VERTICAL);
		sepLivreur.setBounds(335, 11, 1, 360);
		pLivreur.add(sepLivreur);
		
		JPanel pFormLivreur = new JPanel();
		pFormLivreur.setLayout(null);
		pFormLivreur.setBorder(new TitledBorder(null, "Formulaire",
						TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pFormLivreur.setBounds(10, 11, 315, 360);
		pLivreur.add(pFormLivreur);
		
		JLabel lblNomLivreur = new JLabel("Nom :");
		lblNomLivreur.setBounds(10, 25, 62, 14);
		pFormLivreur.add(lblNomLivreur);
		
		txtNomLivreur = new JTextField();
		txtNomLivreur.setEditable(false);
		txtNomLivreur.setColumns(10);
		txtNomLivreur.setBounds(82, 22, 223, 17);
		pFormLivreur.add(txtNomLivreur);
		
		JLabel lblPrenomLivreur = new JLabel("Prenom :");
		lblPrenomLivreur.setBounds(10, 53, 62, 14);
		pFormLivreur.add(lblPrenomLivreur);
		
		txtPrenomLivreur = new JTextField();
		txtPrenomLivreur.setEditable(false);
		txtPrenomLivreur.setColumns(10);
		txtPrenomLivreur.setBounds(82, 50, 223, 17);
		pFormLivreur.add(txtPrenomLivreur);
		
		JLabel lblAdrLivreur = new JLabel("Adresse :");
		lblAdrLivreur.setBounds(10, 81, 62, 14);
		pFormLivreur.add(lblAdrLivreur);
		
		txtAdrLivreur = new JTextField();
		txtAdrLivreur.setEditable(false);
		txtAdrLivreur.setColumns(10);
		txtAdrLivreur.setBounds(82, 78, 223, 17);
		pFormLivreur.add(txtAdrLivreur);
		
		JLabel lblTelLivreur = new JLabel("Telephone :");
		lblTelLivreur.setBounds(10, 109, 62, 14);
		pFormLivreur.add(lblTelLivreur);
		
		txtTelLivreur = new JTextField();
		txtTelLivreur.setEditable(false);
		txtTelLivreur.setColumns(10);
		txtTelLivreur.setBounds(82, 106, 223, 17);
		pFormLivreur.add(txtTelLivreur);
		
		JButton btnSupprLivreur = new JButton("Supprimer");
		btnSupprLivreur.setBounds(10, 326, 89, 23);
		pFormLivreur.add(btnSupprLivreur);
		
		JButton btnAjoutLivreur = new JButton("Ajouter");
		btnAjoutLivreur.setBounds(216, 326, 89, 23);
		pFormLivreur.add(btnAjoutLivreur);
		
		JButton btnModLivreur = new JButton("Modifier");
		btnModLivreur.setBounds(117, 326, 89, 23);
		pFormLivreur.add(btnModLivreur);
		
		JPanel pListeLivreur = new JPanel();
		pListeLivreur.setLayout(null);
		pListeLivreur.setBorder(new TitledBorder(null, "Liste",
						TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pListeLivreur.setBounds(346, 42, 343, 329);
		pLivreur.add(pListeLivreur);

		JLabel lblRechLivreur = new JLabel("Recherche :");
		lblRechLivreur.setBounds(346, 11, 65, 20);
		pLivreur.add(lblRechLivreur);
		
		txtRechLivreur = new JTextField();
		txtRechLivreur.setColumns(10);
		txtRechLivreur.setBounds(421, 11, 268, 20);
		pLivreur.add(txtRechLivreur);
		
		// Fin de l'onglet des livreurs

		txtRechResaurant.getDocument().addDocumentListener(

		new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {

				ObjetListe restaurantListe = globalList.getRestaurantList();

				restaurantListe.setListBeginning();

				Restaurant searchRestaurant;

				while (((AfficheObject) restaurantListe.getObject()) != null) {

					searchRestaurant = (Restaurant) ((AfficheObject) restaurantListe
							.getObject()).getMainObject();

					if (searchRestaurant.getNom().toLowerCase()
							.contains(txtRechResaurant.getText())) {

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(true);

					} else if (searchRestaurant.getNom().toLowerCase()
							.contains(txtRechResaurant.getText())) {

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(true);

					} else if (searchRestaurant.getAdresse().toStringSpaced().toLowerCase()
							.contains(txtRechResaurant.getText())) {

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(true);

					} else if (searchRestaurant.getTypeCuisine().toLowerCase()
							.contains(txtRechResaurant.getText())) {

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(true);

					} else if (searchRestaurant.getTelephone().toLowerCase()
							.contains(txtRechResaurant.getText())) {

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(true);

					} else

						((AfficheObject) restaurantListe.getObject())
								.setDispalyed(false);

					restaurantListe.nextObject();

				}

				restaurantListe.changeEnded();

			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				changedUpdate(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				changedUpdate(arg0);

			}
		});
		
	} // Fin de 'creerOnglet()'

	/**************************************************************
	 * Creation / Mise en page de l'onglet d'accueil
	 *
	 * @param pAccueil Panneau pour l'information d'accueil
	 ***************************************************************/
	public void creerAccueil(JPanel pAccueil) {

		// Affichage du logo
		JLabel lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setIcon(new ImageIcon(
				GUIPrincipal.class
						.getResource("/com/sun/java/swing/plaf/motif/icons/DesktopIcon.gif")));
		lblLogo.setBounds(10, 11, 679, 76);
		pAccueil.add(lblLogo);

		// Ajout d'un premier separateur horizontal
		JSeparator sep1 = new JSeparator();
		sep1.setBounds(10, 98, 679, 2);
		pAccueil.add(sep1);

		// Ajout d'un autre separateur au dessous du bienvenu
		JSeparator sep2 = new JSeparator();
		sep2.setOrientation(SwingConstants.VERTICAL);
		sep2.setBounds(345, 145, 1, 226);
		pAccueil.add(sep2);

		// Separateur vertical
		JSeparator sep3 = new JSeparator();
		sep3.setBounds(10, 145, 679, 2);
		pAccueil.add(sep3);

		// Mot de bienvenu
		JLabel lblBienvenu = new JLabel("Bienvenu");
		lblBienvenu.setFont(new Font("Calibri", Font.BOLD, 18));
		lblBienvenu.setHorizontalAlignment(SwingConstants.CENTER);
		lblBienvenu.setBounds(10, 111, 679, 23);
		pAccueil.add(lblBienvenu);

		// Description du logiciel
		JLabel lblDescription = new JLabel("<html>"
				+ "<b>Systeme de gestion (v.3.0)</b> <br><br>"
				+ "<b>Description:</b> <br> " + "Ce logiciel est utilise comme"
				+ " gestion de commandes et de livraison pour differents"
				+ " restaurants. <br><br>" + "<b>Auteurs:</b><ol>"
				+ "<li>David Grimard</li>" + "<li>Jean Henocq</li>"
				+ "<li>Steven Lao</li>" + "<li>Guillaume Lepine</li>" + "</ol>"
				+ "</html>");
		lblDescription.setVerticalAlignment(SwingConstants.TOP);
		lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescription.setBounds(20, 175, 315, 196);
		pAccueil.add(lblDescription);

		// Tutorat sur l'utilisation du logiciel
		JLabel lblTutorat = new JLabel("<html>"
				+ "<b>Instructions:</b><br><ol>"
				+ "<li>Pour debuter, selectionnez l'onglet que vous desirez"
				+ " travailler.</li>"
				+ "<li>Remplissez le formulaire et cliquez 'Enregistrer'.</li>"
				+ "<li>Pour modifier un formulaire enregistre, selectionnez"
				+ " le formulaire en question dans la liste d'objet et "
				+ "cliquez sur le bouton 'Modifier'.</li>" + "</ol>"
				+ "Pour voir les modifications recentes, choisissez l'onglet"
				+ " 'Log'.<br><br>"
				+ "Pour vous deconnecter du systeme, cliquez dans 'Fichier' "
				+ "> 'Deconnexion' ou 'Ctrl + D'." + "</html>");
		lblTutorat.setHorizontalAlignment(SwingConstants.TRAILING);
		lblTutorat.setVerticalAlignment(SwingConstants.TOP);
		lblTutorat.setBounds(366, 175, 323, 196);
		pAccueil.add(lblTutorat);

	} // Fin de 'creerAccueil()'

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {

		this.displayRestaurantList(globalList.getRestaurantList());
		this.displayRestaurateurList(globalList.getRestaurateurList());

	}

	/***************************************************************
	 * Display restaurant list.
	 *
	 * @param restaurantList the restaurant list
	 ***************************************************************/
	public void displayRestaurantList(ObjetListe restaurantList) {
		restaurantList.setListBeginning();
		enableSelectedIndexChange = false;
		jlistRestaurant.clearSelection();
		listModelRestaurant.clear();

		while (((AfficheObject) restaurantList.getObject()) != null) {

			if (((AfficheObject) restaurantList.getObject()).isDisplayed()) {

				Restaurant restaurant = (Restaurant) ((AfficheObject) restaurantList
						.getObjectAndIterate()).getMainObject();

				String restaurantName = restaurant.getNom();
				listModelRestaurant.addElement(restaurantName);

			} else
				restaurantList.nextObject();

		}

		enableSelectedIndexChange = true;

	}

	/***************************************************************
	 * Display restaurateur list.
	 *
	 * @param restaurateurList the restaurateur list
	 ***************************************************************/
	public void displayRestaurateurList(ObjetListe restaurateurList) {

		restaurateurList.setListBeginning();
		enableSelectedIndexChange = false;
		jlistRestaurateur.clearSelection();
		listModelRestaurateur.clear();

		while (((AfficheObject) restaurateurList.getObject()) != null) {

			if (((AfficheObject) restaurateurList.getObject()).isDisplayed()) {

				Restaurateur restaurateur = (Restaurateur) ((AfficheObject) restaurateurList
						.getObjectAndIterate()).getMainObject();

				String restaurateurName = restaurateur.getNom() + " "
						+ restaurateur.getPrenom();

				listModelRestaurateur.addElement(restaurateurName);

			} else
				restaurateurList.nextObject();

		}

		enableSelectedIndexChange = true;

	}

	/***************************************************************
	 * Trouve restaurateur id.
	 *
	 * @param idRestaurateur the id restaurateur
	 * @return the restaurateur
	 ***************************************************************/
	private Restaurateur trouveRestaurateurID(UUID idRestaurateur) {
		ObjetListe restaurateurListe = globalList.getRestaurateurList();
		restaurateurListe.setListBeginning();
		Restaurateur restaurateurRetour = null;

		while (((AfficheObject) restaurateurListe.getObject()) != null) {

			Restaurateur restaurateur = (Restaurateur) ((AfficheObject) restaurateurListe
					.getObjectAndIterate()).getMainObject();

			if (restaurateur.getId().equals(idRestaurateur) ) {
				restaurateurRetour = restaurateur;
			}

		}

		return restaurateurRetour;

	}
}
