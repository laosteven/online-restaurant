# Restaurant Catalog System #
-------

Build a runnable **online restaurant catalog** for takeout orders which allows different types of user to interact with it, such as:

* **Restaurant owners**: create, edit or delete menu orders sold from their restaurant.
* **Customers**: sign in, log in, view and order from different restaurants available.
* **Delivery**: check in, check pending deliveries, view directions.

For example, once a customer order something from one of the restaurant, both of them will receive an **email notification** confirming the reception of the order. 

When the order is ready to be delivered, an available delivery man confirms with his mobiles phones **GPS coordinates** that he's ready and the application will calculate the closest possible way to the customer's delivery address.

Prototype images from 2nd iteration out of 4:

![1.png](https://bitbucket.org/repo/6kXgjp/images/2515085982-1.png)

![2.png](https://bitbucket.org/repo/6kXgjp/images/2506431008-2.png)

All accounts, restaurant and menu informations are stored in a database with **MySQL**.

# Objectives #
-------
* Show the necessary information based on *user's privilege*.
* Build user account and menu forms with required informations.

# For more information #
-------
Visit the following website: [**Software analysis and conception** (LOG210)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log210) [*fr*]